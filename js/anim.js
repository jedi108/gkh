$(function() {
//	$(".buttCont").hover(function(){
//	    $(this).stop().animate({"opacity": 0.5});
//	},function(){
//	    $(this).stop().animate({"opacity": 1});
//	});
//
//	$("#logomain").hover(function(){
//	    $(this).stop().animate({"opacity": 0.5});
//	},function(){
//	    $(this).stop().animate({"opacity": 1});
//	});

//Скроллинг
 $('body').bgscroll({scrollSpeed:50 , direction:'h' });
});


(function() {
    $.fn.bgscroll = $.fn.bgScroll = function( options ) {
         
        if( !this.length ) return this;
        if( !options ) options = {};
        if( !window.scrollElements ) window.scrollElements = {};
         
        for( var i = 0; i < this.length; i++ ) {
             
            var allowedChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            var randomId = '';
            for( var l = 0; l < 5; l++ ) randomId += allowedChars.charAt( Math.floor( Math.random() * allowedChars.length ) );
             
                this[ i ].current = 0;
                this[ i ].scrollSpeed = options.scrollSpeed ? options.scrollSpeed : 110;
                this[ i ].direction = options.direction ? options.direction : 'h';
                window.scrollElements[ randomId ] = this[ i ];
                 
                eval( 'window[randomId]=function(){var axis=0;var e=window.scrollElements.' + randomId + ';e.current -= 1;if (e.direction == "h") axis = e.current + "px 0";else if (e.direction == "v") axis = "0 " + e.current + "px";else if (e.direction == "d") axis = e.current + "px " + e.current + "px";$( e ).css("background-position", axis);}' );
                                                          
                setInterval( 'window.' + randomId + '()', options.scrollSpeed ? options.scrollSpeed : 70 );
            }
             
            return this;
        }
})(jQuery);




/*
jQuery FancyDropShadow-Plugin
author: Ralph Harrer
website: http://www.ralphharrer.at
date: 2010-05-02

This plugin is free to use. You can change it as you like.
I would appreciate it if you mention my name if you use this plugin in your code.
cheers and have fun!
*/
(function($){
	$.fn.fancyDropShadow = function(options){
		var opt = $.extend("", $.fn.fancyDropShadow.defaults, options);
		var target = null;
		if (opt.target == "this"){
			target = $(this);
		} else {
			target = $(opt.target);
		}
		var thisItem = this;
		target.bind("mousemove", function(e){
			var x = e.pageX;
			var y = e.pageY;
			var thisX = thisItem.position().left + thisItem.width()/2;
			var thisY = thisItem.position().top + thisItem.height()/2;
			var dx = (thisX - x) * opt.strengthX;
			if (dx < 0 && Math.abs(dx) > opt.maxX){ dx = -opt.maxX; }
			if (dx > opt.maxX){ dx = opt.maxX; }
			
			var dy = (thisY - y) * opt.strengthY;
			if (dy < 0 && Math.abs(dy) > opt.maxY){ dy = -opt.maxY; }
			if (dy > opt.maxY){ dy = opt.maxY; }
			
			var dist = Math.sqrt(dx*dx + dy*dy) * opt.strengthBlur;
			//debug(dx + ", " + dy + ", dist: " + dist);
			var shadowColor = opt.shadowColor;
			var shadowMode = opt.cssShadowMode;
			if (opt.cssShadowMode == "textShadow"){
				thisItem.css({
					textShadow: dx + "px " + dy + "px " + dist + "px " + (shadowColor)
				});
			} else {
				thisItem.css({
					boxShadow: dx + "px " + dy + "px " + dist + "px " + (shadowColor),
					"-moz-box-shadow": dx + "px " + dy + "px " + dist + "px " + (shadowColor),
					"-webkit-box-shadow": dx + "px " + dy + "px " + dist + "px " + (shadowColor)
				});
			}
		});
	}
	
	$.fn.fancyDropShadow.defaults = {
		shadowColor: "#000",
		strengthX: 0.1,
		strengthY: 0.1,
		strengthBlur: 0.5,
		maxX: 5,
		maxY: 10,
		maxBlur: 10,
		cssShadowMode: "textShadow",
		target: "this"
	};
})(jQuery);