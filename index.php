<?php
// $Id: dump-database-d6.sh,v 1.3 2010/07/30 01:28:00 dries Exp $

// define('DRUPAL_ROOT', "/var/www/freelance/gkh"); 

date_default_timezone_set("UTC"); 

// Define default settings.
// $_SERVER['HTTP_HOST']       = 'default';
// $_SERVER['PHP_SELF']        = '/index.php';
// $_SERVER['REMOTE_ADDR']     = '127.0.0.1';
// $_SERVER['SERVER_SOFTWARE'] = NULL;
// $_SERVER['REQUEST_METHOD']  = 'GET';
// $_SERVER['QUERY_STRING']    = '';
// $_SERVER['PHP_SELF']        = $_SERVER['REQUEST_URI'] = '/';
// $_SERVER['HTTP_USER_AGENT'] = 'console';

// // Bootstrap Drupal.
// include_once '../gkh/includes/bootstrap.inc';
// drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
 

// Если хост равен localhost, то включаем режим отладки и подключаем отладочную
// конфигурацию
if(($_SERVER['HTTP_HOST']=='gkh-yii') || ($_SERVER['HTTP_HOST']=='gkh')){
	// remove the following lines when in production mode
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	$yii=dirname(__FILE__).'/../../yii/framework/yii.php';
	$config=dirname(__FILE__).'/protected/config/local.php';
	// specify how many levels of call stack should be shown in each log message
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
	//echo $_SERVER['HTTP_HOST'];
	//die();
}
// Иначе выключаем режим отладки и подключаем рабочую конфигурацию
else { 
	define('YII_DEBUG', false);
	$yii=dirname(__FILE__).'/../../yii/framework/yiilite.php';
	$config=dirname(__FILE__).'/protected/config/production.php';
}

//require_once './protected/components/simplehtmldom' . DIRECTORY_SEPARATOR. 'simple_html_dom.php';

require_once($yii);
Yii::createWebApplication($config)->run();
