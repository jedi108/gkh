<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'sourceLanguage' => 'en_US',
    'language' => 'ru',
    'charset' => 'utf-8',
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Учебно-курсовой комбинат жилищно-коммунального хозяйства',
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        // Helpers
        'application.helpers.*',
        'application.models.*',
        'application.components.*',
        // Class Images (upload, view image formats)
        'application.modules.images.components.*',
        'application.modules.jfiles.models.*',
        'application.modules.jfile.models.*',
        'application.extensions.xupload.models.*',
        'application.modules.GkhZayavki.models.*',
        'application.modules.Profile.models.*',

        'application.components.nestedset.*',
        'application.components.jcom_ImgResizeHtml.*',
        'application.components.simplehtmldom.*',

        'application.modules.gkh.models.*',
        'application.modules.feedback.models.*',

        'application.modules.juser.components.*',
        //'application.modules.juser.models.*',
        'application.modules.article.models.*',
        'application.modules.news.models.*',
        'application.modules.gkh.models.*',
        'application.modules.gallary.models.*',

        'application.modules.user.models.*',
        'application.modules.user.components.*',
    ),
    'aliases' => array(
        'xupload' => 'ext.xupload',
        //'user.UserModule' => 'user'
    ),
    'modules' => array(

        'gkh',
        //'juser',
        'news',
        'article',
        'gkh',
        'org',
        'Profile',
        'GkhZayavki',
        'jfiles',
        'images',
        'dash',
        'feedback',
        'gallary',
        'user'=> array(
            'returnUrl'=>array('/site/index'),
            'sendActivationMail' => false,
            # allow access for non-activated users
            'loginNotActiv' => false,
            'user_page_size' => 100,
            'fields_page_size' => 100,
        ),

    ),
    // application components
    'components' => array(
//        'user' => array(
// Social
//            'class' => 'JWebUser',
//            'allowAutoLogin' => true,
//             enable cookie-based authentication
//            'loginUrl' => array('/user/user/login'),
//            'defaultRoles' => array('guest'),
// MyCode
//            'loginUrl' => array('/site/login'),
//            'class' => 'WebUser',
//            'allowAutoLogin'=>true,

//        ),
        'user'=>array(
            // enable cookie-based authentication
            //'class' => 'WebUser',
            'class' => 'JWebUser',
            'allowAutoLogin'=>true,
            'loginUrl' => array('/user/login'),
        ),
        'urlManager' => array(
            //'cacheID' => '_url__cache',
            
            'showScriptName' => false,
            'urlFormat' => 'path',
            'caseSensitive' => true,
            'rules' => array(
                'news' => 'news/news/index',
                '/news/<id:\d+>' => 'news/news/view',
//                            
                'article' => 'article/article/index',
                '/article/<id:\d+>' => 'article/article/view',
                
                '/org/<id:\d+>' => 'org/view',
//                            
                //'site/login' => 'juser/site/login',
                'feedback/admin' => 'feedback/feedback/admin',
                'feedback' => 'feedback/feedback/create',
                'feedback/all' => 'feedback/feedback/index',
                'feedback/thankyou' => '/feedback/feedback/thankyou',
                
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                //'<module:\w+>/<controller:\w+>/<action:\w+>/*' => '<module>/<controller>/<action>',
                //'<module:\w+>/<controller:\w+>/<action:\w+>/*' => '<module>/<controller>/<action>',
            ),
        ),
        'image' => array(
            'class' => 'application.modules.images.extensions.imageapi.CImage', // 'ext.imageapi.CImage',
        //'presets' => 
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=jedi108_gkhyii',
            'emulatePrepare' => true,
            'username' => 'jedi108_gkhyii',
            'password' => '108gkhyii',
            'charset' => 'utf8',
            'tablePrefix' => 't_',
            // включить кэширование схем для улучшения производительности
            'schemaCachingDuration' => 36000,
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on webPHP Parse error: syntax error, unexpected end of file in pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'vadimka108@gmail.com',
    ),
);
