<?php
return CMap::mergeArray(
    // наследуемся от production.php
    require(dirname(__FILE__).'/production.php'),
    array(
//		'import'=>array(
//
//		),
		'modules'=>array(

			'gii'=>array(
				'class'=>'system.gii.GiiModule',
				'password'=>'sex108',
                'generatorPaths'=>array('application.gii'),
				// If removed, Gii defaults to localhost only. Edit carefully to taste.
				'ipFilters'=>array('127.0.0.1','::1'),
			),
		),		
        'components'=>array(
            'urlManager' => array(
                //'cacheID' => '_url__cache',

                'showScriptName' => true,
            ),
//			'user'=>array(
//				// enable cookie-based authentication
//				'class' => 'JWebUser',
//				'allowAutoLogin'=>true,
//				//'defaultRoles' => array('guest'),
//				'loginUrl' => array('/site/login'),
//			),
            // переопределяем компонент db
			'db'=>array(
				'connectionString' => 'mysql:host=localhost;dbname=gkh_yii',
				'emulatePrepare' => true,
				'username' => 'root',
				'password' => 'mysql',
				'charset' => 'utf8',
				'tablePrefix' =>'t_',
				// включить кэширование схем для улучшения производительности
				'schemaCachingDuration'=>0,
                                //'enableProfiling'=>true,
			),
        ),
    )
);
 
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
//return array(
//
//'sourceLanguage'=>'en_US',
//  'language'=>'ru',
//  'charset'=>'utf-8',
// 
//	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
//	'name'=>'Учебно-курсовой комбинат жилищно-коммунального хозяйства',
//
//	'preload'=>array('log'),
//
//	// autoloading model and component classes
//	'import'=>array(
//		// Helpers
//		'application.helpers.*',
//
//		'application.models.*',
//		'application.components.*',
//		// Class Images (upload, view image formats)
//		'application.modules.images.components.*',
//
//		'application.modules.jfiles.models.*',
//		'application.modules.jfile.models.*',
//		'application.extensions.xupload.models.*',
//		
//		'application.modules.GkhZayavki.models.*',
//		'application.modules.Profile.models.*',
//		'application.components.nestedset.*',
//		'application.modules.gkh.models.*',
//
//	),
//	'aliases' => array(
//		'xupload' => 'ext.xupload'
//	),
//
//	'modules'=>array(
//		'gkh',
//		'org',
//		'Profile',
//		'GkhZayavki',
//		'jfiles',
//		'images',
//		'dash',
//		'feedback',
//		// uncomment the following to enable the Gii tool
//		'gii'=>array(
//			'class'=>'system.gii.GiiModule',
//			'password'=>'sex108',
//		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
//			'ipFilters'=>array('127.0.0.1','::1'),
//		),
//	),
//
//	// application components
//	'components'=>array(
//		// 'coreMessages'=>array(
//
//		// 	'language'=>'ru',
//		// 	//'basePath'=>null,
//
//		// ),
//
//		'user'=>array(
//			// enable cookie-based authentication
//			'class' => 'WebUser',
//			'allowAutoLogin'=>true,
//			//'defaultRoles' => array('guest'),
//			//'loginUrl' => array('/user/user/login'),
//		),
//
//		'urlManager'=>array(
//            'caseSensitive' => true,
//			'showScriptName'=>false,
//			'urlFormat'=>'path',
//			'rules'=>array(
//				'feedback'=>'feedback/feedback/create',
//				'feedback/thankyou'=>'/feedback/feedback/thankyou',
//				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
////				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
////				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
////				'<module:\w+>/<controller:\w+>/<action:\w+>/*' => '<module>/<controller>/<action>',
////				'<module:\w+>/<controller:\w+>/<action:\w+>/*' => '<module>/<controller>/<action>',
//
//			),
//			
//		),
// 		/*
//		'db'=>array(
//			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
//		),
//		*/
//		// uncomment the following to use a MySQL database
//		'image' => array(
//			'class' =>'application.modules.images.extensions.imageapi.CImage',// 'ext.imageapi.CImage',
//			//'presets' => 
//		),
//		'db'=>array(
//			'connectionString' => 'mysql:host=localhost;dbname=gkh_yii',
//			'emulatePrepare' => true,
//			'username' => 'root',
//			'password' => 'mysql',
//			'charset' => 'utf8',
//			'tablePrefix' =>'t_',
//			// включить кэширование схем для улучшения производительности
//			// 'schemaCachingDuration'=>3600,
//		),
//
//		'errorHandler'=>array(
//			// use 'site/error' action to display errors
//            'errorAction'=>'site/error',
//        ),
//		'log'=>array(
//			'class'=>'CLogRouter',
//			'routes'=>array(
//				array(
//					'class'=>'CFileLogRoute',
//					'levels'=>'trace, error, warning',
//				),
//                array(
//                    'class' => 'CFileLogRoute',
//                    'categories'=>'my.*',
//                    'levels' => 'trace',
//                    'maxFileSize' => '10240',
//                    'logFile' => 'my_app_log_file_name',
//                ),
//				// uncomment the following to show log messages on web pages
//				/*
//				array(
//					'class'=>'CWebLogRoute',
//				),
//				*/
//			),
//		),
//	),
//
//	// application-level parameters that can be accessed
//	// using Yii::app()->params['paramName']
//	'params'=>array(
//		// this is used in contact page
//		'adminEmail'=>'info@belleriveparfum.com',
//	),
//);
