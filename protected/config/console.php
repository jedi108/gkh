<?php


//This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',
	// application components
        'import' => array(
            'application.helpers.*',
        ),
    'modules'=>array(
        #...
        'user'=>array(
            # encrypting method (php hash function)
            'hash' => 'md5',
            # send activation email
            'sendActivationMail' => true,
            # allow access for non-activated users
            'loginNotActiv' => false,

            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => false,

            # automatically login from registration
            'autoLogin' => true,

            # registration path
            'registrationUrl' => array('/user/registration'),

            # recovery password path
            'recoveryUrl' => array('/user/recovery'),

            # login form path
            'loginUrl' => array('/user/login'),

            # page after login
            'returnUrl' => array('/user/profile'),

            # page after logout
            'returnLogoutUrl' => array('/user/login'),
        ),
        #...
    ),
	'components'=>array(
		// 'db'=>array(
		// 	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		// ),
            //./yiic migrate --connectionID=dblocal
		'dblocal'=>array(
			'class'=>'CDbConnection',
			'connectionString' => 'mysql:host=localhost;dbname=gkh_yii',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'mysql',
			'charset' => 'utf8',
			'tablePrefix' =>'t_',
			// включить кэширование схем для улучшения производительности
			//'schemaCachingDuration'=>3600,
		),
		'db'=>array(
			'class'=>'CDbConnection',
			'connectionString' => 'mysql:host=localhost;dbname=jedi108_gkh',
			'emulatePrepare' => true,
			'username' => 'jedi108_gkh',
			'password' => '108gkh',
			'charset' => 'utf8',
			'tablePrefix' =>'t_',
			// включить кэширование схем для улучшения производительности
			//'schemaCachingDuration'=>3600,
		),
	),
);

 
// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
//return array(
//	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
//	'name'=>'My Console Application',
//	// application components
//	'components'=>array(
//		// 'db'=>array(
//		// 	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
//		// ),
//
//		'db'=>array(
//			'connectionString' => 'mysql:host=localhost;dbname=yiigkh1',
//			'emulatePrepare' => true,
//			'username' => 'root',
//			'password' => 'mysql',
//			'charset' => 'utf8',
//			'tablePrefix' =>'t_',
//			// включить кэширование схем для улучшения производительности
//			//'schemaCachingDuration'=>3600,
//		),
//	),
//);