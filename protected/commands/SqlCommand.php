<?php

class SqlCommand extends CConsoleCommand
{
	function run($args)
	{
		Yii::import('ext.yii-database-dumper.SDatabaseDumper');
                $dumper = new SDatabaseDumper();
                $file = Yii::getPathOfAlias('webroot.backups').DIRECTORY_SEPARATOR.'dump_'.date('Y-m-d_H_i_s').'sql';
                if(function_exists('gzencode')) {
                    file_put_contents ($file.'.gz', gzencode ($dumper->getDump ()));
                } else {
                    file_put_contents ($file, $dumper->getDump());
                }
	}

	function index()
	{
		echo '++';
	}
}
?>