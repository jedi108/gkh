<?php

class m130613_140306_add_cost_all extends CDbMigration
{
	public function up()
	{
            $sql = "ALTER TABLE  `t_specializ` ADD  `cost_all` DECIMAL( 9, 2 ) NOT NULL COMMENT  'Стоимость общая' AFTER  `cost_year`";
            $this->execute($sql);
        }   

	public function down()
	{
		echo "m130613_140306_add_cost_all does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}