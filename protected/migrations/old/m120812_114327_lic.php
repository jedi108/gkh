<?php

class m120812_114327_lic extends CDbMigration
{
	public function up()
	{
		$sql = "CREATE TABLE IF NOT EXISTS `t_licenz` (
  				`id` int(32) NOT NULL AUTO_INCREMENT,
				`name` varchar(255) NOT NULL,
				`pic` varchar(255) NOT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Лицензии' AUTO_INCREMENT=7 ;

				--
				-- Дамп данных таблицы `t_licenz`
				--

				INSERT INTO `t_licenz` (`id`, `name`, `pic`) VALUES
				(1, ' Лицензия на право ведения общеобразовательной деятельности ', '/file/pic/lic/1_001.jpg'),
				(2, 'Приложение № 1 стр. 1  к Лицензии на право ведения общеобразовательной деятельности ', '/file/pic/lic/2_001.jpg'),
				(3, 'Приложение № 1 стр. 2 к Лицензии на право ведения общеобразовательной деятельности ', '/file/pic/lic/3_001.jpg'),
				(4, 'Приложение № 1 стр. 3  к Лицензии на право ведения общеобразовательной деятельности ', '/file/pic/lic/4_001.jpg'),
				(5, 'Приложение № 1 стр. 4  к Лицензии на право ведения общеобразовательной деятельности ', '/file/pic/lic/5_001.jpg'),
				(6, 'Приложение № 1 стр. 5  к Лицензии на право ведения общеобразовательной деятельности ', '/file/pic/lic/6_001.jpg');";
				
		$this->execute($sql);
		echo "Исправление utf8_unicode_ci для uuid так как все сейчас в utf8_general_ci, вообще предлагаю для uuid использовать везде utf8_general_ci для скорости";

	}

	public function down()
	{
		echo "m120812_114327_lic does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}