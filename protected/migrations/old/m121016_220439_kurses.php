<?php

class m121016_220439_kurses extends CDbMigration
{
	public function up()
	{
		$sql = "
DROP TABLE IF EXISTS `t_kurs`;
CREATE TABLE IF NOT EXISTS `t_kurs` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `img_logo` varchar(255) NOT NULL,
  `img_slide` varchar(255) NOT NULL,
  `id_org` int(32) NOT NULL,
  `body` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_org` (`id_org`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `t_kurs`
--

INSERT INTO `t_kurs` (`id`, `name`, `img_logo`, `img_slide`, `id_org`, `body`) VALUES
(1, 'Дорожно-строительные машины (ДСМ)', '/file/pic/direction/logo/5026ef4c087c60ebc6700b811eeaa869%s.png', '/file/pic/direction/slide/5026e4e50ad4eb8e4ee90b6419b3e196%s.jpg', 2, '<p>\r\n	localhost yiigkh1 t_page &quot;Страницы&quot; Обзор Структура SQL Поиск Вставить Экспорт Импорт Операции Слежение # Поле Тип Сравнение Атрибуты Null По умолчанию Дополнительно Действие 1 id int(32) Нет Нет AUTO_INCREMENT Изменить Удалить Ещё 2 title varchar(255) utf8_general_ci Нет Нет Изменить Удалить Ещё 3 body lon<strong>gtext utf8_general_ci Нет Нет Изменить Удалить Ещё 4 type int(2) Нет Нет Изменить Удалить Ещё 5 date_created timestamp Нет CURRENT_TIMESTAMP Изменить Удалить Ещё Отметить все / Снять выделение С отмеченными: Обзор Изменить Удалить Первичный Уникальный Индекс Версия для печати Связи Анализ структуры таблицы Отсл</strong>еживать таблицу Добавить поле(я) В конец таблицы В начало таблицы После Индексы: Действие Имя индекса Тип Уникальный Упакован Поле Уникальных элементов Сравнение Null Комментарий Изменить Удалить PRIMARY BTREE Да Нет id 10 A Изменить Удалить type BTREE Нет Нет type 5 A Создать индекс для столбца/ов Используемое пространство Тип Использование Данные 65,536 Байт Индекс 16,384 Байт Всего 81,920 Байт Статистика строк Характеристика Значение Формат Compact Сравнение utf8_general_ci Следующий Autoindex 9 Создание Авг 31 2012 г., 21:47</p>\r\n'),
(2, 'Котлы, баллоны, сосуды и трубопроводы', '/file/pic/direction/logo/5026fcba0ebd7edaa089003a2da7ae3d%s.png', '', 0, ''),
(3, 'Грузоподъемные механизмы (ГПМ)', '/file/pic/direction/logo/5026fcde14fd22e4423c080d11dc9bd8%s.png', '/file/pic/direction/slide/5026e618211868c2958a06b32e5b9250%s.jpg', 0, ''),
(4, 'Сварочные работы', '/file/pic/direction/logo/5026fd0f0ebdef7109b903213d446697%s.png', '', 0, ''),
(5, 'Электробезопасность и теплоэнергоустановки', '/file/pic/direction/logo/5027cd8513720c4a2091047a1313d46f%s.png', '', 0, ''),
(6, 'Лифтовое хозяйство', '/file/pic/direction/logo/5027cd9b0e5eac91f8f00c5b23d890f7%s.png', '', 0, ''),
(7, 'Саморегулируемые организации (СРО)', '/file/pic/direction/logo/5027cfc01052fc7f100e06562ca2dd03%s.png', '', 0, ''),
(8, 'Экология', '/file/pic/direction/logo/5027cec60756a5efc13003202cea901c%s.png', '/file/pic/direction/slide/5040881d17fc38cf984a0db9316db40b%s.jpg', 1, ''),
(9, 'Охрана труда', '', '', 0, ''),
(10, 'Оказание первой медицинской помощи', '/file/pic/direction/logo/5027cf6d12aaf215ea100f3f148ded0b%s.png', '/file/pic/direction/slide/502678d3149517607ce90b1131476eab%s.jpg', 0, ''),
(11, 'Пожарная безопасность', '/file/pic/direction/logo/5027cef6117e976be24b056a10f85c90%s.png', '', 0, ''),
(12, 'Специальности ЖКХ', '', '', 0, ''),
(13, 'Газовое хозяйство', '/file/pic/direction/logo/5027cf8115020d7fef5e0edd3ab7ed4e%s.png', '', 0, ''),
(14, 'ДОПОГ и безопасность движения', '/file/pic/direction/logo/5027cfaf049a96a433c301e528a8bc5d%s.png', '/file/pic/direction/slide/50267ba01495dda8b3560ca719f48bb5%s.jpg', 0, ''),
(15, 'Аттестация рабочих мест', '/file/pic/direction/logo/5027cf9a0c06a27ebb310faa34f6a572%s.png', '', 0, '');";
		$this->execute($sql);
	}

	public function down()
	{
		echo "m121016_220439_kurses does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}