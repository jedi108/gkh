<?php

class m121013_221901_menu extends CDbMigration
{
	public function up()
	{
		$sql="
DROP TABLE IF EXISTS `t_jmenu`;
CREATE TABLE IF NOT EXISTS `t_jmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `root` int(10) unsigned DEFAULT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rgt` int(10) unsigned NOT NULL,
  `level` smallint(5) unsigned NOT NULL,
  `parentId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `level` (`level`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Дамп данных таблицы `t_jmenu`
--

INSERT INTO `t_jmenu` (`id`, `name`, `link`, `root`, `lft`, `rgt`, `level`, `parentId`) VALUES
(19, 'aaa', 'bbb', 19, 1, 8, 1, 0),
(23, 'Главная', 'Direction/index', NULL, 2, 5, 2, 0),
(24, 'deeee', 'rrr', NULL, 6, 7, 2, 0),
(25, 'rr', 'tt', NULL, 3, 4, 3, 0);
";
		$this->execute($sql);
	}

	public function down()
	{
		echo "m121013_221901_menu does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}