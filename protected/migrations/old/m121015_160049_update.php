<?php

class m121015_160049_update extends CDbMigration
{
	public function up()
	{
		$sql = "

DROP TABLE IF EXISTS `t_education`;
CREATE TABLE IF NOT EXISTS `t_education` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `id_profile` int(32) NOT NULL,
  `institution` varchar(255) NOT NULL COMMENT 'Название учебного заведения',
  `n_diplom` varchar(16) NOT NULL COMMENT 'Номер диплома',
  `educate` varchar(16) NOT NULL COMMENT 'Образование',
  `qualification` int(11) NOT NULL COMMENT 'Квалификация',
  `specialty` varchar(255) NOT NULL COMMENT 'Специальность',
  `end_year` varchar(4) NOT NULL COMMENT 'год окончания',
  `qualification_text` varchar(128) NOT NULL,
  `id_educate` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_profile` (`id_profile`),
  KEY `Qualification` (`qualification`),
  KEY `id_educate` (`id_educate`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Образование' AUTO_INCREMENT=80 ;

-- --------------------------------------------------------

--
-- Структура таблицы `t_profile`
--

DROP TABLE IF EXISTS `t_profile`;
CREATE TABLE IF NOT EXISTS `t_profile` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(255) NOT NULL COMMENT 'Фамилия',
  `firstname` varchar(255) NOT NULL COMMENT 'Имя',
  `middlename` varchar(255) NOT NULL,
  `year_birthday` varchar(4) NOT NULL,
  `addr` varchar(255) NOT NULL,
  `email` varchar(64) NOT NULL,
  `phone` varchar(64) NOT NULL,
  `id_org` int(32) NOT NULL,
  `doljnost` varchar(128) NOT NULL COMMENT 'Должность',
  PRIMARY KEY (`id`),
  KEY `id_org` (`id_org`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=143 ;

-- --------------------------------------------------------

--
-- Структура таблицы `t_profile_org`
--

DROP TABLE IF EXISTS `t_profile_org`;
CREATE TABLE IF NOT EXISTS `t_profile_org` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `id_profile` int(32) NOT NULL,
  `id_org` int(32) NOT NULL,
  `type_link` int(32) NOT NULL COMMENT 'тип связи',
  PRIMARY KEY (`id`),
  KEY `id_profile` (`id_profile`,`id_org`),
  KEY `type_link` (`type_link`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=78 ;			
";
		//$this->execute($sql);
	}

	public function down()
	{
		echo "m121015_160049_update does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}