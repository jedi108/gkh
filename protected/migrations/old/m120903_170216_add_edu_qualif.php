<?php

class m120903_170216_add_edu_qualif extends CDbMigration
{
	public function up()
	{
		$sql = "ALTER TABLE  `t_education` ADD  `qualification_text` VARCHAR( 128 ) NOT NULL;
				ALTER TABLE  `t_profile` ADD  `id_cat` INT( 32 ) NOT NULL;";
		$this->execute($sql);
	}

	public function down()
	{
		echo "m120903_170216_add_edu_qualif does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}