<?php

class m121002_192909_add_field_id_educat extends CDbMigration
{
	public function up()
	{
		$sql = "
				ALTER TABLE  `t_education` ADD  `id_educate` INT NOT NULL ,
ADD INDEX (  `id_educate` );
				ALTER TABLE  `t_gkh_zayavka` ADD  `id_attestation` INT NOT NULL;
			";
		$this->execute($sql);
	}

	public function down()
	{
		echo "m121002_192909_add_field_id_educat does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}