<?php

class m120828_175619_org_profile_files extends CDbMigration
{
	public function up()
	{
		$sql="
			DROP TABLE IF EXISTS `t_files`;
			CREATE TABLE IF NOT EXISTS `t_files` (
			  `id` int(32) NOT NULL AUTO_INCREMENT,
			  `file` varchar(255) NOT NULL,
			  `name` varchar(255) NOT NULL,
			  `type` varchar(8) NOT NULL,
			  `id_page` int(32) NOT NULL,
			  PRIMARY KEY (`id`),
			  KEY `id_page` (`id_page`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Хранилице файлов' AUTO_INCREMENT=10 ;

			-- --------------------------------------------------------

			--
			-- Структура таблицы `t_org`
			--

			DROP TABLE IF EXISTS `t_org`;
			CREATE TABLE IF NOT EXISTS `t_org` (
			  `id` int(32) NOT NULL AUTO_INCREMENT,
			  `name_small` varchar(255) NOT NULL,
			  `FIO_rukovoditel` varchar(255) NOT NULL,
			  `FIO_ispolnitil` varchar(255) NOT NULL,
			  `logo` varchar(255) NOT NULL COMMENT 'логотип',
			  `inn` varchar(12) NOT NULL COMMENT 'ИНН',
			  `kpp` varchar(9) NOT NULL COMMENT 'КПП',
			  `bik` varchar(9) NOT NULL,
			  `adr_fact` varchar(255) NOT NULL,
			  `adr_ur` varchar(255) NOT NULL,
			  `phone` varchar(128) NOT NULL,
			  `fax` varchar(128) NOT NULL,
			  `email` varchar(128) NOT NULL,
			  `bnk_korr` varchar(20) NOT NULL,
			  `bnk_rchet` varchar(20) NOT NULL,
			  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  `_org_type` int(11) NOT NULL,
			  `_z_type` int(1) NOT NULL,
			  PRIMARY KEY (`id`),
			  KEY `name_small` (`name_small`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Организации' AUTO_INCREMENT=47 ;

			-- --------------------------------------------------------

			--
			-- Структура таблицы `t_profile`
			--

			DROP TABLE IF EXISTS `t_profile`;
			CREATE TABLE IF NOT EXISTS `t_profile` (
			  `id` int(32) NOT NULL AUTO_INCREMENT,
			  `lastname` varchar(255) NOT NULL COMMENT 'Фамилия',
			  `firstname` varchar(255) NOT NULL COMMENT 'Имя',
			  `middlename` varchar(255) NOT NULL,
			  `year_birthday` varchar(4) NOT NULL,
			  `addr` varchar(255) NOT NULL,
			  `email` varchar(64) NOT NULL,
			  `phone` varchar(64) NOT NULL,
			  `id_specializ` int(32) NOT NULL,
			  `id_org` int(32) NOT NULL,
			  PRIMARY KEY (`id`),
			  KEY `id_specializ` (`id_specializ`),
			  KEY `id_org` (`id_org`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

			--
			-- Ограничения внешнего ключа сохраненных таблиц
			--

			--
			-- Ограничения внешнего ключа таблицы `t_profile`
			--
			ALTER TABLE `t_profile`
			  ADD CONSTRAINT `t_profile_ibfk_1` FOREIGN KEY (`id_specializ`) REFERENCES `t_specializ` (`id`);
			SET FOREIGN_KEY_CHECKS=1;
			COMMIT;";
		$this->execute($sql);
	}

	public function down()
	{
		echo "m120828_175619_org_profile_files does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}