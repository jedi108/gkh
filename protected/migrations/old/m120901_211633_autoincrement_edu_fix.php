<?php

class m120901_211633_autoincrement_edu_fix extends CDbMigration
{
	public function up()
	{
		$sql="ALTER TABLE  `t_education` CHANGE  `id`  `id` INT( 32 ) NOT NULL AUTO_INCREMENT";
		$this->execute($sql);
	}

	public function down()
	{
		echo "m120901_211633_autoincrement_edu_fix does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}