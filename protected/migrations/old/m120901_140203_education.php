<?php

class m120901_140203_education extends CDbMigration
{
	public function up()
	{
		$sql = "DROP TABLE IF EXISTS `t_education`;
				CREATE TABLE IF NOT EXISTS `t_education` (
				  `id` int(32) NOT NULL,
				  `id_profile` int(32) NOT NULL,
				  `institution` varchar(255) NOT NULL COMMENT 'Название учебного заведения',
				  `qualification` int(11) NOT NULL COMMENT 'Квалификация',
				  `specialty` varchar(255) NOT NULL COMMENT 'Специальность',
				  `end_year` varchar(4) NOT NULL COMMENT 'год окончания',
				  PRIMARY KEY (`id`),
				  KEY `id_profile` (`id_profile`),
				  KEY `Qualification` (`qualification`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Образование';
				SET FOREIGN_KEY_CHECKS=1;";
		$this->execute($sql);
	}

	public function down()
	{
		echo "m120901_140203_education does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}