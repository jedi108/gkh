<?php

class m120830_211759_id_org_add extends CDbMigration
{
	public function up()
	{
		$sql = "ALTER TABLE  `t_direction` ADD  `id_org` INT( 32 ) NOT NULL ,
				ADD INDEX (  `id_org` );
				";
		$this->execute($sql);
	}

	public function down()
	{
		echo "m120830_211759_id_org_add does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}