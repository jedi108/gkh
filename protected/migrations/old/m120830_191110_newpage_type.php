<?php

class m120830_191110_newpage_type extends CDbMigration
{
	public function up()
	{
		$sql="ALTER TABLE  `t_page` ADD  `type` INT( 2 ) NOT NULL ,	ADD INDEX (  `type` );
				ALTER TABLE  `t_page` ADD  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;";
		$this->execute($sql);
	}

	public function down()
	{
		echo "m120830_191110_newpage_type does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}