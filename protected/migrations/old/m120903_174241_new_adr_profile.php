<?php

class m120903_174241_new_adr_profile extends CDbMigration
{
	public function up()
	{
		$sql = "ALTER TABLE  `t_profile` ADD  `id_org_adr` INT( 32 ) NOT NULL COMMENT  'Желаемый адрес обучения';
				ALTER TABLE  `t_org` CHANGE  `orglink`  `orglink` INT( 32 ) NOT NULL COMMENT  'Партнер, просто организация'";
		$this->execute($sql);
	}

	public function down()
	{
		echo "m120903_174241_new_adr_profile does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}