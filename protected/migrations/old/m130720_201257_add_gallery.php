<?php

class m130720_201257_add_gallery extends CDbMigration
{
	public function up()
	{
            $sql = "CREATE TABLE IF NOT EXISTS `t_gallary` (
            `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
            `name` varchar(255) NOT NULL COMMENT 'название галереи',
            `body` longtext NOT NULL COMMENT 'текст для галереи',
            PRIMARY KEY (`id`),
            KEY `name` (`name`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='список галерей' AUTO_INCREMENT=1 ;
            SET FOREIGN_KEY_CHECKS=1;
            COMMIT;
            
            ALTER TABLE  `t_gallary` CHANGE  `name`  `name` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT  'название галереи';
            ALTER TABLE  `t_gallary` CHANGE  `body`  `body` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT  'текст';
            ALTER TABLE  `t_gallary` CHANGE  `body`  `body` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT  'текст'
            ALTER TABLE  `t_news` ADD  `id_gallary` INT NULL , ADD INDEX (  `id_gallary` );
            ALTER TABLE  `t_page` ADD  `id_gallary` INT NULL , ADD INDEX (  `id_gallary` );
            
CREATE TABLE IF NOT EXISTS `t_gallary_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_gallary` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `body` longtext CHARACTER SET utf8,
  `order` int(11) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_gallary` (`id_gallary`,`title`),
  KEY `order` (`order`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


";
            $this->execute($sql);
	}

	public function down()
	{
		echo "m130720_201257_add_gallery does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}