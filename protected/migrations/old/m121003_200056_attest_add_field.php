<?php

class m121003_200056_attest_add_field extends CDbMigration
{
	public function up()
	{
		$sql = "ALTER TABLE  `t_gkh_zayavka` ADD  `attestation` VARCHAR( 128 ) NOT NULL COMMENT  'Аттестуется в качестве';";
		$this->execute($sql);
	}

	public function down()
	{
		echo "m121003_200056_attest_add_field does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}