<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
// class UserIdentity extends CUserIdentity
// {
// 	/**
// 	 * Authenticates a user.
// 	 * The example implementation makes sure if the username and password
// 	 * are both 'demo'.
// 	 * In practical applications, this should be changed to authenticate
// 	 * against some persistent user identity storage (e.g. database).
// 	 * @return boolean whether authentication succeeds.
// 	 */
// 	public function authenticate()
// 	{
// 		$users=array(
// 			// username => password
// 			'demo'=>'demo',
// 			'admin'=>'admin',
// 		);
// 		if(!isset($users[$this->username]))
// 			$this->errorCode=self::ERROR_USERNAME_INVALID;
// 		else if($users[$this->username]!==$this->password)
// 			$this->errorCode=self::ERROR_PASSWORD_INVALID;
// 		else
// 			$this->errorCode=self::ERROR_NONE;
// 		return !$this->errorCode;
// 	}
// }

class UserIdentity extends CUserIdentity
{
    private $_id;
    public $email;

    private $_role;

    public function getRole()
    {
        return $this->_role;
    }

    public function authenticate()
    {
    	//CVarDumper::dump ($this);

    	//echo 'email==='.$this->email;	
        $record=User::model()->findByAttributes(array('email'=>$this->email));
        if($record===null){
        	$this->errorCode=self::ERROR_USERNAME_INVALID;
        	echo 'error username';
        }
        else if($record->password!==md5($this->password)) { 
        	$this->errorCode=self::ERROR_PASSWORD_INVALID;
        	echo 'error pass';
        }
        else
        {
            $this->_id=$record->id;
            $this->setState('title', $record->username);
            $this->errorCode=self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
 
    public function getId()
    {
        return $this->_id;
    }

    public function getName()
    {
        return $this->username;
    }
    
    public function getUser()
    {
        return $this->user;
    }
 
    public function setUser(CActiveRecord $user)
    {
        $this->user=$user->attributes;
    }
}