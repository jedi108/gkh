<?php

class imageModelBehavior extends CActiveRecordBehavior {

    public $modelFileField;
    public $path = '/../file/tmp/';
    public $del_img;
    private $fieldData = null;
    private $uploadedFile;
    private $fileNameToSave;

    private function getFilePathFromModel() {
        return $this->owner->attributes[$this->modelFileField];
    }

    public function d($var, $die = false) {
        CVarDumper::dump($var, 100, 1);
        if ($die)
            die();
    }

    public function init() {
        
    }

    public function attach($owner) {
        $validators = $owner->getValidatorList();
        $validators->add(CValidator::createValidator('boolean', $owner, 'del_img'));
        parent::attach($owner);
    }

    private function isInPost() {
        return !$this->getFilePathFromModel() == '';
    }

    public function beforeSave($event) {

        $this->uploadedFile = CUploadedFile::getInstance($this->owner, $this->modelFileField);
        if (!is_null($this->uploadedFile)) {
            //die('checked---');
            $this->fileNameToSave = uuid::uuidgen() . '.' . $this->uploadedFile->extensionName;  // random number + file name
            $this->owner->{$this->modelFileField} = $this->fileNameToSave;

            $image = Yii::app()->basePath . $this->path . $this->fieldData;
            if (is_file($image))
                unlink($image);
        }
        else {
            $this->owner->{$this->modelFileField} = $this->fieldData;
        }
        //die();
        if ($this->owner->del_img == true) {
            //die('checked');
            $image = Yii::app()->basePath . $this->path . $this->getFilePathFromModel();
            if (is_file($image))
                unlink($image);
            $this->owner->{$this->modelFileField} = null;
        }
        parent::beforeSave($event);
    }

    public function afterFind($event) {
        parent::afterFind($event);
        //$this->owner->setAttributeLabel(array('del_img'=>'sssss'));
        //if(isset($_POST['News']))CVarDumper::dump($_POST['News'],100,1);
        $this->fieldData = $this->getFilePathFromModel();
        //$this->d($this->fieldData,false);
    }

    public function copyFile() {
        if (!is_null($this->uploadedFile)) {
            $this->uploadedFile->saveAs(Yii::app()->basePath . $this->path . $this->fileNameToSave);
        }
    }

    public function afterDelete() {
        parent::afterDelete();
        $image = Yii::app()->basePath . $this->path . $this->getFilePathFromModel();
        if (is_file($image))
            unlink($image);
    }

    public function afterSave($event) {
        $this->copyFile();
        parent::afterSave($event);
    }

}