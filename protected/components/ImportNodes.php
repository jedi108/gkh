<?php
class ImportNodes {
	private $book;
	private $direction;
	private $specializ;

	public function loads()
	{
		 $this->load_node(4);
		$this->load_node(23);
		$this->load_node(24);
		 $this->load_node(25);
		 $this->load_node(26);
		$this->load_node(27);
		 $this->load_node(28);
		 $this->load_node(29);
		 $this->load_node(30);
		 $this->load_node(31);
		 $this->load_node(32);
		 $this->load_node(33);
		
		$this->load_node(34);
		
		$this->load_node(35);
		$this->load_node(36);
		$this->load_node(37);
		$this->load_node(388);
		$this->load_node(389);
	}

	public static function view()
	{
		echo 'view nodes <br/>';
        $bid = 6;
        $podsh = 643;
        $where = array();   
        $where[':type'] =  'services';
  
        $result = db_query('SELECT nid, title FROM {node} WHERE   type = :type', $where);
        if($result){
            foreach($result as $o){
            	echo '='. $o->nid .'<br/>';
                // $node = node_load($o->nid);
                // $node->book[bid] = $bid;
                // $node->book[plid] = $podsh;
                //book_node_update($a);
            }
        } else {
        	echo 'error node load <br/>';
        }
	}

	public function deleteAll()
	{
		$sql = 'set foreign_key_checks=0; 
				TRUNCATE `t_cat_specializ`;
				TRUNCATE `t_direction`;
				TRUNCATE `t_specializ`;
				';
		
	}

	private function load_node($nid)
	{
		$this->book = null;

		$node = node_load($nid);

		if (isset($node->book)) {
			$this->book = $node;
			$this->direction = new Direction;
			$this->direction->name = $node->title;
			if ($this->direction->save()){
				$cont = $this->book_toc_recursive($node->book['mlid'], $node->nid, $node->book['mlid'], $node->title);
			} else {
				CVarDumper::dump($this->direction->errors, 100, true);
			}		
		} else {
			echo 'node '. $node->nid . ' is not book <br/>';
		}

		
	}

	private function book_toc_recursive ($mlid, $nid, $mlid_start, $tit) {
	        $c = ''; 
	        $content = '';
	        $sql= 'SELECT DISTINCT status, n.nid as nid, m.plid as plid, m.mlid as mlid,  n.title as title  
	                FROM {book} as b
	                inner join {menu_links} as m ON b.mlid = m.mlid
	                inner join {node} as n ON n.nid = b.nid
	                WHERE m.plid = :mlid AND status = 1
	                ORDER by m.weight, n.title
	                ';
	        $children = db_query ($sql, array(':mlid'=>$mlid) );
	 
	        if ($mlid != $mlid_start) {//не выводить самый первый уровень
	                $content .= '<li>' . l($tit, 'node/' . $nid, $attributes = array(), $query = NULL, $fragment = NULL, $absolute = FALSE, $html = FALSE ) .'</li>';
	        }
	        
	        foreach ($children as $child ) {
	                $c .= $this->book_toc_recursive($child->mlid, $child->nid, $mlid_start, $child->title);
	                //echo '<p>$child->title='. $child->title.'</p>';
	                $this->node_l($child->nid);
	        }
	 
	        if (strlen($c) > 2) {
	                $content .= '<ul>'. $c. '</ul>';
	        }
	    return $content;
	}

	private function verfield($field) {
		if (isset($field['und']['0']['value'])) 
			return $field['und']['0']['value'];
		else
			return '';
	}

	private function node_l($nid)
	{
		$this->specializ = new Specializ;
		//echo 'Book.'.$this->book->title .'<br/>';
	    $node = node_load($nid);

	    $this->specializ->id_f_direction=$this->direction->id;
	    $this->specializ->name = $node->title;

	    $this->specializ->id_org = null;
	    
	    //CVarDumper::dump($node->field_cost, 100, true);
	    $this->specializ->cost = $this->verfield($node->field_cost);

	    $this->specializ->times = $this->verfield($node->field_times);
	    $this->specializ->body = $this->verfield($node->body);

	    if ($this->specializ->save()) {
	    	//Добавление категорий

	    	if (isset($node->field_type_person['und'])) {
				foreach ($node->field_type_person['und'] as $key => $value) {
					$catspec = NULL;
					$catspec = new CatSpecializ;
		    		$catspec->id_t_specializ = $this->specializ->id;
					//CVarDumper::dump($value, 100, true);
					if (isset($value['tid'])) {
						switch ($value['tid']) {
							case '174': //Рабочий 1
								$catspec->id_t_cat=1;
							break;
							case '172': //Руководитель 2
								$catspec->id_t_cat=2;
							break;
							case '173': //Специалист 3
								$catspec->id_t_cat=3;
							break;
						}	
						if ($catspec->save()) {
							echo 'saved';
						} else {
							echo get_class($catspec);
							CVarDumper::dump($catspec->errors, 100, true);
						}
					}
				} 		
	    	}

			//Добавление дат
			$sdates = new SpecializDates;
			$sdates->id_t_specializ = $this->specializ->id;
			$sdates->date = $this->verfield($node->field_estimated_date); 	
			if ($sdates->save()) {
				echo 'saved';
			} else {
				//echo get_class($catspec);
				//CVarDumper::dump($catspec->errors, 100, true);
			}

	    } else {
	    	echo get_class($this->specializ);
	    	CVarDumper::dump($this->specializ->errors, 100, true);
	    }
 
	    /**
	     * @param VARDump
         */
	    /*
	    echo '<p>';
	    $this->fields($node->title, 'one', 'title');
	    $this->fields($node->field_type_person,'many','tid');   //Many
	    $this->fields($node->field_estimated_date,'many', 'value');//Many
	    $this->fields($node->field_cost, 'one', 'value');          //One
	    $this->fields($node->field_times, 'one', 'value');         //One
	    $this->fields($node->body, 'one', 'value');                //One
	    echo '</p>';
	    */
	}

	private function fields($field, $how, $field_name, $model_field='')
	{
	    
	    if ($field_name=='title') {
	        echo $field.', ';
	    }
	    else {
	        if (isset($field['und'])) {
	            switch ($how) {
	                case 'one':
	                    $val = $field['und']['0'][$field_name];
	                    //CVarDumper::dump($val, 100, true);
	                    echo $val;
	                break;
	                case 'many':
	                    foreach ($field['und'] as $key => $value) {
	                        //CVarDumper::dump($value, 100, true);
	                        echo $value[$field_name].',';
	                    }
	                    break;
	            }

	        }
	    }
	    
	}
}