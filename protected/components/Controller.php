<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	public $menus = array(
					array('label'=>'Главная', 'url'=>array('/Direction/index')),
					array('label'=>'О комбинате', 'url'=>array('/site/about')),
					array('label'=>'Лицензии', 'url'=>array('/Licenz/List'),'itemOptions'=>array('id'=>'lastLi')),
					array('label'=>'Услуги', 'url'=>array('/site/uslugi')),
					array('label'=>'Прайс', 'url'=>array('Specializ/Price')),
					array('label'=>'Договор', 'url'=>array('page/view?id=5')),
					array('label'=>'Заявки', 'url'=>array('zayavka/start')),
					array('label'=>'Партнеры', 'url'=>array('page/2')),
					array('label'=>'Новости', 'url'=>array('page/news')),
					array('label'=>'Контакты', 'url'=>array('page/7')),
				);
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
}

// class PhpMessageSource extends CPhpMessageSource
// {          
//     public function eventMissingTranslation(CMissingTranslationEvent $event)
//     {
//         if($event->language !== Yii::app()->sourceLanguage)
//         {
//             Yii::app()->language = Yii::app()->sourceLanguage;
//             $event->message = Yii::t($event->category, $event->message);
//             Yii::app()->language = $event->language;
//         }
//     }
// } 