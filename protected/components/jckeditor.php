<?php

class jckeditor extends CWidget
{
    public $model;
//    public function setModel($model)
//    {
//        $this->model=$model; return this;
//    }
    public function run()
    {

	$this->widget('ext.yiiext.widgets.ckeditor.ECKEditor',array(
		'model'=>$this->model,                # Data-Model (form model)
		'attribute'=>'body',         # Attribute in the Data-Model
		//'editorTemplate'=>'full',	
		//'editorTemplate' => 'advanced',
		//"toolbar"=>"basic",
		'skin'=>'office2003',
		'options'=>array(
//		'editorTemplate'=>'basic',		
//              'toolbar' => array(
//                    array('Bold','Italic','Underline','Strike','-','NumberedList','BulletedList','-','Outdent','Indent','Blockquote','-','Link','Unlink','-','Table','SpecialChar','-','Cut','Copy','Paste','-','Undo','Redo','-','Maximize',),
//              ),
                'filebrowserBrowseUrl'=>CHtml::normalizeUrl(array('site/browse')),
                'filebrowserImageBrowseUrl'=>CHtml::normalizeUrl(array('site/browse')),
                //'filebrowserUploadUrl'=>CHtml::normalizeUrl(array('site/browse')),
                'filebrowserUploadUrl'=>CHtml::normalizeUrl(array('site/file')),
            ),
	));
	
    }
}
