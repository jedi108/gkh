<?php

class HttpRequest extends CHttpRequest
{
	public function validateCsrfToken($event)
	{
		if($this->getIsPostRequest())
		{
			// only validate POST requests
			$cookies=$this->getCookies();
			
			//--Мои исправления--
			//var_dump(Yii::app()->request->getPathInfo());
				//exit;
			if ( Yii::app()->request->getPathInfo() == 'images/upload/ajaxupload' )
			{
				$access = isset($_GET[$this->csrfTokenName]);
			} else {
				$access = false;
			}
			//--
			
			if($cookies->contains($this->csrfTokenName) && ( isset($_POST[$this->csrfTokenName]) || $access ))
			{
				$tokenFromCookie=$cookies->itemAt($this->csrfTokenName)->value;
				$tokenFromPost= ( $access ? $_GET[$this->csrfTokenName] : $_POST[$this->csrfTokenName] );
				$valid=$tokenFromCookie===$tokenFromPost;
			}
			else
				$valid=false;
			if(!$valid)
				throw new CHttpException(400,Yii::t('yii','The CSRF token could not be verified.'));
		}
	}
}
