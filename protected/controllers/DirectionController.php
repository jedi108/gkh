<?php
class DirectionController extends Controller
{
	public $layout='/layouts/column2';
	public function actionView($id)
	{
		$direct = Direction::model()->findByPk($id);
		
		//$catDirSpec = Direction::getDirectionsCat($id);
		//$this->render('view', array('catDirSpec'=>$catDirSpec, 'direct'=>$direct, 'id'=>$id));


		$specializ = $direct->specializ;
		$catDirSpec=array();
		$i=0;
		$k=0;
		foreach ($specializ as $key => $value) {
			if($value['id_cat'] != $k) {$i=0;$k=$value['id_cat'];}
			$catDirSpec[$value['id_cat']][$i]['id']=$value['id'];
			$catDirSpec[$value['id_cat']][$i]['name']=$value['name'];
			$i++;
		}
		//print_r ($catDirSpec);
                Yii::app()->user->setState('direction_name',$direct->name);
                Yii::app()->user->setState('direction_id',$direct->id);
                
		$this->render('view', array('catDirSpec'=>$catDirSpec, 'direct'=>$direct, 'id'=>$id));

	}

	public function actionIndex()
	{
		$direct = Direction::model()->findAll();
		$news=new CActiveDataProvider('News',array(
			'criteria'=>array(
        		'condition' => 'type=0'
        )));
		$this->render('indexDirection', array('direct'=>$direct, 'news'=>$news));
	}
}