<?php

class AdmDirectionController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new Direction('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Direction']))
            $model->attributes=$_GET['Direction'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

	public function actionCreate()
    {
        $model=new Direction;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Direction']))
        {
            $model->attributes=$_POST['Direction'];
            if($model->save())
                $this->redirect(array('Direction/view','id'=>$model->id));
        }

        $this->render('_formDirection',array(
            'model'=>$model,
        ));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Direction']))
        {
            $model->attributes=$_POST['Direction'];
            if($model->save())
                if (isset($_POST['Relation']))
                    Relation::saveRelation($model->id, $_POST['Relation']['id_specializ']);
                else
                    Relation::saveRelation($model->id, array());
                $this->redirect(array('Direction/view','id'=>$model->id));
        }

        $this->render('_formDirection',array(
            'model'=>$model,
        ));
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='direction-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function loadModel($id)
    {
        $model=Direction::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            // array('deny',
            //     'actions'=>array('admin'),
            //     'users'=>array('?'),
            // ),
            array('allow',
                'actions'=>array('Admin','Delete', 'Create','Update'),
                //'roles'=>array('sex'),
                //'users'=>array('vadimka108@gmail.com'),
                'expression'=>'Yii::app()->user->role=="sex"',
            ),
            array('deny',
                'actions'=>array('Admin','Delete', 'Create','Update'),
                'users'=>array('*'),
            ),
        );
    }

}