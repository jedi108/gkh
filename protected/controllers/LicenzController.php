<?php

class LicenzController extends Controller
{
	public $layout='/layouts/column2';
	public function actionList()
	{
		$model = Licenz::model()->findAll();
		$this->render('lic', array('model'=>$model));
	}

	public function actionLic($id)
	{
		//Yii::app()->clientScript->scriptMap['*.js'] = false;
		$model = Licenz::model()->findByPk($id);

		$this->renderPartial('_lic', array('model'=>$model));
	}

}