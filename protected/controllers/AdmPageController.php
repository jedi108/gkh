<?php
class AdmPageController extends Controller
{
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model=Page::model()->with('jfiles')->findByPk($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	public function actionAddPage()
	{
		$model=new Page;
		//$xupload = new XUploadForm;
		//$this->performAjaxValidation( $model );
		if(isset($_POST['Page'])) {
			$model->attributes=$_POST['Page'];
			if ($model->save()) {

	        } else $this->redirect(array("/site/error"));	
	        $this->redirect( Yii::app()->createUrl('page/view', array('id'=>$model->id)) );
		}
		$this->render('_formPage', array(
			'model'=>$model, //'jFiles'=>$jFiles
			//'xupload'=>$xupload
			));
	}

	public function actionEditPage($id)
	{
		//$xupload = new XUploadForm;
		$model=Page::model()->with('jfiles')->findByPk($id);
		//$jFiles=jFiles::model()->find('id_page=:id_page', array('id_page'=>$model->id));
		//$this->performAjaxValidation( $model );
		if(isset($_POST['jFiles'])) {
			jFiles::echoVar($_POST['jFiles'], $model->id);	
			//Yii::app()->end();	
		} else 
			jFiles::echoVar(array(), $model->id);	
		
		if(isset($_POST['Page'])) {
			$model->attributes=$_POST['Page'];
			if ($model->save()) {

	        } else $this->redirect(array("site/error"));	
	        $this->redirect( Yii::app()->createUrl('page/view', array('id'=>$model->id)) );
		}
		$this->render('_formPage', array(
			'model'=>$model, //'jFiles'=>$jFiles
			//'xupload'=>$xupload
			));
	}

	public function actionAdmin()
	{
		$model=new Page('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Page']))
			$model->attributes=$_GET['Page'];

		if(Yii::app()->request->isAjaxRequest) 
			$this->renderPartial('adminPage', array('dataProvider'=>$model));
		else
			$this->render('adminPage', array('dataProvider'=>$model));

	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='Page')
		{
			echo CActiveForm::validate($model);
			//echo 'true';
			Yii::app()->end();
		}
	}

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
    {
        return array(
            // array('deny',
            //     'actions'=>array('admin'),
            //     'users'=>array('?'),
            // ),
            array('allow',
                'actions'=>array('Admin','DeletePage', 'EditPage','AddPage'),
                //'roles'=>array('sex'),
                //'users'=>array('vadimka108@gmail.com'),
                'expression'=>'Yii::app()->user->role=="sex"',
            ),
            array('deny',
                'actions'=>array('Admin','DeletePage', 'EditPage','AddPage'),
                'users'=>array('*'),
            ),
        );
    }
}
?>
