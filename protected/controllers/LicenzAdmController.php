<?php
class LicenzAdmController extends Controller
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('Admin', 'Create','Update', 'Delete','view'),
                'expression'=>'Yii::app()->user->role=="sex"',
            ),
            array('deny',
                'actions'=>array('Admin', 'Create','Update', 'Delete','view'),
                'users'=>array('*'),
            ),
        );
    }


 

	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Licenz('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Licenz']))
			$model->attributes=$_GET['Licenz'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='org-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Licenz;
		//$model->_z_type = 1;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Licenz']))
		{
			$model->attributes=$_POST['Licenz'];
			if($model->save())
				$this->redirect(array('/Licenz/view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Licenz']))
		{
			$model->attributes=$_POST['Licenz'];
			if($model->save()) {
				//$this->redirect(array('/Licenz/view','id'=>$model->id));
				$this->redirect( Yii::app()->createUrl('/LicenzAdm/view', array('id'=>$model->id)) );
				Yii::app()->end();
				
			}
			else {
				CVarDumper::dump($model->errors,100,1);
				Yii::app()->end();
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}



	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Licenz::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	

    public function actionView($id)
    {
    	$model = Licenz::model()->findByPk($id);
    	if(Yii::app()->getRequest()->getIsAjaxRequest()) {
    		$this->renderPartial('view',array(
				'model'=>$model, 'css'=>false
			));
    	} else {
    		$this->render('view',array(
				'model'=>$model, 'css'=>true
			));
    	}
    }		

}