<?php
class OrgAdmController extends Controller
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('Admin','AdminZayavki','AdminZayavkiPrint', 'Create','Update', 'Delete','view'),
                'expression'=>'Yii::app()->user->role=="sex" || Yii::app()->user->role=="admin"',
            ),
            array('deny',
                'actions'=>array('Admin','AdminZayavkiPrint','AdminZayavki', 'Create','Update', 'Delete'),
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id)
    {
    	$model = Org::model()->findByPk($id);
    	if(Yii::app()->getRequest()->getIsAjaxRequest()) {
    		$this->renderPartial('viewzayavka',array(
				'model'=>$model, 'css'=>false
			));
    	} else {
    		$this->render('viewzayavka',array(
				'model'=>$model, 'css'=>true
			));
    	}
    }

    public function actionTest()
    {
        if(Yii::app()->hasModule('GkhZayavki')) echo 'Существует';
        else echo 'не сущетсвует';
    }

	public function actionAdminZayavki()
	{
		$model=new Org('searchDao');
		if (isset($_GET['Org'])) $model->attributes = $_GET['Org'];
		$this->render('adminZayavki', array('SqldataProvider'=>$model));
	}

	public function actionAdminZayavkiPrint()
	{
		$filename = Yii::getPathOfAlias('webroot.test').'/file/tmp/my.csv';
		$sql = "SELECT groups.num, profile.lastname, profile.firstname, profile.middlename, org.name_small, cat.name_cat, specializ.name
				FROM t_gkh_zayavka AS zayavka
				LEFT JOIN t_profile AS profile ON ( zayavka.id_profile = profile.id ) 
				LEFT JOIN t_specializ AS specializ ON ( zayavka.id_specializ = specializ.id ) 
				LEFT JOIN t_education AS education ON ( profile.id = education.id_profile ) 
				LEFT JOIN t_org AS org ON ( profile.id_org = org.id ) 
				LEFT JOIN t_gkh_zayavka_group AS groups ON ( groups.id = zayavka.id_zayavka ) 
				LEFT JOIN t_cat AS cat ON ( zayavka.id_cat = cat.id ) 
				ORDER BY groups.num DESC ";
		
		$provider = Yii::app()->db->createCommand($sql);
		Yii::import('ext.ECSVExport.ECSVExport');
		$csv = new ECSVExport($provider);
		$content = $csv->toCSV();                   
		Yii::app()->getRequest()->sendFile($filename, $content, "text/csv", false);
		exit();
	}

	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Org('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Org']))
			$model->attributes=$_GET['Org'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='org-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Org;
		//$model->_z_type = 1;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Org']))
		{
			$model->attributes=$_POST['Org'];
			if($model->save())
				$this->redirect(array('/Org/view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Org']))
		{
			$model->attributes=$_POST['Org'];
			if($model->save())
				$this->redirect(array('/Org/view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}



	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Org::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}