<?php

class JmenuController extends Controller
{
	public $CQtreeGreedView  = array (
        'modelClassName' => 'Jmenu', //название класса
        'adminAction' => 'admin' //action, где выводится QTreeGridView. Сюда будет идти редирект с других действий.
    );
	public $tree=array();
	public function actions() {
        return array (
            'create'=>'ext.QTreeGridView.actions.Create',
            'update'=>'ext.QTreeGridView.actions.Update',
            'delete'=>'ext.QTreeGridView.actions.Delete',
            'moveNode'=>'ext.QTreeGridView.actions.MoveNode',
            'makeRoot'=>'ext.QTreeGridView.actions.MakeRoot',
        );
    }	
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	
    public function accessRules()
    {
        return array(
            // array('deny',
            //     'actions'=>array('admin'),
            //     'users'=>array('?'),
            // ),
            array('allow',
                'actions'=>array('Admin','Delete', 'Create','Update','moveNode','makeRoot','view'),
                //'roles'=>array('sex'),
                //'users'=>array('vadimka108@gmail.com'),
                'expression'=>'Yii::app()->user->role=="sex"',
            ),
            array('deny',
                'actions'=>array('Admin','Delete', 'Create','Update'),
                'users'=>array('*'),
            ),
        );
    }
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}



	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Jmenu;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Jmenu']))
		{
			$rootID = $_POST['Jmenu']['root'];
			unset($_POST['Jmenu']['root']);
			$model->attributes=$_POST['Jmenu'];
			// if (!$root)
				// $root=Jmenu::model()->find();
			$root=Jmenu::model()->find('root=:rootID', array(':rootID'=>$rootID));
			if($model->appendTo($root))
				$this->redirect('/jmenu/admin');
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		//unset($_POST['Jmenu']['root']);
		//print_r ($_POST);
		$category=$model->findByPk($id);
		if (!$parent=$category->isRoot()) {
			$parent=$category->getparent();
			$model->root = isset($_POST['Jmenu']['root']) ? intval($_POST['Jmenu']['root']) : $parent->root;
		} else {
			$model->root = isset($_POST['Jmenu']['root']) ? intval($_POST['Jmenu']['root']) : $id;
		}
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Jmenu']))
		{
			$model->attributes=$_POST['Jmenu'];
			if($model->saveNode());
				$this->redirect('/jmenu/admin');
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

//	/**
//	 * Deletes a particular model.
//	 * If deletion is successful, the browser will be redirected to the 'admin' page.
//	 * @param integer $id the ID of the model to be deleted
//	 */
//	public function actionDelete($id)
//	{
//		if(Yii::app()->request->isPostRequest)
//		{
//			// we only allow deletion via POST request
//			$this->loadModel($id)->delete();
//
//			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//			if(!isset($_GET['ajax']))
//				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//		}
//		else
//			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
//	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Jmenu');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Jmenu('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Jmenu']))
			$model->attributes=$_GET['Jmenu'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Jmenu::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='jmenu-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
