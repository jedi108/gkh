<?php

class PageController extends Controller
{
	public $layout='/layouts/column2';

	public function actionView($id)
	{
        $this->getIsAccess($id);

		$model = Page::model()->with('jfiles')->findByPk($id);
        if (count($model) == 1)
            $this->render('view', array('model' => $model));
        else {
            $model = News::model()->findByPk($id);
            if (count($model) == 1)
                $this->render('redirect', array('model' => $model));
            throw new CHttpException(404, 'Указанная запись не найдена');
        }
	}

	public function actionNews()
	{
		$criteria = new CDbCriteria();
		$criteria->condition='type=1';
		$criteria->order='date_created DESC';
		$count=Page::model()->count($criteria);

		$pages=new CPagination($count);
		$pages->pageSize=50;
		$pages->applyLimit($criteria);

		$models=Page::model()->findAll($criteria);

		$this->render('indexNews',array(
			'models'=>$models,
			'pages'=>$pages,
		));
	}

    private function getIsAccess($id) {
        if ((Yii::app()->user->isGuest) && ($id=='70')) {
            Yii::app()->controller->redirect(array('user/login'));
        }
    }
}