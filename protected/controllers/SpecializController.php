<?php

class SpecializController extends Controller
{
	function actionPrice()
	{
		$model=new Specializ('searchDao');
		if (isset($_GET['Specializ'])) {
			$model->attributes=$_GET['Specializ'];
			// CVarDumper::dump($model->attributes,100,true);
			// echo '=============';
			// CVarDumper::dump($_GET['Specializ'],100,true);
			// echo '=============';
		}
		if(Yii::app()->request->isAjaxRequest) 
			$this->renderPartial('application.views.admSpecializ.admin', array('SqldataProvider'=>$model));
		else
			$this->render('application.views.admSpecializ.admin', array('SqldataProvider'=>$model));
	}	

	public function actionView($id)
	{
		$this->layout='/layouts/column2';
		$model=Specializ::model()->with(
				'specializDates'
				,'categories'
				//,'idFDirection'
				)->findByPk($id);
		$this->render('viewSpecizaliz', array('mSpecializ'=>$model));

	}
}