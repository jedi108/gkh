<?php

class SiteController extends Controller
{
	public $layout='/layouts/column2';
	
     public function actionFile()
    {
        //номер функции обратного вызова
        $callback = $_GET['CKEditorFuncNum'];
          //имя фалйа
           $file_name = $_FILES['upload']['name'];
           //временное имя файла на сервере
           $file_name_tmp = $_FILES['upload']['tmp_name'];
           //указываем куда складывать изображения
           $file_new_name = 'file/';
           //формируем полный путь к изображению
           $full_path = $file_new_name.$file_name;
           //формируем адрес для атрибута src тега img
           $http_path = '/file/'.$file_name;
           $error = '';
           if( move_uploaded_file($file_name_tmp, $full_path) ) {} 
           else
           {
               $error = 'Some error occured please try again later';
               $http_path = '';
            }
            echo "<script type=\"text/javascript\">
                 window.parent.CKEDITOR.tools.callFunction(".$callback.",  \"".$http_path."\", \"".$error."\" );
             </script>";
    }
	
	public function actionLic()
	{
		$this->render('lic');
	}
	public function actionBrowse() {
		// я создал специально отдельный пустой лейаут
		$this->layout='//layouts/clear';
		$this->render('browser');
	}
	public function actionRegistration()
	{
	        // тут думаю все понятно
	        $form = new User();

	        // Проверяем являеться ли пользователь гостем
	        // ведь если он уже зарегистрирован - формы он не должен увидеть.
	        if (!Yii::app()->user->isGuest) {
	             throw new CException('Вы уже зарегистрированны!');
	        } else {
	            // Если $_POST['User'] не пустой массив - значит была отправлена форма
	            // следовательно нам надо заполнить $form этими данными
	             // и провести валидацию. Если валидация пройдет успешно - пользователь
	            // будет зарегистрирован, не успешно - покажем ошибку на экран
	            if (!empty($_POST['User'])) {
	                
	                 // Заполняем $form данными которые пришли с формы
	                $form->attributes = $_POST['User'];
	                
	                // Запоминаем данные которые пользователь ввёл в капче
	                 $form->verifyCode = $_POST['User']['verifyCode'];
	                
	                    // В validate мы передаем название сценария. Оно нам может понадобиться
	                    // когда будем заниматься созданием правил валидации [читайте дальше]
	                     if($form->validate('registration')) {
	                        // Если валидация прошла успешно...
	                        // Тогда проверяем свободен ли указанный логин..

	                            if ($form->model()->count("email = :email", array(':email' => $form->email))) {
	                                 // Указанный логин уже занят. Создаем ошибку и передаем в форму
	                                $form->addError('email', 'Email уже занят');
	                                $this->render("registration", array('form' => $form));
	                             } else {
	                             	$form->real = $form->password;
	                             	$form->password = md5($form->password);
	                                // Выводим страницу что "все окей"
	                                if ($form->save())
	                                	$this->render("registration_ok");
	                                else
	                                	$this->render('error', $error);
	                            }
	                                             
	                    } else {
	                        // Если введенные данные противоречат 
	                        // правилам валидации (указаны в rules) тогда
	                        // выводим форму и ошибки.
	                         // [Внимание!] Нам ненадо передавать ошибку в отображение,
	                        // Она автоматически после валидации цепляеться за 
	                        // $form и будет [автоматически] показана на странице с 
	                         // формой! Так что мы тут делаем простой рэндер.
	                        
	                        $this->render("registration", array('form' => $form));
	                    }
	             } else {
	                // Если $_POST['User'] пустой массив - значит форму некто не отправлял.
	                // Это значит что пользователь просто вошел на страницу регистрации
	                // и ему мы должны просто показать форму.
	                 
	                $this->render("registration", array('form' => $form));
	            }
	        }
	 }

    // public function init()
    // {
    //     parent::init();
    //     Yii::app()->urlManager->setAppLanguage();
    // } 
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
// Обработчик сообщений от файл-менеджера
            'fileManager'=>array(
                'class'=>'ext.yiiext.widgets.elfinder.ElFinderAction',
            ),
			
			'upload' => array('class' => 'xupload.actions.XUploadAction', 'path' => Yii::app() -> getBasePath() . "/../images/uploads", "publicPath" => Yii::app()->getBaseUrl()."/images/uploads" ),
			
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
				'foreColor'=>'#000000',
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionAbout()
	{
		//О нас
		$model = Page::model()->findByPk('1');
		$this->render('application.views.page.view', array('model'=>$model));
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		//О нас
		//$model = Page::model()->findByPk('1');
		//$this->render('application.views.page.view', array('model'=>$model));
		$direct = Direction::model()->findAll();
		$news=new CActiveDataProvider('News',array(
			'criteria'=>array(
        		'condition' => 'type=0'
        )));
		$this->render('application.views.direction.indexDirection', array('direct'=>$direct,'news'=>$news));		
	}

	public function actionUslugi()
	{
		//О нас
		//$model = Page::model()->findByPk('1');
		//$this->render('application.views.page.view', array('model'=>$model));
		$direct = Direction::model()->findAll();
		$this->render('uslugi', array('direct'=>$direct));		
	}

	public function actionOar()
	{
		//О нас
		//$model = Page::model()->findByPk('1');
		//$this->render('application.views.page.view', array('model'=>$model));
		$this->render('oar', array());		
	}
	public function actionOros()
	{
		//О нас
		//$model = Page::model()->findByPk('1');
		//$this->render('application.views.page.view', array('model'=>$model));
		$this->render('oros', array());		
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    } else $this->render('error', $error);
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact($isAbout = true)
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact',Yii::t('site', 'Thank you for contacting'));
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model, 'isAbout'=>$isAbout,));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionTest()
	{
		$data=array();
		$treeList1=Jmenu::model()->findAll();
		foreach ($treeList1 as $i=>$model) {
		  $data[$i]=$this->formatData($model);
		  $treeList2=$model->children();
		  foreach ($treeList2 as $j=>$submodel) {
			$data[$i]['children'][]=$this->formatData($submodel);
		  };
		};		
		$this->render('__test', array('data'=>$data));
	}
  protected function formatData(&$tree)
  {
	  return array('url'=>'#');
    if($tree->children())
      return array('text'=>CHtml::link($tree->name,"#"),'id'=>$tree->id,'expanded'=>true);
    else
      return array('text'=>CHtml::link($tree->name,"#"),'id'=>$tree->id);
  }
 	
}