<?php

class AdmSpecializController extends Controller {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            // array('deny',
            //     'actions'=>array('admin'),
            //     'users'=>array('?'),
            // ),
            array('allow',
                'actions' => array('Admin', 'DeleteSpecializ', 'EditSpecializ', 'AddSpecializ'),
                //'roles'=>array('sex'),
                //'users'=>array('vadimka108@gmail.com'),
                'expression' => 'Yii::app()->user->role=="sex"',
            ),
            array('deny',
                'actions' => array('Admin', 'DeleteSpecializ', 'EditSpecializ', 'AddSpecializ'),
                'users' => array('*'),
            ),
        );
    }

    public function actionAdmin() {

        $model = new Specializ('searchDao');
        if (isset($_GET['Specializ'])) {
            $model->attributes = $_GET['Specializ'];
            // CVarDumper::dump($model->attributes,100,true);
            // echo '=============';
            // CVarDumper::dump($_GET['Specializ'],100,true);
            // echo '=============';
        }
        if (Yii::app()->request->isAjaxRequest)
            $this->renderPartial('admin', array('SqldataProvider' => $model));
        else
            $this->render('admin', array('SqldataProvider' => $model));
    }

    public function actionDeleteSpecializ($id) {
        //$modelVerify = GkhZayavka::model()->findAll('id_specializ=:id_specializ',array(':id_specializ'=>$id));
        if (false) { //(count($modelVerify)>0)
            echo 'Невозможно удалить, есть связанные заявки';
        } else {
            CatSpecializ::model()->deleteAll('id_t_specializ=:id_t_specializ', array(':id_t_specializ' => $id));
            $model = Specializ::model()->findByPk($id);
            $model->delete();
        }

        //SpecializDates::delAll($model->id);
        //$model->delete();
    }

    public function actionEditSpecializ($id) {
        $model = Specializ::model()->with('specializDates')->findByPk($id);
        $this->performAjaxValidation($model);

        if (isset($_POST['Specializ'])) {
            //unset ($_POST['Specializ']['id_cat']);
            $model->attributes = $_POST['Specializ'];
            if ($model->save()) {
                if (isset($_POST['SpecializDates']))
                    SpecializDates::saveDates($_POST['SpecializDates'], $model->id);
                else
                    SpecializDates::delAll($model->id);

                if (isset($_POST['cat']))
                    CatSpecializ::saveSelection($_POST['cat'], $model->id);
                else
                    CatSpecializ::delAll($model->id);
                if (isset($_POST['Relation']))
                    Relation::saveRelationDirections($model->id, $_POST['Relation']['id_directions']);
                else
                    Relation::saveRelationDirections($model->id, array());
            }
            else
                $this->redirect(array("site/error"));
            $this->redirect(Yii::app()->createUrl('Specializ/view', array('id' => $model->id)));
        }
        $this->render('_formSpecializ', array('model' => $model));
    }

    public function actionAddSpecializ() {
        $model = new Specializ;
        $this->performAjaxValidation($model);

        if (isset($_POST['Specializ'])) {
            $model->attributes = $_POST['Specializ'];
            if ($model->save()) {
                if (isset($_POST['SpecializDates']))
                    SpecializDates::saveDates($_POST['SpecializDates'], $model->id);
                else
                    SpecializDates::delAll($model->id);

                if (isset($_POST['cat']))
                    CatSpecializ::saveSelection($_POST['cat'], $model->id);
                else
                    CatSpecializ::delAll($model->id);
            }
            else
                $this->redirect(array("site/error"));
            $this->redirect(Yii::app()->createUrl('Specializ/view', array('id' => $model->id)));
        }
        $this->render('_formSpecializ', array('model' => $model));
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'specializ-_formSpecializ-form') {
            echo CActiveForm::validate($model);
            //echo 'true';
            Yii::app()->end();
        }
    }

}