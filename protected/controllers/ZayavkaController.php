<?php
/**
 * Заявки
 */
class ZayavkaController extends Controller
{
	private $promt;
	private $isSave=true;
	public $layout='/layouts/column2';
	/**
	 * Тип заявки, только для организаций и физ лица
	 * @var type string 
	 */
	public $typeOfZayavka = 'profile';
	
	public $zTitleProfile = 'Форма заявки для частных лиц';

	public function actionView($id,$print=false)
	{
		
		$this->layout=($print)? '//layouts/column1': '/layouts/column1';
		$modGkhz = GkhZayavkaGroup::model()->findByPk($id);
		$modOrg = Org::model()->findByPk($modGkhz->org->id);
		if(is_null($modGkhz)) 
			throw new CHttpException(404, 'not found');
		$this->render('Zview', array('modGkhz'=>$modGkhz, 'modOrg'=>$modOrg,'print'=>$print));
	}

	public function actionAdmin()
	{
		$modGkhz = new GkhZayavkaGroup('search');
		if(isset($_GET['GkhZayavkaGroup']))
			$model->attributes=$_GET['GkhZayavkaGroup'];
		$this->render('Zadmin', array('modGkhz'=>$modGkhz));
	}

	/**
	 * Выбор типа заявки
	 */
	public function actionStart()
	{
		$this->render('start');
	}

	public function actionFinish()
	{
		$this->render('finish');
	}

	/**
	 * Сохранение организации и её сотрудников в качестве заявок на обучение
	 */
	public function actionAddOrgAjax()
	{
		$err = array();

		if (isset($_POST['Profile'])) {
			foreach ($_POST['Profile'] as $key => $value) {
				$profiles[] = new Profile;
				$err[] = json_decode(CActiveForm::validateTabular(  $profiles ));
			}
		}
		if (isset($_POST['Education'])) {
			foreach ($_POST['Education'] as $key => $value) {
				$Educations[] = new Education;
				$err[] = json_decode(CActiveForm::validateTabular(  $Educations ));
			}
		}
		if (isset($_POST['GkhZayavka'])) {
			foreach ($_POST['GkhZayavka'] as $key => $value) {
				$GkhZayavka[] = new GkhZayavka;
				$err[] = json_decode(CActiveForm::validateTabular(  $GkhZayavka ));
			}
		}
		if (isset($_POST['Org'])) {
			$org = new Org;
			$org->_z_type = ($this->typeOfZayavka == 'org')? 1 : 0 ; //Заявка от ЮР ЛИЦА, ЗАЯВКА от Физлица
			$org->attributes = $_POST['Org'];
			if (!$org->validate()) 
				$err[] = $org->errors;
		}
		if (isset($_POST['GkhZayavkaGroup'])) {
			$zGroup = new GkhZayavkaGroup;
			$zGroup->attributes = $_POST['GkhZayavkaGroup'];
			if (!$zGroup->validate()) 
				$err[] = $zGroup->errors;
			//CVarDumper::dump($zGroup->errors,100,0);
		}
		$this->promt = array();
		foreach ($err as $key => $value) {
			foreach ($value as $inkey => $invalue) {
				foreach ($invalue as $toinkey => $toinvalue) {
					$this->promt[$toinvalue] = $toinvalue;
				}
				
			}
		}
		if ($this->promt===array()) {
//			CVarDumper::dump($promt,100,0);
//			die();
			$transaction = Yii::app()->getDb()->beginTransaction();
			
			try {
					$this->savePromt($org);
					$zGroup->id_org = $org->id;
					$this->savePromt($zGroup);
					foreach ($_POST['Profile'] as $key => $profile_one) {
						$pro = new Profile;
						$pro->attributes=$profile_one;
						$pro->id_org = $org->id;
						$this->savePromt($pro);

						$proLink = new ProfileOrg;
						$proLink->type_link = 1; //Сотрудник
						$proLink->id_org=$org->id;
						$proLink->id_profile=$pro->id;
						$this->savePromt($proLink);

						foreach ($_POST['GkhZayavka'] as $key => $GkhZayavka_one) {
							//Данные о заявке
							$gkhZ=new GkhZayavka;
							$gkhZ->attributes=$GkhZayavka_one;
							$gkhZ->id_org = $org->id;
							$gkhZ->id_profile = $pro->id;
							$this->savePromt($gkhZ);
						}
						
						$postE = $_POST['Education'][$key];
						$edu = new Education;
						$edu->attributes=$postE;
						$edu->id_profile = $pro->id;
						$this->savePromt($edu);
					}
					if($this->isSave) 
						$transaction->commit();
					else
						$transaction->rollBack();
	 
 
			} catch(Exception $e) {
				$transaction->rollBack();
				//$this->promt[] = $e.'<br/>';
			}
		} 
		if ($this->promt===array()) 
			echo json_encode(array('status'=>'200', 'redirect'=>'/zayavka/finish'));
		else 
			//CVarDumper::dump($this->promt,100,1);
			echo json_encode(implode($this->promt, '<br/>'));
	}

	
	public function actionAddOrg()
	{
		$this->typeOfZayavka = 'org'; 
		$this->echoForm('orgFormZayavka');
	}

	public function actionAddProfile()
	{
		$this->typeOfZayavka = 'profile'; 
		$this->echoForm();
	}
	/**
	 * Форма заявки для всех типов
	 */
	private function echoForm($scenario=null)
	{
		$gkhZG = new GkhZayavkaGroup;
		if(isset($_POST['GkhZayavkaGroup'])) {
			switch ($_POST['GkhZayavkaGroup']['type']) {
				case 1:  //Юр лицо
					$scenario='orgFormZayavka';
					$gkhZG->type=1;
					break;
				case 0:  //Физ лицо
					$gkhZG->type=0;
					break;
			}
		}
		//$this->typeOfZayavka = 'org'; //Заявка для организации
 
		$gkhZ = new GkhZayavka;
		$modelOrg = (is_null($scenario))? new Org: new Org($scenario);
		$gkhZG->type = ($this->typeOfZayavka == 'org')? 1 : 0 ; //Заявка от ЮР ЛИЦА, ЗАЯВКА от Физлица
		$modelOrg->_z_type = ($this->typeOfZayavka == 'org')? 1 : 0 ; //Заявка от ЮР ЛИЦА, ЗАЯВКА от Физлица
		$modelProfile = new Profile;
		$modelProfileRuk = new Profile;
		$modelProfileBuh = new Profile;
		$modelProfileZay = new Profile;
		$educat = new Education;
		$modelOrg->_org_type = 0;
		
		$id_specializ = Yii::app()->request->getParam( 'id_specializ' );
		$gkhZ->id_specializ =  $id_specializ;

		if(Yii::app()->getRequest()->getIsAjaxRequest()) {
			$this->defAjax($modelOrg);
		}
		
		if (isset($_POST['Profile'])) {
			$this->realSave();
		}
		
		$this->render('zayavka', array(
			'modelProfile'=>$modelProfile,
			'modelProfileRuk'=>$modelProfileRuk,
			'modelProfileBuh'=>$modelProfileBuh,
			'modelProfileZay'=>$modelProfileZay,
			'educat'=>$educat, 
			'modelOrg'=>$modelOrg, 
			'gkhZ'=>$gkhZ,
			'gkhZG'=>$gkhZG,
		));
	}
	
	public function realSave()
	{
			$transaction = Yii::app()->getDb()->beginTransaction();
			$org = new Org;
			$org->_z_type = ($this->typeOfZayavka == 'org')? 1 : 0 ; //Заявка от ЮР ЛИЦА, ЗАЯВКА от Физлица
			$org->attributes = $_POST['Org'];
			try {
					$this->savePromt($org);
					$zGroup = new GkhZayavkaGroup;
					$zGroup->type =$_POST['GkhZayavkaGroup']['type'];
					$zGroup->id_org = $org->id;
					$this->savePromt($zGroup);
					foreach ($_POST['Profile'] as $key => $profile_one) {
						$pro = new Profile;
						$pro->attributes=$profile_one;
						$pro->id_org = $org->id;
						$this->savePromt($pro);


						$type_link = 1;
						if(($key === 'buh') || ($key==='ruk') || ($key==='zay')) {
							if($key == 'buh') $type_link = 2;
							if($key == 'zay') $type_link = 3;
							if($key == 'ruk') $type_link = 0;
						} else {
							$GkhZayavka_one=$_POST['GkhZayavka'][$key];
							$gkhZ=new GkhZayavka;
							$gkhZ->attributes=$GkhZayavka_one;
							$gkhZ->id_org = $org->id;
							$gkhZ->id_profile = $pro->id;
							$gkhZ->id_zayavka=$zGroup->id;
							$this->savePromt($gkhZ);
							//}
 

							$postE = $_POST['Education'][$key];
							$edu = new Education;
							$edu->attributes=$postE;
							$edu->id_profile = $pro->id;
							//$edu->qualification='ssssssssss';
							$this->savePromt($edu);
						}

							
							
						$proLink = new ProfileOrg;
						$proLink->type_link = $type_link; //Сотрудник
						$proLink->id_org=$org->id;
						$proLink->id_profile=$pro->id;
						$this->savePromt($proLink);
						
						

					}
					if($this->isSave) {
						$transaction->commit();
						$this->redirect(array('finish','id'=>1));
					}
					else {
						$transaction->rollBack();
						//CVarDumper::dump($this->promt,100,1);
					}
						
	 
 
			} catch(Exception $e) {
				$transaction->rollBack();
				//CVarDumper::dump($this->promt,100,1);
			}	
			//CVarDumper::dump($this->promt,100,1);
	}
	
	public function defAjax($model)
	{
			if (isset($_POST['Profile'])) {
				foreach ($_POST['Profile'] as $key => $value) {
					$profiles[$key] = new Profile;
				}
			}
			if (isset($_POST['Education'])) {
				foreach ($_POST['Education'] as $key => $value) {
					$Educations[$key] = new Education;
					//$err[] = json_decode(CActiveForm::validateTabular(  $Educations ));
				}
			}			
			if (isset($_POST['GkhZayavka'])) {
				foreach ($_POST['GkhZayavka'] as $key => $value) {
					$GkhZayavka[$key] = new GkhZayavka;
					//$err[] = json_decode(CActiveForm::validateTabular(  $GkhZayavka ));
				}
			}
			
			$err_zay = json_decode(CActiveForm::validateTabular( $GkhZayavka ), true);
			$err_edu = json_decode(CActiveForm::validateTabular( $Educations ), true);
		    $err_profile = json_decode(CActiveForm::validateTabular( $profiles ), true);
			$err_org = json_decode(CActiveForm::validate($model), true);
			
			$err = array_merge($err_profile, $err_org, $err_edu, $err_zay);
 
			//CVarDumper::dump ($err,100,0);
			echo json_encode($err);
			
		    Yii::app()->end();
	}



	protected function setErr($model, $num = 0)
	{
		Yii::app()->user->setFlash('error', "Ошибка записи $num");
		if(YII_DEBUG) Yii::app()->user->setFlash('notice', FunLib::prr($this->promt));
	}

	public function savePromt(&$model)
	{
		if(!$model->validate() || !$model->save()) {
			//echo get_class($model);
			$name_cls = get_class($model);
			
			
			if(YII_DEBUG) {
				$this->promt[$name_cls]['getErrors'] = $model->getErrors();
				$this->promt[$name_cls]['errors'] = $model->errors;
			} else {
				$this->promt = 'Ошибка в '.$name_cls;
			}

			$this->isSave=false;
			Yii::app()->user->setFlash('error', $this->promt);
//			CVarDumper::dump($this->promt,100,1);
//			die();
		}
	}
	
	/**
	 * AJAX Добавление полей физ лиц в форме заявки для Организации
	 */
	public function actionAddOrgfield($i)
	{
		$this->typeOfZayavka = 'org'; 
		$profile = new Profile;
		$educat = new Education;
		$gkhZ = new GkhZayavka;
		
		Yii::app()->clientScript->scriptMap[ 'jquery.js' ] = false;
		Yii::app()->clientScript->scriptMap[ 'jquery.min.js' ] = false;
			
		if ($i<=5) { 
			$this->renderPartial('z_profile', 
			array(	'modelProfile'=>$profile ,
					'educat'=>$educat,
				  	'i'=>$i,
					'gkhZ'=>$gkhZ,
				  	'form'=>Yii::app()->session['form'] ),
					false, true);
		}
	}
	
	/**
	 * Форма данных от частного лица
	 */
	public function actionAddProfile1() 
	{
		$this->zTitleProfile = 'Форма заявки для частных лиц';
		$model = new Profile('zprofile');
		$educat = new Education;
		$gkhZ = new GkhZayavka;
		$org = new Org('profile');
		$id_specializ = Yii::app()->request->getParam( 'id_specializ' );
			$model->id_specializ =  $id_specializ;
			
		$this->performAjaxValidation(array($org, $model, $educat, $gkhZ));
		
		if( isset($_POST['Profile']) && isset($_POST['Org']) && isset($_POST['Education']) ) {
			$model->attributes=$_POST['Profile'];
			if (isset($_POST['Org'])) $org->attributes=$_POST['Org'];
			if (isset($_POST['Education'])) $educat->attributes=$_POST['Education'];
			$org->_org_type = 0;
			$org->_z_type = 0; // Заявка от ФИЗИЧЕСКОГО ЛИЦА
			if($org->save()) {
				$model->id_org = $org->id;
				if($model->save()) {
					$educat->id_profile=$model->id;
					if($educat->save()) {
						$gkhZ->id_org = $org->id;
						$gkhZ->id_profile = $model->id;
						$gkhZ->save();
						$this->redirect(array('finish','id'=>$model->id));
					} else
							self::setErr($educat, 3);
				}
				else 
					self::setErr($model, 2);
			} else 
				self::setErr($org, 1);

		}

		$this->render('zprofile', array('model'=>$model, 'org'=>$org, 'educat'=>$educat, 'gkhZ'=>$gkhZ));
	}


	protected function arrayAjaxValidation()
	{
		if (isset($_POST['ajax'])) {
			$new = array();
			$i=0;
			foreach ($_POST['Profile'] as $key => $prof) {
				if (isset($_POST['Profile'][$key])) { 


					$profile = new Profile;
					$profile->attributes = $_POST['Profile'][$key];
					
					if (!$profile->validate())
						foreach ($profile->getErrors() as $keys => $value) {
							$new['Profile_'.$i.'_'.$keys] = $value; 
							
						}
					$i++;
					

				}
			}
			echo json_encode($new);
			Yii::app()->end();
 		}  
	}

	protected function performAjaxValidation2($model)
	{
		if (Yii::app()->request->isAjaxRequest)
		{
			
			$valid = CActiveForm::validate($model);
			Yii::trace(print_r(json_decode($valid), true), 'my.zayav.perfomajax');
			echo $valid;
			Yii::app()->end();
		}
	}


	protected function performAjaxValidation($model)
	{
		//if(isset($_POST['ajax']) && $_POST['ajax']==='profile-form')
		if(isset($_POST['ajax']) && $_POST['ajax']==='yw0')
		{
			
			$valid = CActiveForm::validate($model);
			Yii::trace(print_r(json_decode($valid), true), 'my.zayav.perfomajax');
			echo $valid;
			Yii::app()->end();
		}
	}

   public function accessRules()
    {
        return array(
            // array('deny',
            //     'actions'=>array('admin'),
            //     'users'=>array('?'),
            // ),
            array('allow',
                'actions'=>array('Admin'),
                //'roles'=>array('sex'),
                //'users'=>array('vadimka108@gmail.com'),
                'expression'=>'(Yii::app()->user->role=="sex") || (Yii::app()->user->role=="admin") ',
            ),
            array('deny',
                'actions'=>array('Admin','Delete', 'Create','Update'),
                'users'=>array('*'),
            ),
        );
    }	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}	
}
?>