<?php

return array(
    'List Gallary'=>'Список галерей',
    'Manage Gallary'=>'Управление галереями',
    'Gallary'=>'Галлереи - управление',
    'Create Gallary'=>'Создать галлерею',
    'Delete Gallary'=>'Удалить галлерею',
);
