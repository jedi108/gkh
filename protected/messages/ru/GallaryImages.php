<?php

return array(
    'GallaryImages'=>'Управление изображениями галлерей',
    'List GallaryImages'=>'Список изображений галлерей',
    'Create GallaryImages'=>'добавить изображение галлереи',
    'Update GallaryImages'=>'редактировать изображение галлереи',
    'Delete GallaryImages'=>'удалить изображение галлереи',
    'Manage GallaryImages'=>'Управление изображениями',
    //'Manage GallaryImages'=>'Управление изображениями'
);