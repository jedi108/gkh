<?php
/**
 * @see Англ
 */
     return array(
		"List Schedule" =>"Список расписаний",
		"Create Schedule" =>"Создать расписание",
		"Manage Schedule" =>"Управление расписаниями",
		"Delete Schedule" =>"Удалить расписание",
		"Update Schedule" =>"Обновить расписание",
		'Schedules' =>'Расписания',
     );
