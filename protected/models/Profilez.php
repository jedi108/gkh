<?php

/**
 * This is the model class for table "{{profile}}".
 *
 * The followings are the available columns in table '{{profile}}':
 * @property integer $id
 * @property string $lastname
 * @property string $firstname
 * @property string $middlename
 * @property string $year_birthday
 * @property string $addr
 * @property string $email
 * @property string $phone
 * @property integer $id_specializ
 *
 * The followings are the available model relations:
 * @property Specializ $idSpecializ
 */
class Profilez extends CActiveRecord
{
	//public $idSpecializ=1;
	public $form_podgotovki  = array(
		'1'=>'новая подготовка',
		'2'=>'переподготовка',
		'3'=>'повышение квалицикации',
		'4'=>'переаттестация'
	);
	
	public $_types = array(
		'0'=>'Заявка',
		'1'=>'Сайт'
	);
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Profile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{profile}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lastname, doljnost, firstname, middlename, email, phone', 'required'),
			array('id_org', 'numerical', 'integerOnly'=>true),
			array('lastname, firstname, middlename, addr', 'length', 'max'=>255),
			array('year_birthday', 'length', 'max'=>4),
			array('year_birthday, addr', 'length', 'min'=>4),
			array('lastname, firstname, middlename', 'length', 'min'=>2),
			array('email, phone', 'length', 'max'=>64),
			array('email', 'email', 'fullPattern'=>true),
			array('email', 'email', 'checkMX'=>true),
			array('id_org', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('email, phone', 'required', 'on'=>'zprofile' ),
			array('id, lastname, firstname, middlename, year_birthday, addr, email, phone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'idSpecializ' => array(self::BELONGS_TO, 'Specializ', 'id_specializ'),
			'org' => array(self::BELONGS_TO, 'Org', 'id_org'),
			'education' => array(self::HAS_MANY, 'Education', 'id_profile'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lastname' => 'Фамилия',
			'firstname' => 'Имя',
			'middlename' => 'Отчество',
			'year_birthday' => 'Год рождения',
			'addr' => 'Адрес',
			'email' => 'E-mail',
			'phone' => 'Телефон',
			'id_org' => 'Организация',
			'doljnost'=>'Должность'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('middlename',$this->middlename,true);
		$criteria->compare('year_birthday',$this->year_birthday,true);
		$criteria->compare('addr',$this->addr,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('id_specializ',$this->id_specializ);
		$criteria->compare('id_org',$this->id_org);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}