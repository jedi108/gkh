<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
	public $name;
	public $email;
	public $subject;
	public $body;
	public $verifyCode;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('name', 'required', 'message' => Yii::t( 'site', 'Please enter your name')),
			array('email', 'required', 'message' => Yii::t( 'site', 'Please enter email address')),
			array('subject', 'required', 'message' => Yii::t( 'site', 'Please enter a subject')),
			array('body', 'required', 'message' => Yii::t( 'site', 'Please enter your message')),
			// email has to be a valid email address
			array('email', 'email', 'message' => Yii::t( 'site', 'Incorrect email format')),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'message' => Yii::t( 'site', 'Incorrect verification code')),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'verifyCode'=>'Verification Code',
		);
	}
}