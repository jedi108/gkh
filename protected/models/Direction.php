<?php

/**
 * This is the model class for table "{{direction}}".
 *
 * The followings are the available columns in table '{{direction}}':
 * @property integer $id
 * @property string $name
 * @property string $img_logo
 * @property string $img_slide
 *
 * The followings are the available model relations:
 * @property Specializ[] $specializs
 */
class Direction extends CActiveRecord {

    /**
     * @return array default scope criteria
     */
    public function defaultScope() {
        return array(
                //'order' => 'name',
        );
    }

    public static function getDirectionsCat($idDir) {
        $dirCat = function($idDir, $idCat) {
                    $sql = "SELECT 	spectizliz.id AS id_spectizliz, 
							t_direction.name AS dir_name,
							t_direction.img_logo AS img_logo, 
							spectizliz.name AS spec_name, 
							cat.name_cat AS name_cat,
							cat_specializ.id_t_cat as id_cat
					FROM  `t_direction` 
					LEFT JOIN t_specializ AS spectizliz ON ( t_direction.id = spectizliz.id_f_direction ) 
					RIGHT JOIN t_cat_specializ AS cat_specializ ON ( spectizliz.id = cat_specializ.id_t_specializ ) 
					LEFT JOIN t_cat AS cat ON ( cat.id = cat_specializ.id_t_cat ) ";
                    //WHERE t_direction.id ='".$id_cat."'";

                    return sql::command($sql . ' WHERE t_direction.id = %s AND cat_specializ.id_t_cat = %s ', $idDir, $idCat)->queryAll();
                };

        $all = array();
        $catG = Cat::model()->findAll();
        foreach ($catG as $key => $value) {
            //CVarDumper::dump($value->id, 100, true);
            $all[$value->name_cat] = $dirCat($idDir, $value->id);
            //$all[]['category'] = $value->name_cat;
        }
        return $all;
    }

    public function getImgslide670() {
        //return CHtml::image($this->img_slide);
        $m = Media::img($this->img_slide, "slide670", "temp", array('width' => '501px'));
        if (!$m)
            $m = ''; //Media::img('/file/no-photo.jpg', "slide920", "temp");
        return $m;
    }

    public function getImgslideCustum() {
        //return CHtml::image($this->img_slide);
        $m = Media::img($this->img_slide, "banner110x28", "temp");
        if (!$m)
            $m = ''; //Media::img('/file/no-photo.jpg', "slide790", "temp");
        return $m;
    }

    public function getImgslide() {
        //return CHtml::image($this->img_slide);
        $m = Media::img($this->img_slide, "slide790", "temp", array('width' => '710px'));
        if (!$m)
            $m = ''; //Media::img('/file/no-photo.jpg', "slide790", "temp");
        return $m;
    }

    public function getImgslide150x80() {
        //return CHtml::image($this->img_slide);
        $m = Media::img($this->img_slide, "150x80", "temp");
        if (!$m)
            $m = Media::img('/file/no-photo.jpg', "150x80", "temp");
        return $m;
    }

    public function getImglogo() {
        //return CHtml::image($this->img_slide);
        $m = Media::img($this->img_logo, "100x80", "temp");
        if (!$m)
            $m = Media::img('/file/no-photo.jpg', "100x80", "temp");
        return $m;
    }

    public function getImglogoButton() {
        //return $this->img_logo;
        //$m = Media::imgPath($this->img_logo, "dirButton", "temp", array('class'=>'dirButton'));
        $m = sprintf($this->img_logo, '');
        if (!$m)
            $m = Media::imgPath('/file/no-photo.jpg', "dirButton", "temp");
        return $m;
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Direction the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{direction}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('name, img_logo, img_slide', 'length', 'max' => 255),
            array('id_org', 'numerical', 'integerOnly' => true),
            array('body', 'length', 'max' => 65535),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, id_org, name, img_logo, img_slide, pagination', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            //'specializs' => array(self::HAS_MANY, 'Specializ', 'id_f_direction'),
            'iOrg' => array(self::BELONGS_TO, 'Org', 'id_org'),
            'relation' => array(self::HAS_MANY, 'Relation', 'id_direction', 'order' => 'relation.id_specializ'),
             'specializ' => array(self::HAS_MANY, 'Specializ', 'id_specializ', 'through' => 'relation'),
            //'specializ' => array(self::HAS_MANY, 'Specializ', 'id_specializ', 'through' => 'relation', 'order' => 'specializ.id_cat, specializ.name'),
                //'rel' => array(self::MANY_MANY, 'Specializ',''),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Направление',
            'img_logo' => 'Иконка кнопки направления',
            'img_slide' => 'Длинный слайд',
            'id_org' => 'Организация (партнер, филиал)',
            'body' => 'тектовая область',
        );
    }

//    public function defaultScope() {
//            return array(
//                    'order'=>'name',
//            );
//    }

    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('img_logo', $this->img_logo, true);
        $criteria->compare('img_slide', $this->img_slide, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->count()
            ),
        ));
    }

    public static function getAllDirections() {
        $criteria = array();
        return CHtml::listData(Direction::model()->findAll($criteria), 'id', 'name');
    }

    public static function getName($id) {
        //$result=Directioni::model()->findByPk($id);
        $specializ = Specializ::model()->findByPk($id);
        $direction = $specializ->direction;
        return $direction[0];
    }

    public static function getAllDirectionIn($id_specializ) {
        $specializ = Specializ::model()->findByPk($id_specializ);
        $direction = $specializ->direction;
        return CHtml::listData($direction, 'id', 'name');
    }

}