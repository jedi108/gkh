<?php

/**
 * This is the model class for table "{{specializ_dates}}".
 *
 * The followings are the available columns in table '{{specializ_dates}}':
 * @property integer $id
 * @property string $date
 * @property integer $id_t_specializ
 *
 * The followings are the available model relations:
 * @property Specializ $idTSpecializ
 */
class SpecializDates extends CActiveRecord
{
	public $SpecializDates;

	public static function delAll($id_spec)
	{
		$sql = "DELETE FROM `t_specializ_dates`WHERE id_t_specializ='$id_spec'";
		Yii::app()->db->createCommand($sql)->execute();
	}

	public static function saveDates($dates, $id_spec)
	{
		$add = array();
		$sqlValues='';
		$sqlDel = '';

		$toWhereNotDel[] = " id_t_specializ = '".$id_spec."' ";

		foreach ($dates as $key => $date) {
			$sql = "SELECT date, id_t_specializ FROM {{specializ_dates}} 
					WHERE DATE(date)  = ".self::dateMysql($date['date'])." AND 
							id_t_specializ = '".$id_spec."'";
			$rows = Yii::app()->db->createCommand($sql)->queryAll();

			$toWhereNotDel[] = "DATE(date) <> ".self::dateMysql($date['date']);
			
			if(count($rows)>0) {
				$i=1;


			} else {
				//Добавляем новые даты, с привязкой id_t_specializ

				$dt = self::dateMysql($date['date']);
				//$dt = $date['date'];
				$add[] = '('.$id_spec . ', ' . $dt .')';
				
			}
		}

		if ($add != array()) {
			$sqlValues = implode(', ', $add);
			$sql = "INSERT INTO {{specializ_dates}} (id_t_specializ, date) VALUES $sqlValues";
			// 		echo $sql;
			// die();
			Yii::app()->db->createCommand($sql)->execute();
		}

		if ($sqlDel != array()) {
			$sqlDel = implode(' AND ', $toWhereNotDel);
			$sql = "DELETE FROM {{specializ_dates}} WHERE ".$sqlDel;
			Yii::app()->db->createCommand($sql)->execute();
		}

	}

 

	private static function dateMysql($date)
	{
		return "STR_TO_DATE('".date('Y-m-d H:i:s',strtotime($date))."','%Y-%m-%d %H:%i:%s')";
	}
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SpecializDates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{specializ_dates}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date, id_t_specializ', 'required'),
			array('id_t_specializ', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('date', 'type', 'type' => 'date', 'message' => '{attribute}: is not a date!', 'dateFormat' => 'yyyy-MM-dd'),
			array('id, date, id_t_specializ', 'safe', 'on'=>'search'),

			//array('SpecializDates_date','my_fun'),
		);
	}

	// public function my_fun($attribute,$params)
 //    {
 //    	echo '----------------------------------';
 //    	CVarDumper::dump( $attribute,100,true);
 //    	CVarDumper::dump( $params,100,true);
 //    	echo '----------------------------------';
 //    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idTSpecializ' => array(self::BELONGS_TO, 'Specializ', 'id_t_specializ'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'id_t_specializ' => 'Id T Specializ',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('id_t_specializ',$this->id_t_specializ);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}