<?php

/**
 * This is the model class for table "{{cat_specializ}}".
 *
 * The followings are the available columns in table '{{cat_specializ}}':
 * @property integer $id
 * @property integer $id_t_specializ
 * @property integer $id_t_cat
 *
 * The followings are the available model relations:
 * @property Cat $idTCat
 * @property Specializ $idTSpecializ
 */
class CatSpecializ extends CActiveRecord
{
	public static function catSpec()
	{
		$sql="select * from {{cat}} ";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();
		return $rows;
	}
	public static function catSel($id)
	{
		$sql = "SELECT id_t_cat FROM {{cat_specializ}} WHERE id_t_specializ='$id'";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();
		$data_selected=array();
		foreach ($rows as $key => $val) { 	array_push($data_selected,$val['id_t_cat']);  } 
		return $data_selected;
	}
	public static function delAll($id_spec)
	{
		$sql = "DELETE FROM {{cat_specializ}} WHERE id_t_specializ='$id_spec'";
		Yii::app()->db->createCommand($sql)->execute();
	}
	public static function saveSelection($cat_selects, $id_spec)
	{
		$add = array();
		$sqlValues='';
		$sqlDel = '';

		$toWhereNotDel[] = " id_t_specializ = '".$id_spec."' ";

		foreach ($cat_selects as $key => $id_cat) {
			$sql = "SELECT id_t_cat, id_t_specializ FROM {{cat_specializ}} 
					WHERE id_t_cat  = ".$id_cat." AND 
							id_t_specializ = '".$id_spec."'";
			$rows = Yii::app()->db->createCommand($sql)->queryAll();

			$toWhereNotDel[] = "id_t_cat <> ".$id_cat;
			
			if(count($rows)>0) {
				$i=1;


			} else {
				//Добавляем новые даты, с привязкой id_t_specializ
				//$dt = self::dateMysql($date['date']);
				$add[] = '('.$id_spec . ', ' . $id_cat .')';
				
			}
		}

		if ($add != array()) {
			$sqlValues = implode(', ', $add);
			$sql = "INSERT INTO {{cat_specializ}} (id_t_specializ, id_t_cat) VALUES $sqlValues";
			
			//echo $sql;
			Yii::app()->db->createCommand($sql)->execute();
		}

		if ($sqlDel != array()) {
			$sqlDel = implode(' AND ', $toWhereNotDel);
			$sql = "DELETE FROM {{cat_specializ}} WHERE ".$sqlDel;

			//echo $sql;
			Yii::app()->db->createCommand($sql)->execute();
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CatSpecializ the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cat_specializ}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_t_specializ, id_t_cat', 'required'),
			array('id_t_specializ, id_t_cat', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_t_specializ, id_t_cat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idTCat' => array(self::BELONGS_TO, 'Cat', 'id_t_cat'),
			'idTSpecializ' => array(self::BELONGS_TO, 'Specializ', 'id_t_specializ'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_t_specializ' => 'Id T Specializ',
			'id_t_cat' => 'Id T Cat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_t_specializ',$this->id_t_specializ);
		$criteria->compare('id_t_cat',$this->id_t_cat);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}