<?php

/**
 * This is the model class for table "{{education}}".
 *
 * The followings are the available columns in table '{{education}}':
 * @property integer $id
 * @property integer $id_profile
 * @property string $institution
 * @property integer $qualification
 * @property string $specialty
 * @property string $end_year
 */
class Education extends CActiveRecord
{
	public $qualit = array(
		'0'=>'бакалавр','1'=>'специалист','3'=>'магистр ',
	);

	public function getEducationByArr()
	{
		return array(
			'1'=>'среднее',
			'2'=>'среднее специальное',
			'3'=>'не оконченное высшее',
			'4'=>'высшее',
			'5'=>'другое'
		);
	}

	public function getDescEducate()
	{
		$arr = $this->getEducationByArr();
		if(isset($arr)) return $arr[$this->id_educate];
	}


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Education the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{education}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_educate,  institution, qualification_text, specialty, end_year, n_diplom', 'required'),
			array('id, id_profile, qualification', 'numerical', 'integerOnly'=>true),
			array('institution, specialty', 'length', 'max'=>255),
			array('qualification_text', 'length', 'max'=>128),
			array('educate, n_diplom', 'length', 'max'=>16),
			array('end_year', 'length', 'max'=>4),
			array('end_year', 'length', 'min'=>4),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_profile, institution, qualification, specialty, end_year', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_profile' => 'Id Profile',
			'institution' => 'Уч.заведение',
			'qualification' => 'Квалификация',
			'specialty' => 'Специальность',
			'end_year' => 'Год окончания',
			'qualification_text' => 'Квалификация',
			'n_diplom'=>'№ Диплома',
			'id_educate'=>'Образование',
			
			'descEducate'=>'Образование',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_profile',$this->id_profile);
		$criteria->compare('institution',$this->institution,true);
		$criteria->compare('qualification',$this->qualification);
		$criteria->compare('specialty',$this->specialty,true);
		$criteria->compare('end_year',$this->end_year,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}