<?php

/**
 * This is the model class for table "{{specializ}}".
 *
 * The followings are the available columns in table '{{specializ}}':
 * @property integer $id
 * @property integer $id_f_direction
 * @property integer $name
 * @property string $cost
 * @property integer $times
 *
 * The followings are the available model relations:
 * @property CatSpecializ[] $catSpecializs
 * @property Direction $idFDirection
 * @property SpecializDates[] $specializDates
 */
class Specializ extends CActiveRecord
{
        public $id_cat;
    /**
	* @return array default scope criteria
	*/
	public function defaultScope()
	{
		return array(
		  //'order' => 'name',
		);
	}

	public function getCost()
	{

	}
	public function getIsKurs()
	{
		return is_null($this->Kurs)?false:true;
	}

	public function getKursData()
	{
		//return $this->Kurs->Name;
		return array(
			'field' => 'Курс',
			'data' => CHtml::link($this->Kurs->name, Yii::app()->createUrl('/gkh/kurs/view',array('id'=>$this->Kurs->id))),
		);
	}

	function getOrgAdr()
	{
		if (isset($this->idOrg))		{
			return $this->idOrg;
		} else {
			return $this->idFDirection->iOrg;
		}
	}

	/**
	 * Фильруемые поля для searchDao
	 */
	public $dir_name;
	public $spec_name;
	public $name_cat;
	public $cost_year;
	public $kurs;
	public $id_org = 0;

	protected function partialSearchString($string)
	{
		$patterns = array();
		$patterns[0] = '/%/';
		$patterns[1] = '/_/';
		$replacements = array();
		$replacements[0] = '';
		$replacements[1] = '';
		return '%'.preg_replace($patterns, $replacements, $string).'%';
	}

	public function searchDao()
	{
		$criteria = array();
		$params = array();
		$criteria[] = "1";

		if (isset($this->dir_name) && $this->dir_name!='') {
			//echo 'RSTATUS::::::::::::::::::'.$this->rates_status.'<br/>';
			$criteria[] = "t_direction.id = '".$this->dir_name."'";
			$params[':t_direction'] = "'".$this->dir_name."'";//$this->partialSearchString(strtolower($this->id_operation_code));
		}
		
		if (isset($this->name_cat) && $this->name_cat!='') {
			//echo 'RSTATUS::::::::::::::::::'.$this->rates_status.'<br/>';
			$criteria[] = "cat_specializ.id_t_cat = '".$this->name_cat."'";
			$params[':cat_specializ.id_t_cat'] = "'".$this->name_cat."'";//$this->partialSearchString(strtolower($this->id_operation_code));
		}

		if (isset($this->kurs) && $this->kurs!='') {
			//echo 'RSTATUS::::::::::::::::::'.$this->rates_status.'<br/>';
			$criteria[] = "spectizliz.id_kurs = '".$this->kurs."'";
			//$criteria[] = "kurs.id = '".$this->kurs."'";
			$params[':kurs.id'] = "'".$this->kurs."'";//$this->partialSearchString(strtolower($this->id_operation_code));
		}

		if (isset($this->spec_name) && $this->spec_name!='') {
			$criteria[] = "spectizliz.name like '".$this->partialSearchString(strtolower($this->spec_name)) ."'";
			$params[':spectizliz.name'] = "'".$this->partialSearchString(strtolower($this->spec_name))."'";
		}

		$sql = "SELECT 
					spectizliz.id as id, 
					t_direction.name AS dir_name, 
					spectizliz.name AS spec_name, 
					cat.name_cat AS name_cat,
					spectizliz.cost as cost,
					spectizliz.cost_year as cost_year,
					kurs.name as kursname
				FROM  `t_direction` 
				LEFT JOIN t_specializ AS spectizliz ON ( t_direction.id = spectizliz.id_f_direction ) 
				LEFT JOIN t_kurs AS kurs ON ( spectizliz.id_kurs = kurs.id ) 
				RIGHT JOIN t_cat_specializ AS cat_specializ ON ( spectizliz.id = cat_specializ.id_t_specializ ) 
				LEFT JOIN t_cat AS cat ON ( cat.id = cat_specializ.id_t_cat ) 
				";

		$counting = "SELECT COUNT( * ) 
				FROM  `t_direction` 
				LEFT JOIN t_specializ AS spectizliz ON ( t_direction.id = spectizliz.id_f_direction ) 
				RIGHT JOIN t_cat_specializ AS cat_specializ ON ( spectizliz.id = cat_specializ.id_t_specializ ) 
				LEFT JOIN t_cat AS cat ON ( cat.id = cat_specializ.id_t_cat )";

		if ($criteria !== array()) {
				$criteria = implode(' AND ', $criteria);
				$counting = $counting.' WHERE '.$criteria .' ORDER BY t_direction.name, spectizliz.name';
					//' GROUP BY t_direction.id 
				$sql = $sql.' WHERE '.$criteria;//.' GROUP BY id ';
				$count = Yii::app()->db->createCommand($counting)->bindValues($params)->queryScalar();
		}

		//CVarDumper::dump ($criteria,100,0); Yii::app()->end();

		$SqldataProvider = new CSqlDataProvider($sql, array(
				'keyField'=>'id',
				'totalItemCount'=>$count,
				//'params' => $params,
				'pagination'=>array('pageSize'=>'100'),
				'sort' => array('attributes' => array('cost', 'name_cat', 'spec_name', 'dir_name')),
		));
		// echo '<br/>';
		//CVarDumper::dump($sql, 200, true);
		// echo '<br/>';
		return $SqldataProvider;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Specializ the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{specializ}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('id_f_direction, name', 'required'),
			array('name', 'required'),
			//array('id_f_direction, id_kurs, times', 'numerical', 'integerOnly'=>true),
			array('id_kurs, times', 'numerical', 'integerOnly'=>true),
			array('cost, cost_year, cost_all', 'length', 'max'=>9),
                        array('body', 'length', 'max'=>65535),
			array('id_org, id_kurs, id_cat', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			//array('id, id_kurs, id_f_direction, name, cost, times, id_org, body', 'safe', 'on'=>'search'),
			array('id, id_kurs, name, cost, times, id_org, body', 'safe', 'on'=>'search'),
			//DAO search
			array('dir_name, kurs, name_cat, spec_name','safe','on'=>'searchDao'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'catSpecializs' => array(self::HAS_MANY, 'CatSpecializ', 'id_t_specializ'),
            'idOrg' => array(self::BELONGS_TO, 'Org', 'id_org'),
            'Kurs' => array(self::BELONGS_TO, 'Kurs', 'id_kurs'),
            //'idFDirection' => array(self::BELONGS_TO, 'Direction', 'id_f_direction'),
            'specializDates' => array(self::HAS_MANY, 'SpecializDates', 'id_t_specializ'),
            'categories'=>array(self::MANY_MANY, 'Cat', 't_cat_specializ(id_t_specializ, id_t_cat)'),
			'relation'=>array(self::HAS_MANY,'Relation','id_specializ','order'=>'relation.id_direction'),
			'direction'=>array(self::HAS_MANY, 'Direction','id_direction','through'=>'relation'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
       return array(
            'id' => 'ID',
           // 'id_f_direction' => 'Направление',
            'name' => 'Специальность',
            'cost' => 'Стоимость первичная',
            'cost_year' => 'Стоимость ежегодная',
            'cost_all' => 'Общая стоимость',
            'times' => 'Время',
            'id_org' => 'Организация (партнер, филиал)',
            'id_kurs' => 'Курс',
            'body' => 'Body',
        );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		//$criteria->compare('id_f_direction',$this->id_f_direction);
		$criteria->compare('name',$this->name);
		$criteria->compare('cost',$this->cost,true);
		$criteria->compare('cost_year',$this->cost,true);
		$criteria->compare('times',$this->times);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public static function getAllSpecializes(){
		$criteria=array();
		return CHtml::listData(Specializ::model()->findAll($criteria),'id','name');
	}
	public static function getAllSpecializesIn($id_direction){
		//$criteria=new CDbCriteria;
		//$criteria->compare();
		//print $id_direction;
		$direction = Direction::model()->findByPk($id_direction);
		$specializ = $direction->specializ;
		//print_r ($specializ);
		//Relation::$direction->
		return CHtml::listData($specializ,'id','name');
		//return CHtml::listData(Specializ::model()->findAll($criteria),'id','name');
	}
	public static function getAllSpecializesNone($id_direction){
					//$id_user = Yii::app()->db->createCommand()
				//->select('id_user')
				//->from('tbl_user')
				//->where('email=email',array(':email'=>Yii::app()->user->name))
				//->queryRow();
		$specializIn = Yii::app()->db->createCommand()
			->select('id_specializ')
			->from('{{relation}}')
			->where('id_direction=:id_direction',array(':id_direction'=>$id_direction))
			->queryColumn();
		//print_r ($specializIn);
		$specializ = Yii::app()->db->createCommand()
			->select('id,name')
			->from('{{specializ}}')
			->where('id NOT IN (:arr)',array(':arr'=>implode(",",$specializIn)))
			->order('name')
			->queryAll();
		//print_r ($specializ);
		return CHtml::listData($specializ,'id','name');
	}
}