<?php

/**
 * This is the model class for table "{{page}}".
 *
 * The followings are the available columns in table '{{page}}':
 * @property integer $id
 * @property string $title
 * @property string $body
 */
class Page extends CActiveRecord
{
	public $types = array(
		'0'=>'Страница',
		//'1'=>'Новость',
		'2'=>'Блок в странице',
	);

	public $type = 0;

	function getPageType()
	{
		return $this->types[$this->type];
	}

	function getLinkNews()
	{
		if ($this->type==1) return array('Новости'=>array('/page/news'));
		return array($this->title);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Page the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{page}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('title, date_created', 'length', 'max'=>255),
			array('body', 'length', 'max'=>65535),
			array('type', 'length', 'min'=>1),
                        array('id_gallary', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, body, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jfiles' => array(self::HAS_MANY, 'jFiles', 'id_page'),
                        'Gallary' => array(self::BELONGS_TO, 'Gallary', 'id_gallary'),
		);
	}
	public function defaultScope()
	{
	    return array('order' => 'date_created DESC',
                         'condition'=>'t.type<>1');
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название страницы',
			'body' => 'Текстовое поле',
			'type' => 'Тип страницы',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('type',$this->type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
                        'pageSize'=>200,
             ), 
		));
	}
}
