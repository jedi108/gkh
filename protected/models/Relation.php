<?php

/**
 * This is the model class for table "{{relation}}".
 *
 * The followings are the available columns in table '{{relation}}':
 * @property integer $id
 * @property integer $id_direction
 * @property integer $id_specializ
 */
class Relation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Relation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{relation}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_direction, id_specializ', 'required'),
			array('id_direction, id_specializ', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_direction, id_specializ', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_direction' => 'Id Direction',
			'id_specializ' => 'Id Specializ',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_direction',$this->id_direction);
		$criteria->compare('id_specializ',$this->id_specializ);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function saveRelation($id_direction,$id_specializ=array()){
		Relation::model()->deleteAll('id_direction = :id_direction', array(':id_direction' => $id_direction));
		if ($id_specializ)
		foreach ($id_specializ as $key => $value) {
			$relation = new Relation;
			$relation->id_direction = $id_direction;
			$relation->id_specializ = $value;
			$relation->save();
		}
	}
	public static function saveRelationDirections($id_specializ,$id_direction=array()){
		Relation::model()->deleteAll('id_specializ = :id_specializ', array(':id_specializ' => $id_specializ));  
		foreach ($id_direction as $key => $value) {
			$relation = new Relation;
			$relation->id_specializ = $id_specializ;
			$relation->id_direction = $value;
			$relation->save();
		}
	}
}