<?php

/**
 * This is the model class for table "{{org}}".
 *
 * The followings are the available columns in table '{{org}}':
 * @property integer $id
 * @property string $name_small Краткое название
 * @property string $name_full Полное название
 * @property string $FIO_rukovoditel
 * @property string $FIO_ispolnitil
 * @property string $logo
 * @property string $inn
 * @property string $kpp
 * @property string $bik
 * @property string $adr_fact
 *
 * The followings are the available model relations:
 * @property Specializ[] $specializs
 */
class Org extends CActiveRecord
{
	//TODO
	public $org_links = array(
		'0'=>'не известн',
		'1'=>'филиал',
		'2'=>'организация-партнер',
	);

	public $_types = array(
		'0'=>'Заявка',
		'1'=>'админ'
	);
	
	/**
	 * Источник заявки
	 * @var type array
	 */
	public $_ztypes = array(
		'0'=>'Физ.л.',
		'1'=>'Юр.л.'
	);

	public $_org_type = 1;
	//public $_z_type = 1;


	
	public static function getFillials()
	{
		return CHtml::listData( Org::model()->findAll('orglink = 1'), 'id', 'adr_fact');
	}

	public function getOrgLnk()
	{
		return $this->org_links[$this->orglink];
	}

	public function getOrgFrom()
	{
		return $this->_types[$this->_org_type];
	}

	public function searchDao()
	{
		$criteria = array();
		$params = array();
		$criteria[] = "1";

		if (isset($this->_z_type) && $this->_z_type!='') {
			//echo 'RSTATUS::::::::::::::::::'.$this->rates_status.'<br/>';
			$criteria[] = "_z_type = '".$this->_z_type."'";
			$params[':_z_type'] = "'".$this->_z_type."'";//$this->partialSearchString(strtolower($this->id_operation_code));
		}
		
		if (isset($this->rates_status) && $this->rates_status!='') {
			//echo 'RSTATUS::::::::::::::::::'.$this->rates_status.'<br/>';
			$criteria[] = "rates.status = '".$this->rates_status."'";
			$params[':rates.status'] = "'".$this->rates_status."'";//$this->partialSearchString(strtolower($this->id_operation_code));
		}
		$sql = "SELECT org.id, 
						specializ.name, 
						org.name_small, 
						profile.lastname, 
						profile.firstname, 
						profile.middlename, 
						_org_type, 
						date_created,  
						_z_type,
						CONCAT( profile.lastname,  ' ', LEFT( profile.firstname, 1 ) ,  '. ', LEFT( profile.middlename, 1 ) ,  '. ' ) 
							as FIO
				FROM t_org as org
				join `t_profile` as profile on (org.id = profile.id_org)
				join t_specializ as specializ on (specializ.id = profile.id_specializ)
				";

		$counting = "SELECT COUNT( * ) 
				FROM t_org as org
				join `t_profile` as profile on (org.id = profile.id_org)
				join t_specializ as specializ on (specializ.id = profile.id_specializ)
				";

		if ($criteria !== array()) {
				$criteria = implode(' AND ', $criteria);
				$counting = $counting.' WHERE '.$criteria ;// .' ORDER BY t_direction.name, spectizliz.name';
					//' GROUP BY t_direction.id 
				$sql = $sql.' WHERE '.$criteria;//.' GROUP BY id ';
				$count = Yii::app()->db->createCommand($counting)->bindValues($params)->queryScalar();
		}

		$SqldataProvider = new CSqlDataProvider($sql, array(
				'keyField'=>'id',
				'totalItemCount'=>$count,
				//'params' => $params,
				'pagination'=>array('pageSize'=>'100'),
				'sort'=>array(
					'attributes'=> array('id','date_created'),
                    'defaultOrder'=>'date_created DESC',
                ),
		));
		// echo '<br/>';
		// CVarDumper::dump($sql, 200, true);
		// echo '<br/>';
		return $SqldataProvider;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Org the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{org}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			/**
			 * Форма для заявки организации
			 */
			array('adr_fact, adr_ur, phone, inn, kpp, bik, bnk_korr, bnk_rchet, email, name_full, bnk_name', 'required','on'=>'orgFormZayavka'),
			array('name_small, _z_type', 'required'),
			array('_org_type, orglink', 'numerical', 'integerOnly'=>true),
			array('name_small, logo, adr_fact, adr_ur', 'length', 'max'=>255),
			array('inn', 'length', 'max'=>12),
			array('kpp, bik', 'length', 'max'=>9),
			array('okpo','length', 'max'=>10),
			array('city, phone, fax, email, www, name_full, bnk_name, okved', 'length', 'max'=>128),
			array('www', 'length', 'max'=>64),
			array('www', 'url', 'allowEmpty'=>true ,'on'=>'orgFormZayavka'),
			array('bnk_korr, bnk_rchet', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name_small, FIO_rukovoditel, FIO_ispolnitil, logo, inn, kpp, bik, adr_fact, adr_ur, phone, fax, email, bnk_korr, bnk_rchet, _org_type', 'safe', 'on'=>'search'),
			//array('name_small', 'required', 'on'=>'profile'),
			array('name_small', 'length', 'min'=>5),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'specializs' => array(self::HAS_MANY, 'Specializ', 'id_org'),
			'profile' => array(self::HAS_MANY, 'Profile', 'id_org'),
			'profileOrg' => array(self::HAS_MANY, 'ProfileOrg', 'id_org'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_small' => 'Название организации (краткое)',
			'name_full'=>'Полное наименование организации',
			'FIO_rukovoditel' => 'ФИО Руководителя',
			'FIO_ispolnitil' => 'ФИО Исполнителя заявки',
			'logo' => 'Logo',
			'inn' => 'ИНН',
			'kpp' => 'КПП',
			'okpo' => 'ОКПО',
			'okved' => 'ОКВЭД',
			'bik' => 'БИК',
			'adr_fact' => 'Фактический адрес',
			'adr_ur' => 'Юридический адрес',
			'phone' => 'Телефон',
			'fax' => 'Факс',
			'www' => 'Сайт организации', 
			'bnk_name'=>'Название банка',
			'email' => 'Емайл организации',
			'bnk_korr' => 'Корр. счет',
			'bnk_rchet' => 'Расчт. счет',
			'_org_type' => 'Тип организации',
			'orglink' => 'Партнерство',
                        'city'=>'Город',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_small',$this->name_small,true);
		$criteria->compare('FIO_rukovoditel',$this->FIO_rukovoditel,true);
		$criteria->compare('FIO_ispolnitil',$this->FIO_ispolnitil,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('inn',$this->inn,true);
		$criteria->compare('kpp',$this->kpp,true);
		$criteria->compare('bik',$this->bik,true);
		$criteria->compare('adr_fact',$this->adr_fact,true);
		$criteria->compare('adr_ur',$this->adr_ur,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('bnk_korr',$this->bnk_korr,true);
		$criteria->compare('bnk_rchet',$this->bnk_rchet,true);
		$criteria->compare('_org_type',$this->_org_type);
		$criteria->compare('orglink',$this->orglink);

		// $params=array(
		// 	'params'=>array(':_org_type'=>1),
		// 	);
		// $condition=array(
		// 	'condition'=>'_org_type=:_org_type',
		// 	);

		$criteria->addCondition('_org_type=1');
		//$criteria->params = array(':_org_type'=>1);

		CVarDumper::dump($criteria->params, 100, 1);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
		        'pageSize'=>100,
		    ), 
		));
	}
}