<?php
$this->breadcrumbs=array(
	'Новости'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'Список новостей, событий', 'url'=>array('index')),
	array('label'=>'Создать новость, собитие', 'url'=>array('create')),
	array('label'=>'Редактировать новость, событие', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить новость, событие', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управлять новостями, событиями', 'url'=>array('admin')),
);
?>

 
 

	<div>


		<div class="row">
			<h1><?= $model->title; ?></h1>
                        <br>
			<?php
			$date = date_create($model->date_created);
			echo 'Дата <strong>'. date_format($date, 'd.m.Y') . '</strong>';
			?>
			<br/>
			<br/>
		</div>
		<br/>
	</div>		
 




<div>
<?= $model->body ;?>
</div>
 

<?php 
//CVarDumper::dump($model->Gallary,100,1);
if (!is_null($model->Gallary)) {
    
    $this->renderPartial('application.modules.gallary.views.gallary._preview5',array('model'=>$model->Gallary));
}
?>

<?php

//$img = CHtml::image(Media::GetImage($model->getLogo()), $model->title, array('style'=>'width:300px'));
//if($model->logo!='')echo  CHtml::link($img,"#data", array("id"=>"fancy-link"));
//
////put fancybox on page
//$this->widget('ext.fancybox.EFancyBox', array(
//        'target'=>'a#fancy-link',
//        //'config'=>array(),
//        )
//	);  
?>  
 
<!-- 
<div style="display:none">
<div id="data">
 
			<img src="<?php //=Media::GetImage($model->getLogo());?>">
 
</div>
</div>-->