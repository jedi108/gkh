<?php
/* @var $this ArticleController */
/* @var $data Article */
?>

<div class="news-list">
        <?php
            if (!is_null($data->getLogo())) {
                echo '<div class="news-list-icon">'.
                        Media::img($data->getLogo(), 'NewsPreview300x200', 'tmp', array('width'=>'220px')).
                        '</div>';
            } else {
            	echo '<div class="news-list-icon">&nbsp;</div>';
            }
        ?>
        <div class="news-list-text">
        	<h2 class="link"><?php echo CHtml::link($data->title,array('/news/view', 'id'=>$data->id));?></h2>
        	<div class="news-list-date">
        	<?php
                $date = date_create($data->date_created);
                $date = date_format($date, 'd.m.Y');
                echo $date;
        	?>
        	</div>
        	<div class="news-list-anonce">
        		<?php
			        $body = $data->is_teaser? $data->teaser: $data->body; 
			        $body = YText::wordLimiter(strip_tags($body),40);
					echo $body;
				?>
        	</div>
        </div>
        <div style="clear: both"></div>
</div>
<div style="clear: both"></div>
<div class="index-delimetr-2">&nbsp;</div>
<div style="clear: both"></div>
