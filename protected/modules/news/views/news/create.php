<?php
$this->breadcrumbs=array(
	'Pages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Список новостей, анонсов', 'url'=>array('index')),
	array('label'=>'Управлять новостями, анонсами', 'url'=>array('admin')),
);
?>

<h1>Создать новость\анонс</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>