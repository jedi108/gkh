<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'page-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type',$model->types); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
	<?php echo $form->labelEx($model,'date_created'); ?>
	<?php
	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
	    'language'=>'ru',
	    'model'=>$model,
	    'attribute'=>'date_created',
	    'options'=>array(
	        'showAnim'=>'fold',
	        'dateFormat'=>'yy-mm-dd',
	    ),
	   
	    'htmlOptions'=>array(
	        'style'=>'height:20px;'
	    ),
	)); ?>
	</div>

        
	<div class="clear"></div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>80,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'body'); ?>
		<?php 
			$this->widget('ext.yiiext.widgets.ckeditor.ECKEditor',array(
				'model'=>$model,                # Data-Model (form model)
				'attribute'=>'body',         # Attribute in the Data-Model
				'editorTemplate'=>'full',		
				'options'=>array(
		            'filebrowserBrowseUrl'=>CHtml::normalizeUrl(array('/site/browse')),
		        ),
			) );
		?>
		<?php echo $form->error($model,'body'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_gallary'); ?>
                <?php echo $form->dropDownList($model,'id_gallary',  GallaryImages::model()->getAllGallarys(),array('prompt'=>'Выберете галерею')); ?>
		<?php echo $form->error($model,'id_gallary'); ?>
	</div>

 
        <div class="row">
                <?php echo $form->labelEx($model,'logo'); ?>
                <?php echo CHtml::activeFileField($model, 'logo'); ?>  
                <?php echo $form->error($model,'logo'); ?>
            
                <?php echo $form->labelEx($model,'del_img'); ?>
                <?php echo $form->checkBox($model,'del_img' );  ?>
                <?php echo $form->error($model,'del_img'); ?>
        </div>
        
        
        <?php if($model->isNewRecord!='1'){ ?>
        <div class="row">
             <?php echo CHtml::image(Yii::app()->request->baseUrl.'/../file/news/'.$model->logo,"logo",array("width"=>200)); ?>  
        </div>
        <?php }?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->