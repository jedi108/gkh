<?php
$this->breadcrumbs=array(
	'Pages'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Список новостей, событий', 'url'=>array('index')),
	array('label'=>'Создать новость, событие', 'url'=>array('create')),
	array('label'=>'Посмотреть новость, событие', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управлять новостями, событиями', 'url'=>array('admin')),
);
?>

<h1>Update Page <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>