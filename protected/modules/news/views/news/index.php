<?php
$this->breadcrumbs=array(
	'Новости',
);

$this->menu=array(
	array('label'=>'Создать новость, анонс', 'url'=>array('create')),
	array('label'=>'Управлять новостями, анонсами', 'url'=>array('admin')),
);
?>

<h1><?= isset($title)? $title : 'Новости'; ?></h1>
 
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'summaryText'=>false,
        'ajaxUpdate'=>false,
)); ?>
