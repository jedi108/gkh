<?php

/**
 * This is the model class for table "page".
 *
 * The followings are the available columns in table 'page':
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property integer $type
 * @property string $date_created
 */
class News extends CActiveRecord {

    public $logo;
    
    public function behaviors() {
        return array(
            'imageModelBehavior'=>array(
                'class'=>'imageModelBehavior',
                'path'=>'/../file/news/',
                'modelFileField'=>'logo',
            )
        );
    }

        public function getUrlLink() {
        return CHtml::link($this->title, array('/news/news/view', 'id' => $this->id));
    }

    public function getLogo() {
        return (!isset($this->logo) || ($this->logo == "") || ($this->logo == "0") || ($this->logo == null) ) ? null : 
                '/file/news/'.$this->logo;
    }

    public $types = array(
        '0' => 'Новость',
        '1' => 'Событие',
            // '2'=>'Банер слева',
            // '3'=>'Банер справа',
    );
    public $type = 0;

    function getPageType() {
        return isset($this->types[$this->type]) ? $this->types[$this->type] : 'неизвестно';
    }

    public function getUrl() {
        return Yii::app()->createUrl('/news/news/view', array('id' => $this->id));
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Page the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{news}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, body, type', 'required'),
            array('title, date_created, logo', 'length', 'max' => 255),
            array('id_gallary', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, type, body, date_created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'Gallary' => array(self::BELONGS_TO, 'Gallary', 'id_gallary'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Заголовок',
            'body' => 'Body',
            //'city' => 'Место',
            'date_created' => 'Дата',
        );
    }

    public function defaultScope() {
        return array('order' => 'date_created DESC');
    }

    public function scopes() {
        return array(
            'last3news' => array(
                'limit' => 3,
                'order' => 'date_created DESC',
                'condition' => 'type=0',
            )
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('body', $this->body, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('date_created', $this->date_created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 100
            ),
        ));
    }

}