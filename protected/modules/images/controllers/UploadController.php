<?php

class UploadController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules(){
		return array(
            array('allow',
                'actions'=>array('AjaxUpload'),
                //'roles'=>array('sex'),
                //'users'=>array('vadimka108@gmail.com'),
                'expression'=>'Yii::app()->user->role=="sex"',
            ),
            array('deny',
                'actions'=>array('AjaxUpload'),
                'users'=>array('*'),
            ),
		);
	}

	public function actionAjaxUpload($p)
	{
		$p = FunLib::unpackData($p);

		$folder    = YiiBase::getPathOfAlias('webroot').$p['path'];
		$previewId = $p['previewId'];
		$inputId = $p['inputId'];
		$preset = $p['preset'];

		if(!Yii::app()->request->isAjaxRequest && !isset($_SERVER['CONTENT_TYPE']))
		throw new CHttpException(400,Yii::t('yii','Your request is invalid.'));
		Yii::import("ext.EAjaxUpload.qqFileUploader");

		$allowedExtensions = array("jpg", "png", "jpeg", "gif", "doc", "docx", "rar", "zip", "7zip");
		$sizeLimit = 2 * 1024 * 1024;
		
		$uploader  = new qqFileUploader($allowedExtensions, $sizeLimit, $p['path'], $previewId, $inputId, $preset);
		
		$result = $uploader->handleUpload($folder);
		
		if ( isset($result['success']) && $result['success'] ) {
			$result['successMessage'] = Y::t('images.upload_aAjaxUpload_fileUploadMessage_success',array('filename'=>$result['file']),null,'File "{filename}" uploaded');
		} 
		
		$return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
		
		
		if ( !empty($result['success']) ) {
//			$fileSize=filesize($folder.$result['filename']);//GETTING FILE SIZE
			$fileName=$result['filename'];//GETTING FILE NAME
			#echo $return;// it's array
		}
		echo $return;
	}
}