<?php

/**
 *
 * Класс конфигурирующий форматы и правила изображений
 * @todo продумать пути к модулю
 *
 * @ps перенес конфигурацию из главного конфига
 */

class MediaConfig
{
	public static $converts = array();

	/**
	 * Дефолтный путь для презетов, если он явно не будет указан в фукнции Media::img
	 */
	public static $pathImg = 'webroot.file.pic.';

	/**
	 * Конфигурация для инициализации презетов
	 * MediaConfig::configPresets()
	 *
	 *  --- ПРИМЕР КОНФИГУРАЦИИ --
	 *
	 *	'100x75' => array(													string - алиас презета, '100x75','MyPreset','FighterAvatar','...'
	 *		'cacheIn' => self::$pathImg.'100x75',							string - путь по умолчанию для сгенерированных презетов, если не будет указан путь модуля (на всякий случай)
	 *
	 *		'actions' => array(												действия(правила) над исходным изображением:
	 *
	 *			'scaleAndCrop' => array(									1. правило: изменить размер пропорционально оригиналу
	 *				'width' => 100,											ширина
	 *				'height' => 75											высота
	 *			),
	 *			'rotate'=>array('degrees'=>30, 'background'=>0xff0000),		2. правило: перевернуть изображение в градусах и поменять цвет фона
	 *		)
	 *	),
	 */
	public static function configPresets() 
	{
		return array(
		'NewsPreview300x200' => array(
			'cacheIn' => self::$pathImg.'NewsPreview300x200',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 300,
					'height' =>200
				),
				// 'rotate'=>array('degrees'=>30, 'background'=>0xff0000),
			)
		),//profile100x75
		'100x75' => array(
			'cacheIn' => self::$pathImg.'100x75',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 100,
					'height' => 75
				),
				// 'rotate'=>array('degrees'=>30, 'background'=>0xff0000),
			)
		),
		'150x200' => array(
			'cacheIn' => self::$pathImg.'150x200',
			'actions' => array(
				'scaleAndCrop' => array(
				'width' => 150,
				'height' => 200
				),
				// 'rotate'=>array('degrees'=>30, 'background'=>0xff0000),
			)
		),
		'300x300' => array(
			'cacheIn' => self::$pathImg.'300x300',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 300,
					'height' => 300
				),
				//'rotate'=>array('degrees'=>90, 'background'=>0xff0000),
			)
		),
		'250x250rotate45' => array(
			'cacheIn' => self::$pathImg.'250x250rotate45',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 250,
					'height' => 250
				),
				'rotate'=>array('degrees'=>45, 'background'=>0x000000),
			)
		),
		'50x50' => array(
			'cacheIn' => self::$pathImg.'50x50',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 50,
					'height' => 50
				),
			'rotate'=>array('degrees'=>30, 'background'=>0x000000),
			)
		),

		'licReal' => array(
			'cacheIn' => self::$pathImg.'150x200',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 900,
					'height' => 1200
				),
			
			)
		),

		'lic150x200' => array(
			'cacheIn' => self::$pathImg.'150x200',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 150,
					'height' => 200
				),
			
			)
		),
		'174x200' => array(
			'cacheIn' => self::$pathImg.'174x200',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 174,
					'height' => 200
				),
			
			)
		),
		'dirButton' => array(
			'cacheIn' => self::$pathImg.'70x120',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 70,
					'height' => 120
				),
			
			)
		),

		'banner110x28' => array(
			'cacheIn' => self::$pathImg.'110x28',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 110,
					'height' => 28
				),
			
			)
		),

		'slide670' => array(
			'cacheIn' => self::$pathImg.'150x80',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 670,
					'height' => 170
				),
			
			)
		),
		'slide790' => array(
			'cacheIn' => self::$pathImg.'790x201',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 790,
					'height' => 201
				),
			
			)
		),
		'slide920' => array(
			'cacheIn' => self::$pathImg.'150x80',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 920,
					'height' => 234
				),
			
			)
		),
		'150x80' => array(
			'cacheIn' => self::$pathImg.'150x80',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 150,
					'height' => 80
				),
			
			)
		),
		'100x80' => array(
			'cacheIn' => self::$pathImg.'100x80',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 100,
					'height' => 80
				),
			
			)
		),
		'300x100' => array(
			'cacheIn' => self::$pathImg.'very_small',
			'actions' => array(
				'scaleAndCrop' => array(
					'width' => 300,
					'height' => 100
				),
				// 'rotate'=>array('degrees'=>30, 'background'=>0xff0000),
			),
		),
		);
	}
}