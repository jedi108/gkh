<?php


define('ROOT',YiiBase::getPathOfAlias('webroot').'/');			// путь от корня системы

define('PATH_PIC','file/pic/');		// для картинок для web
define('ROOT_PIC',ROOT.PATH_PIC);		// для картинок для системы

define('PATH_TMP','file/tmp/');		// tmp для картинок для web
define('ROOT_TMP',ROOT.PATH_TMP);		// tmp для картинок для системы


interface MediaAbstract
{
	/**
	 * Сохранить изображение
	 * 
	 * Возвращает PATH_{TMP,PIC и тд}/{destFolder}/{date:YYYYMMDD}/{imgName}%s.{ext}
	 * 
	 * @param string $destFolder - путь назначения (Указывается только модуль т.е. место после /file/pic/ где нужно сохранить)
	 * @param string $srcFile -исходный файл (передается в виде /file/.../{image name}%s.{ext})
	 * @param array $presets - презеты для генерации, только id презетов
	 * @return mixed - string:путь к сохраненному файлу; false: если ошибка
	 */
	static function saveImage($destFolder, $srcFile, $presets=array());

	/**
	 * Создание презетов исходного файла
	 *
	 * @param string $srcFile - исходый файл
	 * @param array $presets - презеты
	 * @return mixed - array: список созданных презетов; false: если ошибка
	 * 		array(
	 *			{id_preset}=>bool|path_of_file,
	 *			...,
	 *		)
	 */
	static function createImageByPreset($srcFile, $presets=array());

	/**
	 * Удаление изображения
	 * 
	 * Удаление исходного файла, если есть презеты то и презетов
	 * 
	 * @param string $srcFile - исходный файл
	 * @return bool 
	 */
	static function deleteImage($srcFile);

	/**
	 * Перемещение изображения
	 * 
	 * @param string $srcFile - исходный файл
	 * @param string $destFolder - папка назначения от корня системы 
	 * @param string $newName - новое имя
	 * @return mixed string: перемещенный файл; false: ошибка
	 */
	static function moveImage($srcFile, $destFolder, $newName=null);

	/**
	 * Проверка существования изображения
	 *
	 * Если указан только $file - проверяется исходный файл, возвращается bool
	 * Если указан $presets - проверяется весь список, возвращается array вида 
	 * 		array(
	 *			{id_preset}=>bool,
	 *			...,
	 *		)
	 *
	 * @param string $file - исходный файл
	 * @param array $presets - презеты
	 * @param bool $create - флаг на создание не существующих изображений
	 * @return mixed bool: если не указан презет; array:если указан презет
	 */
	static function existImage($file, $presets=array(), $create=false);

	/**
	 * Получить название файла с полным веб-путем
	 *
	 * Если указан только $srcFile - возвращается только его веб-путь
	 * Если указан $presets - возвращается array вида 
	 * 		array(
	 *			{id_preset}=>string,
	 *			...,
	 *		)
	 *
	 * @param string $srcFile - исходнй файл
	 * @param array $presets - презеты
	 * @return mixed string: если не указан презет; array:если указан презет
	 */
	public static function getImageName($srcFile, $presets=null);
	
	/**
	 * Возсращает Имя файла для показа в Веб
	 * 
	 * @param string $srcFile
	 * @param mixed object:если передается модель; array:если так, то нужно минимальные параметры указать, чтоб в будущем можно было указывать ЦДНки;
	 * @return string FullPath to the Web
	 */
	public static function getWebName($srcFile,$model=null);
}

class Media 
//implements MediaAbstract
{
	static function saveImage($destFolder, $srcFile, $presets=array())
	{
		if ( empty($srcFile) || empty($destFolder) || !is_dir( ROOT_PIC.$destFolder ) ) return false;
		$origFileName = basename($srcFile);					// сохраним (только)имя файла с %s
		$srcFile = self::getImageName($srcFile);
		if ( !is_file(ROOT.$srcFile) ) return false;		// Должно приходить в виде /file/...
		
		$destFolder = $destFolder.'/'.date('Ymd').'/';
		$_destFolder = ROOT_PIC.$destFolder; // директория назначения от корня системы
		#$destFolder = '/'.$destFolder;
		
		if ( !is_dir($_destFolder) ) {
			$res = self::mkdir($_destFolder);
			if ( $res === false ) return false;	// если не удалось создать директорию
		}
		
		#var_dump(ROOT.$srcFile, $_destFolder.basename($srcFile), $_destFolder.$origFileName);
		#$mvRes = @rename( ROOT.$srcFile , $_destFolder.basename($srcFile) );
		$mvRes = @copy( ROOT.$srcFile , $_destFolder.basename($srcFile) );
		
		if ( $mvRes ) {
			/*** make presets ***/
			if ( sizeof($presets) > 0 ) {
				#var_dump($destFolder.basename($srcFile),$presets);
				#TODO учитывать ли результат выполнения этого скрипта?
				self::createImageByPreset($destFolder.basename($srcFile),$presets);
			}
			/*** END make presets ***/
			
			return $destFolder.$origFileName;
			
		} else {
			return false;
		}
		return false;
	}

	static function isTmpPic($srcFile)
	{
		return 'file/tmp';
		if ( strpos($srcFile, PATH_TMP) === false ){
			return false;
		} else {
			return true;
		}
	}
	
	static function trim($src,$f='/')
	{
		return trim($src,$f);
	}
	
	static function createImageByPreset($srcFile, $presets=array())
	{

		if ( empty($srcFile) ) return false;
		$srcFile = trim( self::getImageName($srcFile) ,'/');	// clean '%s' & trims '/'
		
		if ( !self::isTmpPic($srcFile) ){
			// not tmp
			$isTmp = false;
			$_srcFile = ROOT_PIC.$srcFile;
		} else {
			// tmp file
			$isTmp = true;
			$_srcFile = ROOT.$srcFile;
		}

		if ( !is_file($_srcFile) || ! sizeof($presets) ) return false;
		#var_dump($_srcFile); die('ddd');
		
		$ret = array();
		$_t = explode('/', trim($srcFile,'/') );
		if ( $isTmp ) $_t = null;
			else $_t = $_t[0];

		for( $i=0,$max=sizeof($presets); $i<$max; ++$i ) {
			#echo "<hr>::::::<hr>{$presets[$i]}, {$_srcFile}, {$_t}<br>";
			$ret[ $presets[$i] ] = Yii::app()->image->createUrl($presets[$i], $_srcFile, $_t);
		}
		
		return $ret;
		
	}
	
	static function deleteImage($srcFile)
	{
		$_srcFile = ROOT_PIC.self::getImageName($srcFile);
		if ( !is_file($_srcFile) ) return false;		// если нет оригинала, то нечего делать!
		
		// получим все презеты по шаблону, оригинал удалим самым последним
		$presetFiles = glob(ROOT_PIC.sprintf($srcFile,'_*'));
		$presetFilesC = sizeof($presetFiles);
		$fDelPresets = true;
		
		if ( $presetFilesC > 0 ) {
			// если презеты есть, то удалим их все
			for( $i=0; $i<$presetFilesC; ++$i ) {
				$r = @unlink($presetFiles[$i]);
				if ( !$r ) {
					// если не получается удалить, меняем флаг, и выходим из цикла
					$fDelPresets = false;
					break;
				}
			}
		} 
		
		if ( $fDelPresets===true ) {
			// удаление файла оригинала
			#echo "<h1>DELETE SRC:: ".( ROOT_PIC.sprintf($srcFile,'') )."</h1>";
			return @unlink( $_srcFile );
		} else {
			#echo "<h1>CANT SRC:: ".( ROOT_PIC.sprintf($srcFile,'') )."</h1>";
			return false;
		}
		
		return false;
	}
	
	static function moveImage($srcFile, $destFolder, $newName=null)
	{
		$srcFileName = self::getImageName($srcFile);
		if (!(self::existImage($srcFileName)))
			return false;

		$ext = function($file) {
			$path_parts = pathinfo($file);
			if (isset($path_parts['extension']))
				return $path_parts['extension'];
			else
				return false;
		};

		if ($newName) {
			$newName = ($ext($newName)) ? $newName : $newName.'.'.$ext($srcFile);
			$destFileName = $newName;
		} else
			$destFileName =  basename($srcFileName);

		return rename( ROOT.$srcFileName , ROOT.$destFolder.$destFileName );
	}
	
	static function existImage($file, $presets=array(), $create=false)
	{
		//echo 'fnme=========='.self::getImageName($file,$presets);
		$isExists = function($fileName, $presetik='') {
			return is_file( ROOT.sprintf($fileName,$presetik) );
		};

		if ( $presets==array() )
			return $isExists($file);
		else {
			if ( is_array($presets) ) {
				$th = array();
				foreach( $presets as $preset ) {
					// Если расскоментировать, то можно увидеть пути
					//$th['file'][$preset] = sprintf($file,'_'.$preset); 
					$th[$preset] = $isExists($file, '_'.$preset) ? true : ($create) ? self::createImageByPreset($file, array($preset)) ? true: false : false;

				}
			} else {
				return $isExists($file, $presets);
			}
		}

		return $th;
	}
	
	public static function getImageName($srcFile, $presets=null)
	{
		if ( empty($srcFile) ) return false;
		if (is_null($presets))
			return sprintf($srcFile,'');
		else {
			/*** presets ***/
			if ( is_array($presets) ) {
				$th = array();
				foreach( $presets as $preset ) {
					$th[] = sprintf($srcFile,'_'.$preset);
				}
			} elseif( $presets===null ) {
				return sprintf($srcFile,'');
			} else {
				return sprintf($srcFile,'_'.$presets);
			}
			/*** END presets ***/
		}
		return $th;
	}
	
	public static function getWebName($srcFile,$model=null)
	{
		$srcFile = self::getImageName($srcFile);
		
		if ( strpos($srcFile,PATH_PIC) === false ) {
			$srcFile = PATH_PIC.$srcFile;
			if ( is_file( ROOT.$srcFile ) ) return '/'.$srcFile;
				else return false;
		} else {
			if ( is_file($srcFile) ) return $srcFile;
				else return false;
		}
		
		#if ( is_file(ROOT.$srcFile)
		
		return $srcFile;
	}
	
	/**
	 * Создает директорию
	 * 
	 * По идее должна создаваться директория в /file/pic/.../{date}
	 * 
	 * @param string Path to create
	 * @return mixed string: if all correct; false:some error;
	 */
	private static function mkdir($path)
	{
		if ( $path == ROOT || $path == ROOT_PIC ) return false;
		if ( is_dir($path) ) return $path;
		$res = @mkdir($path,0775);
		if ( $res ) return $path;
			else return false;
	}


  /**
   *
   * Возвращает картинку по правилу @preset
   * 
   * @return string возврашает картинку
   * @param string $src - путь и имя исходного файла
   * @param string $preset - алиас правила преобразования изображения - конфигурируется в *application.modules.images.components.MediaConfig.php*
   * @param string $pathModule=null название модуля для генерации путя к картинке например 'user' будет сгенерирован путь /file/pic/user
   * @param string $htmlOptions=array() - class, id, alt, style....
   *
   * PS Презеты или папку презетов /file/pic/imagecache или /file/pic/$module_name/imagecache можно удлалять смело, всё равно они сгенерируются при необходимости, генерируются мгновенно и один раз
   */
	public static function img($src, $preset, $pathModule=null, $htmlOptions=array())
	{
		//if ($src=='/images/' || $src=='') return '';
		$sitePath = YiiBase::getPathOfAlias('webroot');
		$src = self::GetImage($src, '');
		//Возврашает преобразованную картинку, если её нет, генерирует и возвращает
		$thumb = Yii::app()->image->createUrl($preset, $sitePath.$src, $pathModule);
		if ($thumb) {
			$htmlOptions['src']=$thumb;
			return CHtml::tag('img', $htmlOptions);
		}
		return false;
	}

	public static function imgPath($src, $preset, $pathModule=null, $htmlOptions=array())
	{
		if ($src=='/images/' || $src=='') return '';
		$sitePath = YiiBase::getPathOfAlias('webroot');
		$src = self::GetImage($src, '');
		//Возврашает преобразованную картинку, если её нет, генерирует и возвращает
		$thumb = Yii::app()->image->createUrl($preset, $sitePath.$src, $pathModule);
		if ($thumb) {
			return $thumb;
		}
		return false;
	}

	static function GetImage($path, $preset='')
	{
		//Дополнить проверкой по Server_id
		return sprintf($path, $preset);
	}
	
	
	
	static function SavePreset($src, $preset, $pathModule=null)
	{
		if ($src=='/file/temp/' || $src=='') return '';
		$sitePath = YiiBase::getPathOfAlias('webroot').PATH_PIC;
		$src = strtr($src,array( PATH_PIC => '' ));
		//Возврашает путь к картинке с названием файла, если её нет, генерирует и возвращает
		$thumb = Yii::app()->image->createUrl($preset, $sitePath.$src, $pathModule);
		#var_dump( $sitePath.$src , $thumb);
		if ($thumb) {
			return $thumb;
		}
		return false;
	}

	
  /**
   * Сохраняем кадр из видео
   * @file - пусть+файл к исходному видео файлу
   *  
   * if ( !extension_loaded('ffmpeg') ) throw new Exception( 'ffmpeg не загружен.' );
   *   $img= Video::getImageFromVideo(Yii::app()->request->baseUrl.'/uploads/1.mp4');
   *   echo '<img src="'.$img.'"/>';  
   */
	public static function getImageFromVideo($file)
	{ 
		$sitePath = YiiBase::getPathOfAlias('webroot');
		$frame = 150; 
		// получаем кадр
		$movie = new ffmpeg_movie($sitePath . $file);
		$image = $movie->getFrame($frame);
		$show_img = $image->toGDImage();
		$image_url = $show_img;
		imagejpeg ($image_url, $sitePath.$file.'.jpg', 100);
		return $file.'.jpg';
	}

	
	/**
	 * Сохранение картинкид
	 */
	// static function SaveImage($moduleName=null, $src, $preset=array() )
	// {
	// 	$rootPath = Y::getRootPath();
		
	// 	$origFileName = basename($src);
		
	// 	$src = self::GetImage($src);
	// 	$srcFileName = basename($src);
	// 	if ( empty($moduleName) || !is_file( $rootPath.$src )   ) return false;
		
	// 	$pathPic = $moduleName.'/'.date('Ymd').'/';
	// 	$rootPathPic = $rootPath.'/file/pic/'.$pathPic;
		
		
	// 	if ( is_dir($rootPathPic) ) {
	// 		$mvRes = rename( $rootPath.$src , $rootPath.PATH_PIC.$moduleName.$srcFileName );
	// 		#FIXME вкл.выкл
	// 		#$mvRes = copy( $rootPath.$src , $rootPathPic.$srcFileName );
			
	// 		/*** presets creation ***/
	// 		if ( sizeof($preset) > 0 ) {
	// 			$th = array();
	// 			foreach( $preset as $v ) {
	// 				$th[] = self::SavePreset( $pathPic.$srcFileName, $v, $moduleName );
	// 			}
	// 		}
	// 		/*** END presets creation ***/
			
	// 		unset($th);
			
	// 		if ( $mvRes ) {
	// 			return $pathPic.$origFileName;
	// 		} else return false;
			
	// 	} else return false;
	// }
	
	// static function SavePreset($src, $preset, $pathModule=null)
	// {
	// 	if ($src=='/file/temp/' || $src=='') return '';
	// 	$sitePath = YiiBase::getPathOfAlias('webroot').'/file/pic/';
	// 	$src = strtr($src,array( '/file/pic/' => '' ));
	// 	//Возврашает путь к картинке с названием файла, если её нет, генерирует и возвращает
	// 	$thumb = Yii::app()->image->createUrl($preset, $sitePath.$src, $pathModule);
	// 	#var_dump( $sitePath.$src , $thumb);
	// 	if ($thumb) {
	// 		return $thumb;
	// 	}
	// 	return false;
	// }

}
