<?php
/* @var $this ArticleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Статьи',
);

$this->menu=array(
	array('label'=>'Создать статью', 'url'=>array('create')),
	array('label'=>'Управлять статьями', 'url'=>array('admin')),
);
?>

<h1>Статьи</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
