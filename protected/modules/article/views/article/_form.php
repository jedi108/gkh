<?php
/* @var $this ArticleController */
/* @var $model Article */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('search', "
$('.isteasers').click(function(){
	$('.is_teaser').toggle();
	return false;
});

function isteaser(){
    $('.is_teaser').toggle();
};

");
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'article-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

        <hr/>
	<div class="row">
                <?php echo $form->labelEx($model,'is_teaser'); ?>
                <?php echo $form->checkBox($model, 'is_teaser', array('onclick'=>"$('.search-form').toggle();")); ?>
                <?php echo $form->error($model,'is_teaser'); ?>
	</div>
        
 
        
        <div class="row search-form" style="display: <?= $model->is_teaser==1?"block":"none"; ?>">
            <?php echo $form->labelEx($model,'teaser'); ?>
            <?php echo $form->textArea($model,'teaser',array('rows'=>6, 'cols'=>50)); ?>
            <?php echo $form->error($model,'teaser'); ?>
        </div>
        <hr/>

	<div class="row">
		<?php echo $form->labelEx($model,'body'); ?>
		<?php $this->widget('jckeditor', array('model'=>$model)); ?>
		<?php echo $form->error($model,'body'); ?>
	</div>
        
	<div class="row">
	<?php echo $form->labelEx($model,'date_created'); ?>
	<?php
	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
	    'language'=>'ru',
	    'model'=>$model,
	    'attribute'=>'date_created',
	    'options'=>array(
	        'showAnim'=>'fold',
	        'dateFormat'=>'yy-mm-dd',
	    ),
	   
	    'htmlOptions'=>array(
	        'style'=>'height:20px;'
	    ),
	)); ?>
	</div>
        
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->