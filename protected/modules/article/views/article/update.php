<?php
/* @var $this ArticleController */
/* @var $model Article */

$this->breadcrumbs=array(
	'Articles'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Список статей', 'url'=>array('index')),
	array('label'=>'Создать статью', 'url'=>array('create')),
        array('label'=>'Просмотр Статьи', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управлять статьями', 'url'=>array('admin')),
);
?>

<h1>Update Article <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>