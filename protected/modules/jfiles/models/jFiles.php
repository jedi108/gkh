<?php

/**
 * This is the model class for table "{{files}}".
 *
 * The followings are the available columns in table '{{files}}':
 * @property integer $id
 * @property string $file
 * @property string $name
 * @property string $type
 * @property integer $id_page
 */
class jFiles extends CActiveRecord
{

	public static $onUpdates;
	public static $onInserte;
	public static $onDeletes;

	public static $nameLinkField='id_page';
	private static $LinkFieldValue = 1;

	public static function echoVar($vari, $id_page)
	{
		self::$LinkFieldValue = $id_page;

		$sql = "SELECT * FROM t_files WHERE id_page = %s ORDER BY id DESC";
		$rows = sql::command($sql, $id_page)->queryAll();

		// echo 'id p = '. $id_page . '<br/>';
		// CVarDumper::dump($rows, 100, true);
		// echo '<br/>';
		// CVarDumper::dump($vari, 100, true);

		// echo '============================================<br/>';

		// $add = array();
		// foreach ($vari as $key => $field) {
		// 	//Add
		// 	if ($field['id']=='') {
		// 		$add[] = $vari[$key];
		// 	}
		// }
		// echo '<br/>=========ADD============<br/>';
		// CVarDumper::dump($add, 100, true);

		foreach ($rows as $key_s => $sql_value) {
			$isExists = false;
			foreach ($vari as $key_p => $field) {
				if ($field['id']!='') {
					if ($sql_value['id'] == $field['id']) { 
						//$onUpdates[] = $field['id'];
						self::verUpdate( $field['id'], $sql_value, $field);
						unset($rows[$key_s]); // TO REAL DELETE ==================
						unset($vari[$key_p]);
					}
				}
			}
		}

		
		
		
		self::verDelete($rows);
		self::daoRun(self::$onDeletes);
		// echo '<br/>=========DELETE SQL============<br/>';
		// CVarDumper::dump(self::$onDeletes, 100, true);

 

		self::verInsert($vari);
		self::daoRun(self::$onInserte);
		// echo '<br/>===add files======ADD DEFAULT VALUE POST_ID============<br/>';
		// CVarDumper::dump(self::$onInserte, 100, true);


		self::daoRun(self::$onUpdates);
		// echo '<br/>=========TO VERIFY , then update============<br/>';
		// CVarDumper::dump(self::$onUpdates, 100, true);

	}

	private static function verUpdate($idx, $arr_sql, $arr_post)
	{
		$tmp = array();
		$diff = array_diff($arr_post, $arr_sql);
		//self::$onUpdates[$idx] = $diff;
		if ($diff !== array()) {
			foreach ($diff as $key => $value) {
				# code...
				$tmp[] =  $key . '="' . $value .'"'; //"  WHERE id ="' .$idx. '"<br/>';
			}
		}
		if ( $tmp !== array() )
			self::$onUpdates[] = 'UPDATE {{files}} SET ' . implode(', ', $tmp) . ' WHERE id ="' .$idx. '"';
	}

	private static function verInsert($arr)
	{

		if ($arr !== array()) {
			foreach ($arr as $key => $fields) {
				$tmp_key = array();
				$tmp_val = array();
				foreach ($fields as $key => $value) {
					if ($key!=='' && $value!=='') {
						$tmp_key[] =  $key;
						$tmp_val[] =  '"'.$value.'"';
					}
				}
				if ( $tmp_key !== array() &&  $tmp_val !== array() )
					self::$onInserte[] = 'INSERT INTO {{files}} (' . self::$nameLinkField . ', ' . implode(', ', $tmp_key) . ') VALUES (' . self::$LinkFieldValue . ', ' . implode(', ', $tmp_val) .  ')';
			}
		}
		
	}

	private static function verDelete($arr)
	{
		$tmp = array();
		$img = array();
		foreach ($arr as $key => $value) {
			$tmp[] = $value['id'];
			$img[] = $value['file'];
			//DELETE IMAGE from DISK
		}
		if ($tmp !== array() )
		self::$onDeletes = 'DELETE FROM {{files}} WHERE id IN ('. implode(', ', $tmp) .')';
	}



	private static function daoRun($sql)
	{
		if ($sql==='' || is_null($sql) || !isset($sql)) return false;
		$new = (count($sql)>1)? implode(';', $sql) : $sql = (is_array($sql)? $sql[0]: $sql );
		
		//echo '<br/>===================<br/>';
		//CVarDumper::dump($sql, 100, true);
		//CVarDumper::dump($new, 100, true);
		if (Yii::app()->db->createCommand($new)->execute()) return true;
		//die();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return jFiles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{files}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('file, name, id_page', 'required'),
			array('id_page', 'numerical', 'integerOnly'=>true),
			array('file, name', 'length', 'max'=>255),
			array('type', 'length', 'max'=>8),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, file, name, type, id_page', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'page' => array(self::HAS_MANY, 'Page', 'id_page'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'file' => 'File',
			'name' => 'Name',
			'type' => 'Type',
			'id_page' => 'Id Page',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('id_page',$this->id_page);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
