<?php
/* @var $this FeedbackController */
/* @var $model Feedback */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'feedback-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля отмеченные <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php if(Yii::app()->user->isAdminSite || Yii::app()->user->isAdmin) { ?>
            <div class="row">
                Подтвержденные отзывы будут размещены на сайте
                <?php echo $form->labelEx($model,'confirmed'); ?>
                <?php echo $form->checkBox($model, 'confirmed'); ?>
                <?php echo $form->error($model,'confirmed'); ?>
            </div>
        <?php } ?>
        
	<div class="row">
		<?php echo $form->labelEx($model,'cat'); ?>
		<?php echo $form->dropDownList($model,'cat',$model->cats); ?>
		<?php echo $form->error($model,'cat'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>	
	
	<div class="row">
		<?php echo $form->labelEx($model,'body'); ?>
		<?php 
			$this->widget('ext.yiiext.widgets.ckeditor.ECKEditor',array(
				'model'=>$model,                # Data-Model (form model)
				'attribute'=>'body',         # Attribute in the Data-Model
				'editorTemplate'=>'basic',		

				'options'=>array(
									'toolbar'=>array(
						array( //'Source', 
							'-', 'Bold', 'Italic', 'Underline', 'Strike','', "NumberedList","BulletedList" ),
					   //array( 'Image', 'Link', 'Unlink', 'Anchor' ),
					),
		            //'filebrowserBrowseUrl'=>CHtml::normalizeUrl(array('/site/browse')),
		        ),
			) );
		?>
		<?php echo $form->error($model,'body'); ?>
	</div>

	<?php if (Yii::app()->user->isAdmin || Yii::app()->user->isAdminSite) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'answer'); ?> Администратора сайта
		<?php 
			$this->widget('ext.yiiext.widgets.ckeditor.ECKEditor',array(
				'model'=>$model,                # Data-Model (form model)
				'attribute'=>'answer',         # Attribute in the Data-Model
				'editorTemplate'=>'basic',		

				'options'=>array(
									'toolbar'=>array(
						array( //'Source', 
							'-', 'Bold', 'Italic', 'Underline', 'Strike','', "NumberedList","BulletedList" ),
					   //array( 'Image', 'Link', 'Unlink', 'Anchor' ),
					),
		            //'filebrowserBrowseUrl'=>CHtml::normalizeUrl(array('/site/browse')),
		        ),
			) );
		?>
		<?php echo $form->error($model,'body'); ?>
	</div>	
	<?php } ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Отправить' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->