<?php
/* @var $this FeedbackController */
/* @var $model Feedback */

$this->breadcrumbs=array(
	'Feedbacks'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Список подтвержденных отзывов', 'url'=>array('index')),
	array('label'=>'Создать отзыв', 'url'=>array('create')),
	array('label'=>'РЕДАКТИРОВАТЬ отзыв', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить отзыв', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление отзывами', 'url'=>array('admin')),
);
?>

<h1>Просмотр отзыва #<?php echo $model->id; ?></h1>

<?php 
$dataProvider=$model->search();
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
