<?php
/* @var $this FeedbackController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Отзывы',
);

$this->menu=array(
	array('label'=>'Создать отзыв', 'url'=>array('create')),
	array('label'=>'Управление отзывами', 'url'=>array('admin')),
);
?>

<h1>Отзывы и предложения</h1>

<?php echo CHtml::button('Оставить свой отзыв',array('submit'=>'/feedback'));?>
<br>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
