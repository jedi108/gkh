<?php
/* @var $this FeedbackController */
/* @var $model Feedback */

$this->breadcrumbs=array(
	//'Feedbacks'=>array('index'),
	'Обратная связь',
);

$this->menu=array(
	array('label'=>'Список подтвержденных отзывов', 'url'=>array('index')),
	array('label'=>'Управление отзывами', 'url'=>array('admin')),
);
?>

<h1>Ваш отзыв</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>