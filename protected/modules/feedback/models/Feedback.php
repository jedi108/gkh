<?php

/**
 * This is the model class for table "{{feedback}}".
 *
 * The followings are the available columns in table '{{feedback}}':
 * @property integer $id
 * @property integer $cat
 * @property string $body
 * @property string $email
 * @property string $name
 * @property string $timestamp
 */
class Feedback extends CActiveRecord
{
        public $cat=0;
        public $confirms = array(
            '0'=>'нет',
            '1'=>'да',
        );
        public $cats = array(
		'0'=>'о работе комбината',
		'1'=>'вопросы и предложения, связанные с функционированием сайта',
	);	
        public $catsAdm = array(
            '0'=>'комбинат',
            '1'=>'caйт',
        );
        
        public function confirmAdm()
        {
            return $this->confirms[$this->confirmed];
        }
        
        public function getCatsAdm()
        {
            if(array_key_exists($this->cat, $this->catsAdm)) {
                return $this->catsAdm[$this->cat];
            } else {
                return $this->cat;
            }
        }

        public function getCatsUser()
        {
            if(array_key_exists($this->cat, $this->catsAdm)) {
                return $this->cats[$this->cat];
            } else {
                return $this->cat;
            }
        }
        
        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Feedback the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{feedback}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cat, body, email, name', 'required'),
			array('cat, confirmed', 'numerical', 'integerOnly'=>true),
			array('body, answer', 'length', 'max'=>512),
			array('email', 'email', 'checkMX'=>true),
			array('name', 'length', 'max'=>255),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, confirmed, cat, body, email, name, timestamp', 'safe', 'on'=>'search'),
		);
	}
	
	public function defaultScope()
	{
	    return array('order' => 'timestamp DESC');
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'confirmed'=>'Подтвержден',
			'cat' => 'Тема',
			'body' => 'Текст сообщения',
			'email' => 'Ваш Email',
			'name' => 'Представьтесь',
			'answer' => 'Ответ',
			'timestamp' => 'Дата-время',
		);
	}

	protected function afterSave()
	{
		if ($this->isNewRecord) {
				$message = "<div>Отправлено с формы обратной связи: http://specialist-gkh.ru/feedback</div>".
						   "<div>" . $this->cats[$this->cat] . "</div>".
						   "<div>" .$this->body . "</div>".
						   "<div>ОТ: " . $this->name . "</div>".
						   "<div>" .$this->email . "</div>";

				$subject = "gkh: обратная связь";

				$headers = "From: admin@specialist-gkh.ru"."\r\n".
						   "Reply-To: ".$this->email."\r\n".
						   'X-Mailer: PHP/' . phpversion()."\r\n".
						   'Content-type: text/html; charset="utf-8"';   

				mail('vadimka108@ya.ru', $subject, $message, $headers);
				usleep(500000);
				//mail('108vadim@mail.ru', $subject, $message, $headers);
				mail('nadaks@mail.ru', $subject, $message, $headers);
				usleep(500000);
		}
	}	
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cat',$this->cat);
                $criteria->compare('confirmed',$this->confirmed);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('timestamp',$this->timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
                        'pageSize'=>100
             ),
		));
	}
}