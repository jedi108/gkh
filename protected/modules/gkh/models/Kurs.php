<?php

/**
 * This is the model class for table "{{kurs}}".
 *
 * The followings are the available columns in table '{{kurs}}':
 * @property integer $id
 * @property string $name
 * @property string $img_logo
 * @property string $img_slide
 * @property integer $id_org
 * @property string $body
 */
class Kurs extends CActiveRecord
{

	public function isKurs()
	{
		return $this->id;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Kurs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kurs}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('id_org', 'numerical', 'integerOnly'=>true),
			array('name, img_logo, img_slide', 'length', 'max'=>255),
			array('body', 'length', 'max'=>65535),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, img_logo, img_slide, id_org, body', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'specialnost' => array(self::HAS_MANY, 'Specializ', 'id_kurs'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название курса',
			'img_logo' => 'Лого',
			'img_slide' => 'Слайд',
			'id_org' => 'Id Org',
			'body' => 'Текстовое описание',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('img_logo',$this->img_logo,true);
		$criteria->compare('img_slide',$this->img_slide,true);
		$criteria->compare('id_org',$this->id_org);
		$criteria->compare('body',$this->body,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}