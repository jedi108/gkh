<?php

/**
 * This is the model class for table "{{schedule}}".
 *
 * The followings are the available columns in table '{{schedule}}':
 * @property integer $id
 * @property string $datetime
 * @property integer $id_specializ
 * @property integer $id_org
 * @property integer $id_prog
 */
class Schedule extends CActiveRecord
{
        public $id_direction;
        
        public function getDirection()
        {
            return CHtml::listData(Direction::model()->findAll(array('order'=>'name')),'id','name');
        }
        public function getSpecializ()
        {
            return CHtml::listData(Specializ::model()->findAll(array('order'=>'name')),'id','name');
        }
        public function getOrg()
        {
            return CHtml::listData( Org::model()->findAll('orglink = 1',array('order'=>'adr_fact')), 'id', 'city');
        }
        public function getProg()
        {
            return CHtml::listData(Prog::model()->findAll(array('order'=>'name')),'id','name');
        }
        public function defaultScope()
        {
            return array(
                 
                    'order'=>'datetime DESC',
                
            );
        }

        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Schedule the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{schedule}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_specializ, id_org, id_prog', 'required'),
			array('id_specializ, id_org, id_prog', 'numerical', 'integerOnly'=>true),
			array('datetime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, datetime, id_direction, id_specializ, id_org, id_prog', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'iSpecializ'=>array(self::BELONGS_TO, 'Specializ', 'id_specializ'),
                    'iOrg' => array(self::BELONGS_TO, 'Org', 'id_org'),
                    'iProg' => array(self::BELONGS_TO, 'Prog', 'id_prog'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'datetime' => 'Datetime',
			'id_specializ' => 'Специальность',
			'id_org' => 'Филиал',
			'id_prog' => 'Программа обучения',
                        'id_direction'=>'Направления',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('id_specializ',$this->id_specializ);
                //$criteria->compare('iSpecializ->idFDirection->id', $this->id_direction);
		$criteria->compare('id_org',$this->id_org);
		$criteria->compare('id_prog',$this->id_prog);
                
                //if($this->id_direction!='') {
                    //$criteria->with=array('iSpecializ.idFDirection'=>
                                    //array(
                                        //'condition'=>'idFDirection.id = :id_direction',
                                        //'params'=>array(
                                         //   ':id_direction'=>$this->id_direction,
                                       // ),
                                    //),
                    //);
                   // $criteria->together=true;
               // }
                
		return new CActiveDataProvider($this, array(
                        'pagination'=>array(
		        //'route'=>'/journal/article/admin',
                        ),
			'criteria'=>$criteria,
		));
	}
}