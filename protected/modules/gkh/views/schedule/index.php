<?php
/* @var $this ScheduleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('Schedule','Schedules'),
);

$this->menu=array(
	array('label'=>Yii::t('Schedule','Create Schedule'), 'url'=>array('create')),
	array('label'=>Yii::t('Schedule','Manage Schedule'), 'url'=>array('admin')),
);
?>

<h1><?=Yii::t('Schedule','Schedules')?></h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'schedule-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		array(
                    'filter'=>false,
                    'header'=>'Дата',
                    'name'=>'datetime',
                    'value'=>'Yii::app()->dateFormatter->format("d MMM hh:ss",strtotime($data->datetime))',
                ),
//		array(
//                    'header'=>'Время',
//                    'filter'=>false,
//                    'name'=>'datetime',
//                    'value'=>'Yii::app()->dateFormatter->format("hh:ss",strtotime($data->datetime))',
//                ),            
               // array(
                  //  'filter'=>Schedule::model()->direction,
                 //   'name'=>'id_direction',
                    //'value'=>'$data->iSpecializ->idFDirection->name',
               // ),
		array(
                    'filter'=> Schedule::model()->specializ,
                    'name'=> 'id_specializ',
                    'value'=> '$data->iSpecializ->name',
                ),
		array(
                    'filter'=> Schedule::model()->prog,
                    'name'=>'id_prog',
                    'value'=>'$data->iProg->name',
                ),
		array(
                    'filter'=>Schedule::model()->org,
                    'name'=> 'id_org',
                    'value'=>'$data->iOrg->city',
                ),
//		array(
//			'class'=>'CButtonColumn',
//		),
	),
)); ?>

