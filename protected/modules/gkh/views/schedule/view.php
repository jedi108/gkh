<?php
/* @var $this ScheduleController */
/* @var $model Schedule */

$this->breadcrumbs=array(
	Yii::t('Schedule','Schedules')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('Schedule','List Schedule'), 'url'=>array('index')),
	array('label'=>Yii::t('Schedule','Create Schedule'), 'url'=>array('create')),
	array('label'=>Yii::t('Schedule','Update Schedule'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('Schedule','Delete Schedule'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('Schedule','Manage Schedule'), 'url'=>array('admin')),
);
?>

<h1>View Schedule #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'datetime',
		'id_specializ',
		'id_org',
		'id_prog',
	),
)); ?>
