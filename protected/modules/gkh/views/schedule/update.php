<?php
/* @var $this ScheduleController */
/* @var $model Schedule */

$this->breadcrumbs=array(
	Yii::t('Schedule','Schedules')=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('Schedule','List Schedule'), 'url'=>array('index')),
	array('label'=>Yii::t('Schedule','Create Schedule'), 'url'=>array('create')),
	array('label'=>Yii::t('Schedule','View Schedule'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('Schedule','Manage Schedule'), 'url'=>array('admin')),
);
?>

<h1>Update Schedule <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>