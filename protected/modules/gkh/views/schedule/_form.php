<?php
/* @var $this ScheduleController */
/* @var $model Schedule */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'schedule-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'datetime'); ?>
                <?php 
                $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'datetime',
                    'options'=>array(
                        'dateFormat'=>'yy-mm-dd',
                        'hourGrid' => 4,
                        'hourMin' => 9,
                        'hourMax' => 17,
                        'timeFormat' => 'h:m',
                        'changeMonth' => true,
                        'changeYear' => true,
                        ),
                    ));  
                ?>
		<?php echo $form->error($model,'datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_specializ'); ?>
		<?php echo $form->dropDownList($model,'id_specializ',$model->specializ);?>
		<?php echo $form->error($model,'id_specializ'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_org'); ?>
		<?php echo $form->dropDownList($model,'id_org',$model->org); ?>
		<?php echo $form->error($model,'id_org'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_prog'); ?>
		<?php echo $form->dropDownList($model,'id_prog',$model->prog); ?>
		<?php echo $form->error($model,'id_prog'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->