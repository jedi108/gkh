<?php
/* @var $this ScheduleController */
/* @var $model Schedule */

$this->breadcrumbs=array(
	Yii::t('Schedule','Schedules')=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>Yii::t('Schedule','List Schedule'), 'url'=>array('index')),
	array('label'=>Yii::t('Schedule','Manage Schedule'), 'url'=>array('admin')),
);
?>

<h1>Create Schedule</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>