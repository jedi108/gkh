<?php
/* @var $this ScheduleController */
/* @var $data Schedule */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datetime')); ?>:</b>
	<?php echo CHtml::encode($data->datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_specializ')); ?>:</b>
	<?php echo CHtml::encode($data->id_specializ); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_org')); ?>:</b>
	<?php echo CHtml::encode($data->id_org); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_prog')); ?>:</b>
	<?php echo CHtml::encode($data->id_prog); ?>
	<br />


</div>