<?php
/* @var $this ScheduleController */
/* @var $model Schedule */

$this->breadcrumbs=array(
	Yii::t('Schedule','Schedules')=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>Yii::t('Schedule','List Schedule'), 'url'=>array('index')),
	array('label'=>Yii::t('Schedule','Create Schedule'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#schedule-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Schedules</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'schedule-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		array(
                    'filter'=>false,
                    'name'=>'datetime',
                ),
//		array(
//                    'filter'=> Schedule::model()->specializ,
//                    'name'=> 'id_specializ',
//                    'value'=> '$data->iSpecializ->name',
//                ),
		array(
                    'filter'=> Schedule::model()->prog,
                    'name'=>'id_prog',
                    'value'=>'$data->iProg->name',
                ),
		array(
                    'filter'=>Schedule::model()->org,
                    'name'=> 'id_org',
                    'value'=>'$data->iOrg->adr_fact',
                ),
		array(
			'class'=>'CButtonColumn',
		),
	),
));


?>
