<?php
/* @var $this ScheduleController */
/* @var $model Schedule */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datetime'); ?>
		<?php echo $form->textField($model,'datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_specializ'); ?>
		<?php echo $form->textField($model,'id_specializ'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_org'); ?>
		<?php echo $form->textField($model,'id_org'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_prog'); ?>
		<?php echo $form->textField($model,'id_prog'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->