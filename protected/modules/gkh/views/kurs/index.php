<?php
$this->breadcrumbs=array(
	'Kurs',
);

$this->menu=array(
	array('label'=>'Создать курс', 'url'=>array('create')),		
	array('label'=>'Управление курсами', 'url'=>array('admin')),
);
?>

<h1>Курсы обучения</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
