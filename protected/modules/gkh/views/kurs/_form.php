<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kurs-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>


	<div class="row span-7">
		<?php
		/*		 * * uploadify ** */
		echo $form->labelEx($model, 'img_logo');
		echo $form->textField($model, 'img_logo', array('style' => 'display:none'));

		if ($model->img_logo == '')
			$file = '/file/no-photo.jpg';
		else
			$file = sprintf($model->img_logo, '');

		echo Media::img($file, '100x80', 'temp', array('id' => 'img-logo'));
		echo $form->error($model, 'img_logo');

		$this->widget('ext.EAjaxUpload.EAjaxUpload', array(
			'id' => 'one',
			'params' => array(
				'path' => '/file/pic/direction/logo/', //--- Путь к папке картинок
				'previewId' => '#img-logo', //--- <img src id="" (превью)
				'inputId' => '#Direction_img_logo', //--- <text type=input id="" (текстбокс)
				'preset' => '100x80', //--- Превьющка которая появится в <img src id="" 
			),
		));
		/*		 * * END uploadify ** */
		?>
	</div>


	<div class="row span-7">
		<?php
		/*		 * * uploadify ** */
		echo $form->labelEx($model, 'img_slide');
		echo $form->textField($model, 'img_slide', array('style' => 'display:none'));

		if ($model->img_slide == '')
			$file = '/file/no-photo.jpg';
		else
			$file = sprintf($model->img_slide, '');

		echo Media::img($file, '300x100', 'temp', array('id' => 'img-preview'));
		echo $form->error($model, 'img_slide');


		$this->widget('ext.EAjaxUpload.EAjaxUpload', array(
			'id' => 'two',
			'params' => array(
				'path' => '/file/pic/direction/slide/', //--- Путь к папке картинок
				'previewId' => '#img-preview', //--- <img src id="" (превью)
				'inputId' => '#Direction_img_slide', //--- <text type=input id="" (текстбокс)
				'preset' => '300x100', //--- Превьющка которая появится в <img src id="" 
			),
		));
		?>
	</div>
	<div style="overflow: hidden;height: 200px;"></div>

	<div class="row">
	<?php 
	$this->widget('ext.yiiext.widgets.ckeditor.ECKEditor',array(
		'model'=>$model,                # Data-Model (form model)
		'attribute'=>'body',         # Attribute in the Data-Model
		'editorTemplate'=>'full',		
		'options'=>array(
            'filebrowserBrowseUrl'=>CHtml::normalizeUrl(array('site/browse')),
        ),
	) );
	?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->