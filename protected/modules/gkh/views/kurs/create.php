<?php
$this->breadcrumbs=array(
	'Kurs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Список курсов', 'url'=>array('index')),
	array('label'=>'Управление курсами', 'url'=>array('admin')),
);
?>

<h1>Create Kurs</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>