<?php
$this->breadcrumbs=array(
	'Курсы'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Список курсов', 'url'=>array('index')),
	array('label'=>'Создать курс', 'url'=>array('create')),
	array('label'=>'Редактировать курс', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить курс', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление курсами', 'url'=>array('admin')),
);
?>

<h1><?php echo $model->name; ?></h1>

<?php 
if(count($model->specialnost)>0) {
	echo '<h3>Специальности:</h3><ul>';
	foreach ($model->specialnost as $key => $dataSpec) {
		echo '<li><span>';
		echo CHtml::link(
			$dataSpec['name'], 
			Yii::app()->createUrl(
				'/Specializ/view', array(
					'id'=>$dataSpec['id']))
			);
		echo '</span></li><br>';
	}
	echo '</ul>';
}
echo $model->body;
?>

<?php 
// $this->widget('zii.widgets.CDetailView', array(
// 	'data'=>$model,
// 	'attributes'=>array(
// 		'id',
// 		'name',
// 		'img_logo',
// 		'img_slide',
// 		'id_org',
// 		'body',
// 	),
// )); 

?>
