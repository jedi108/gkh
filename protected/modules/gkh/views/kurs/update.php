<?php
$this->breadcrumbs=array(
	'Kurs'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Список курсов', 'url'=>array('index')),
	array('label'=>'Создать курс', 'url'=>array('create')),
	array('label'=>'Просмотр курса', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление курсами', 'url'=>array('admin')),
);
?>

<h1>Редактировать курс: <?php echo $model->name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>