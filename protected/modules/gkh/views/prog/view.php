<?php
/* @var $this ProgController */
/* @var $model Prog */

$this->breadcrumbs=array(
	Yii::t('Prog','Progs')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('Prog','List Prog'), 'url'=>array('index')),
	array('label'=>Yii::t('Prog','Create Prog'), 'url'=>array('create')),
	array('label'=>Yii::t('Prog','Update Prog'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('Prog','Delete Prog'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('Prog','Manage Prog'), 'url'=>array('admin')),
);
?>

<h1><?php echo $model->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
