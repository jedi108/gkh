<?php
/* @var $this ProgController */
/* @var $model Prog */

$this->breadcrumbs=array(
	Yii::t('Prog','Progs')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('Prog','List Prog'), 'url'=>array('index')),
	array('label'=>Yii::t('Prog','Create Prog'), 'url'=>array('create')),
	array('label'=>Yii::t('Prog','View Prog'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('Prog','Manage Prog'), 'url'=>array('admin')),
);
?>

<h1>Обновить: <?php echo $model->name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>