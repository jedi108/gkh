<?php
/* @var $this ProgController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Progs',
);

$this->menu=array(
	array('label'=>Yii::t('Prog','Create Prog'), 'url'=>array('create')),
	array('label'=>Yii::t('Prog','Manage Prog'), 'url'=>array('admin')),
);
?>

<h1>Progs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
