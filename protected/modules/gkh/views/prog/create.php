<?php
/* @var $this ProgController */
/* @var $model Prog */

$this->breadcrumbs=array(
	Yii::t('Prog','Progs')=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>Yii::t('Prog','List Prog'), 'url'=>array('index')),
	array('label'=>Yii::t('Prog','Manage Prog'), 'url'=>array('admin')),
);
?>

<h1>Создать программу обучения</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>