<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jedi
 * Date: 18.03.13
 * Time: 21:48
 * To change this template use File | Settings | File Templates.
 */

class JuserModule extends CWebModule
{
    public function init()
    {
        $this->setImport(array(
            'juser.components.*',
        ));
    }
}