<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jedi
 * Date: 18.03.13
 * Time: 22:21
 * To change this template use File | Settings | File Templates.
 */
class JWebUser extends CWebUser
{
    private $_model = null;

    function getRole() {
        if($user = $this->getModel()){
            // в таблице User есть поле role
            return $user->role;
        }
    }

    private function getModel(){
        if (!$this->isGuest && $this->_model === null){
            $this->_model = User::model()->findByPk($this->id, array('select' => 'role'));
        }
        return $this->_model;
    }

    function getFirst_Name(){
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->first_name;
    }

    // This is a function that checks the field 'role'
    // in the User model to be equal to 1, that means it's admin
    // access it by Yii::app()->user->isAdmin()
    function getisAdmin(){
        if ($this->isGuest)
            return false;
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->role == 'sex' ;
    }
    
    function getisAdminSite(){
        if ($this->isGuest)
            return false;
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->role == 'admin' ;
    }
    
    protected function loadUser($id=null)
    {
        if($this->_model===null)
        {
            if($id!==null)
                $this->_model=User::model()->findByPk($id);
        }
        return $this->_model;
    }
}