<h1>Вход через социальные сети</h1>

<div class="contents">
    <?php  $this->widget('application.modules.juser.components.UloginWidget', array(
        'params'=>array(
            'redirect'=>'http://'.$_SERVER['HTTP_HOST'].'/juser/ulogin/login' //Адрес, на который ulogin будет редиректить браузер клиента. Он должен соответствовать контроллеру ulogin и действию login
        )
    )); ?>
</div>