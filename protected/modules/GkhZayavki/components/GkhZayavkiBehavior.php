<?php
class GkhZayavkiBehavior extends CActiveRecordBehavior
{
	public $parent_field_name;
	
	public function afterSave($event) 
	{
		if($this->owner->isNewRecord){
			$modelGkhZayavka = new GkhZayavka;
			$modelGkhZayavka->attributes[$this->$parent_field_name] = $this->owner->id;
			$modelGkhZayavka->save();
		} 
	}
}

//	public function behaviors() {
//		if(Yii::app()->hasModule('GkhZayavki')) {
//			return array(
//				'GkhZayavkiBehavior'=>array(
//					'class'=>'GkhZayavkiBehavior',
//					'parent_field_name'=>'id_org',
//				)
//			);
//		}
//	}
//
//public function attach($owner)
//{  
//    parent::attach($owner);
//    $this->getOwner()->getMetaData()->addRelation('getValues', array(CActiveRecord::BELONGS_TO, 'MyValues', 'valuesId));
//}