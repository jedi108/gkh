<?php

/**
 * This is the model class for table "{{gkh_zayavka}}".
 *
 * The followings are the available columns in table '{{gkh_zayavka}}':
 * @property integer $id
 * @property integer $id_org
 * @property integer $id_profile
 */
class GkhZayavka extends CActiveRecord
{
	private $attest = array(
			'1'=>'первоочередная',
			'2'=>'очередная',
			'3'=>'переаттестация',
		);
	
	public $form_podgotovki  = array(
		'1'=>'новая подготовка',
		'2'=>'переподготовка',
		'3'=>'повышение квалицикации',
		'4'=>'переаттестация'
	);
	
	public function getFormPodg()
	{
		return $this->form_podgotovki[$this->id_form_podgotovki];
	}

	public function getThisAttestation()
	{
		if(isset($this->attest[$this->id_attestation])) 
			return $this->attest[$this->id_attestation];
		else return null;
	}

	public function getAttestation()
	{
		return $this->attest;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GkhZayavka the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{gkh_zayavka}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_attestation, attestation, id_specializ, id_cat, id_org_adr, id_form_podgotovki', 'required'),
			array('id_org, id_profile, id_specializ, id_cat, id_org_adr, id_form_podgotovki, id_zayavka', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_org, id_profile', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idOrg'=>array(self::BELONGS_TO, 'Org','id_org'),
			'idProfile'=>array(self::BELONGS_TO, 'Profile','id_profile'),
			'idSpecializ' => array(self::BELONGS_TO, 'Specializ', 'id_specializ'),
			'idCat' => array(self::BELONGS_TO, 'Cat', 'id_cat'),
			'idOrgAddres' => array(self::BELONGS_TO, 'Org', 'id_org_adr'),
			'ZGroup' => array(self::BELONGS_TO, 'GkhZayavkaGroup', 'id_zayavka'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_profile' => 'Id Profile',
			'id_specializ' => 'Специализация',
			'id_org' => 'Организация',
			'id_cat' => 'Квалификация',
			'id_org_adr' => 'Адрес обучения',
			'id_form_podgotovki'=>'Форма обучения',
			'id_attestation'=>'Аттестация',
			'attestation'=>'Аттестуется в качестве',
			
			'formPodg'=>'Форма подготовки',
			'thisAttestation'=>'Аттестация',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_org',$this->id_org);
		$criteria->compare('id_profile',$this->id_profile);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}