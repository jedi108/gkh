<?php

/**
 * This is the model class for table "{{gkh_zayavka_group}}".
 *
 * The followings are the available columns in table '{{gkh_zayavka_group}}':
 * @property integer $id
 * @property string $num
 * @property integer $type
 * @property integer $id_user
 */
class GkhZayavkaGroup extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GkhZayavkaGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeValidate() {
		$this->num=date('ymd-hms');
		if(is_null(Yii::app()->user->id)) $this->id_user =0;
		else $this->id_user=Yii::app()->user->id;
		return parent::beforeValidate();
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{gkh_zayavka_group}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('num, id_user, type', 'required'),
			array('id_org, type, id_user', 'numerical', 'integerOnly'=>true),
			array('num', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, num, type, id_user', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'zayavka' => array(self::HAS_MANY, 'GkhZayavka', 'id_zayavka'),
			'org' => array(self::BELONGS_TO, 'Org', 'id_org'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'num' => 'Num',
			'type' => 'Type', //1-Юрлицо, 0-физ лицо
			'id_user' => 'Id User',
			'timestamp'=>'Время'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with=array('zayavka');
		$criteria->compare('id',$this->id);
		$criteria->compare('num',$this->num,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('timestamp',$this->timestamp);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=> array(
				'defaultOrder'=>'timestamp DESC'
			),
			'pagination'=> array(
                'pageSize'=>200,
			),
		));
	}
}

/**



-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 28 2012 г., 00:43
-- Версия сервера: 5.5.24
-- Версия PHP: 5.3.10-1ubuntu3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `yiigkh1`
--

-- --------------------------------------------------------

--
-- Структура таблицы `t_gkh_zayavka_group`
--

DROP TABLE IF EXISTS `t_gkh_zayavka_group`;
CREATE TABLE IF NOT EXISTS `t_gkh_zayavka_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_org` varchar(32) NOT NULL COMMENT 'Ссылка на организацию',
  `num` varchar(32) NOT NULL COMMENT 'Номер заявки',
  `type` tinyint(4) NOT NULL COMMENT 'Тип заявки',
  `id_user` int(32) NOT NULL COMMENT 'Пользователь заявки(если есть)',
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `num` (`num`),
  KEY `id_user` (`id_user`),
  KEY `id_org` (`id_org`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


CREATE TABLE IF NOT EXISTS `t_profile_org` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `id_profile` int(32) NOT NULL,
  `id_org` int(32) NOT NULL,
  `type_link` int(32) NOT NULL COMMENT 'тип связи',
  PRIMARY KEY (`id`),
  KEY `id_profile` (`id_profile`,`id_org`),
  KEY `type_link` (`type_link`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


CREATE TABLE IF NOT EXISTS `t_profile_org_typeLink` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name_link` varchar(64) NOT NULL COMMENT 'описание связки',
  PRIMARY KEY (`id`),
  KEY `name_link` (`name_link`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Тип связки профиля и организаций' AUTO_INCREMENT=1 ;

 */