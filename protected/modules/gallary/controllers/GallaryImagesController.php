<?php

class GallaryImagesController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    public function actions() {
        return array(
            'order' => array(
                'class' => 'ext.OrderColumn.OrderAction',
                'modelClass' => 'GallaryImages',
                'pkName' => 'id',
            ),
        );
    }

    public function actionImage($id) {
        //Yii::app()->clientScript->scriptMap['*.js'] = false;
        $model = GallaryImages::model()->findByPk($id);

        $this->renderPartial('_image', array('model' => $model));
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('Admin', 'View', 'Create', 'Update', 'Delete'),
                'expression' => 'Yii::app()->user->role=="sex"',
            ),
            array('deny',
                'actions' => array('Admin', 'View', 'Create', 'Update', 'Delete'),
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new GallaryImages;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['GallaryImages'])) {
            $model->attributes = $_POST['GallaryImages'];
            $uploadedFile = CUploadedFile::getInstance($model, 'image');
            if (!is_null($uploadedFile)) {
                $fileName = uuid::uuidgen() . '.' . $uploadedFile->extensionName;  // random number + file name
                $model->image = $fileName;
                if ($model->save()) {
                    $uploadedFile->saveAs(Yii::app()->basePath . '/../file/gallary/' . $fileName);
                    $this->redirect(array('view', 'id' => $model->id));
                }
            } else {
                $model->addError('image', 'Загрузите картинку');
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['GallaryImages'])) {
            $_POST['GallaryImages']['image'] = $model->image;
            $model->attributes = $_POST['GallaryImages'];

            $uploadedFile = CUploadedFile::getInstance($model, 'image');

            if ($model->save()) {
                if (!empty($uploadedFile))
                    $uploadedFile->saveAs(Yii::app()->basePath . '/../file/gallary/' . $model->image);
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('GallaryImages');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new GallaryImages('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['GallaryImages']))
            $model->attributes = $_GET['GallaryImages'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return GallaryImages the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = GallaryImages::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param GallaryImages $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'gallary-images-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
