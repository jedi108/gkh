<?php

/**
 * This is the model class for table "{{gallary_images}}".
 *
 * The followings are the available columns in table '{{gallary_images}}':
 * @property integer $id
 * @property integer $id_gallary
 * @property string $title
 * @property string $body
 * @property integer $order
 * @property string $image
 */
class GallaryImages extends CActiveRecord {

    public $order = 1;
    public function getAllGallarys() {
        $Gallary = Gallary::model()->findAll(array('order' => 'name'));
        return CHtml::listData($Gallary, 'id', 'name');
    }

    public function getImageH100() {
        $assetsDir = Yii::app()->basePath . '/../file/gallary/';
        return (!empty($this->image)) ? CHtml::image(Yii::app()->request->baseUrl . '/../file/gallary/' . $this->image, "image", array("height" => 100)) : "no image";
    }

    public function getImage() {
        $assetsDir = Yii::app()->basePath . '/../file/gallary/';
        return (!empty($this->image)) ? CHtml::image(Yii::app()->request->baseUrl . '/../file/gallary/' . $this->image, "image"
                                                        //, array("height" => 100)
                        ) 
                        : "no image";
    }    
    
    public function afterDelete() {
        parent::afterDelete();
        $image = Yii::app()->basePath . '/../file/gallary/' . $this->image;
        unlink($image);
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GallaryImages the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{gallary_images}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_gallary, title, order, image', 'required'),
            array('id_gallary, order', 'numerical', 'integerOnly' => true),
            array('title, image', 'length', 'max' => 255),
            array('body', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, id_gallary, title, body, order, image', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'Gallary' => array(self::BELONGS_TO, 'Gallary', 'id_gallary'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'id_gallary' => 'Галлерея',
            'title' => 'Заголовок',
            'body' => 'Текстовое описание',
            'order' => 'Сортировка',
            'image' => 'Изображение',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('id_gallary', $this->id_gallary);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('body', $this->body, true);
        $criteria->compare('order', $this->order);
        $criteria->compare('image', $this->image, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                //'route'=>'/journal/article/admin',
                'defaultOrder' => 't.order asc',
            ),
            'pagination' => array(
                'pageSize' => 100
            ),
        ));
    }

}