<div style="overflow: hidden;">

    <?php
    $gallaryImages = $model->GallaryImages;
    foreach ($gallaryImages as $id => $image) {


        $text = $image->title;
        $file = '/file/gallary/' . $image->image;
        $pic = Media::img($file, "lic150x200", "temp");
        //CVarDumper::dump($file,100,1);
        echo '<div class="span-5">';
        echo CHtml::ajaxLink($pic . $text, Yii::app()->createUrl('/gallary/gallaryImages/image', array('id' => $image->id)), array('type' => 'GET',
            'beforeSend' => 'loading',
            'update' => '#preview',
            'complete' => 'afterAjax',
                ), array('class' => 'lic', 'rel' => 'gallery'));
        echo '</div>';
    }
    ?>



    <?php
    $this->widget('ext.yiiext.widgets.fancybox.EFancyboxWidget', array(
        // Selector for generate fancybox.
        //'selector'=>'a[href$=\'.jpg\'],a[href$=\'.png\'],a[href$=\'.gif\']',
        // Enable "mouse-wheel" to navigate throught gallery items.
        // Defaults to false.
        // 'enableMouseWheel'=>false,
        // [fancybox options](http://fancybox.net/api/).
        'options' => array(
            'top' => 10
        // 'padding'=>10,
        // 'margin'=>20,
        // 'enableEscapeButton'=>true,
        // 'onComplete'=>'js:function() {$("#fancybox-wrap").hover(function() {$("#fancybox-title").show();}, function() {$("#fancybox-title").hide();});}',
        ),
    ));
// $this->widget('application.extensions.fancybox.EFancyBox', array(
//    //  'target'=>'a[rel=gallerys]',
//    //  'config'=>array(
//    //              'transitionIn'=>'elastic',
//    //              'transitionOut'=>'elastic',
//    //              'titlePosition'=>'outside',
// 			// 	'padding'=>'20px',
// 			// 	'margin'=>'20px',
// 			// 	'top'=>'0',
// 			// 	'enableEscapeButton'=>true,
// 			// ),
//     )
// );
    ?>

    <div style="display:none">
        <div id="preview">
            <p></p>
        </div>
    </div>

</div>
<?php
echo $model->body;
?>


<script type="text/javascript">
    function loading()
    {
        $.fancybox.showActivity();
    }
    function afterAjax()
    {
        $.fancybox.hideActivity();
        $.fancybox({
            href: '#preview',
            scrolling: 'false',
            // padding : '0',
            // margin : '0',
            top: '10',
            'onComplete': function() {
                $("#fancybox-wrap").css({'top': '10px', 'bottom': 'auto'});
            }
            // transitionIn : 'fade',
            // transitionOut : 'fade', 
            //check the fancybox api for additonal config to add here   
            //onClosed: function() { $('#preview').html(''); }, //empty the preview div
        });
    }
</script>