<?php
/* @var $this GallaryController */
/* @var $model Gallary */

$this->breadcrumbs = array(
    Yii::t('Gallary', 'Gallaries') => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => Yii::t('Gallary', 'List Gallary'), 'url' => array('index')),
    array('label' => Yii::t('Gallary', 'Create Gallary'), 'url' => array('create')),
    array('label' => Yii::t('GallaryImages', 'GallaryImages'), 'url' => array('/gallary/gallaryImages/admin')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#gallary-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление галлереями</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'gallary-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        //'body',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
