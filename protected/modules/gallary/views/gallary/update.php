<?php
/* @var $this GallaryController */
/* @var $model Gallary */

$this->breadcrumbs = array(
    Yii::t('Gallary', 'Gallaries') => array('index'),
    $model->name => array('view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => Yii::t('Gallary', 'List Gallary'), 'url' => array('index')),
    array('label' => Yii::t('Gallary', 'Create Gallary'), 'url' => array('create')),
    array('label' => Yii::t('Gallary', 'View Gallary'), 'url' => array('view', 'id' => $model->id)),
    array('label' => Yii::t('Gallary', 'Manage Gallary'), 'url' => array('admin')),
    array('label' => Yii::t('GallaryImages', 'GallaryImages'), 'url' => array('/gallary/gallaryImages/admin')),
);
?>

<h1>Update Gallary <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>