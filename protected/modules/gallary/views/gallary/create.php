<?php
/* @var $this GallaryController */
/* @var $model Gallary */

$this->breadcrumbs = array(
    Yii::t('Gallary', 'Gallaries') => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => Yii::t('Gallary', 'List Gallary'), 'url' => array('index')),
    array('label' => Yii::t('Gallary', 'Manage Gallary'), 'url' => array('admin')),
    array('label' => Yii::t('GallaryImages', 'GallaryImages'), 'url' => array('/gallary/gallaryImages/admin')),
);
?>

<h1>Создать галлерею</h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>