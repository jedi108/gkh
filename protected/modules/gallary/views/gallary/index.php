<?php
/* @var $this GallaryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Gallaries',
);

$this->menu = array(
    array('label' => Yii::t('Gallary', 'Create Gallary'), 'url' => array('create')),
    array('label' => Yii::t('Gallary', 'Manage Gallary'), 'url' => array('admin')),
    array('label' => Yii::t('GallaryImages', 'GallaryImages'), 'url' => array('/gallary/gallaryImages/admin')),
);
?>

<h1>Gallaries</h1>

<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
));
?>
