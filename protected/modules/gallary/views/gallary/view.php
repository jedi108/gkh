<?php
/* @var $this GallaryController */
/* @var $model Gallary */

$this->breadcrumbs = array(
    Yii::t('Gallary', 'Gallaries') => array('index'),
    $model->name,
);

$this->menu = array(
    array('label' => Yii::t('Gallary', 'List Gallary'), 'url' => array('index')),
    array('label' => Yii::t('Gallary', 'Create Gallary'), 'url' => array('create')),
    array('label' => Yii::t('Gallary', 'Update Gallary'), 'url' => array('update', 'id' => $model->id)),
    array('label' => Yii::t('Gallary', 'Delete Gallary'), 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => Yii::t('Gallary', 'Manage Gallary'), 'url' => array('admin')),
    array('label' => Yii::t('GallaryImages', 'GallaryImages'), 'url' => array('/gallary/gallaryImages/admin')),
);
?>

<h1>View Gallary #<?php echo $model->id; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'name',
    ),
));
?>

<?php
$this->renderPartial('_preview5', array('model' => $model));
?>