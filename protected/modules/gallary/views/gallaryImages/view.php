<?php
/* @var $this GallaryImagesController */
/* @var $model GallaryImages */

$this->breadcrumbs=array(
	Yii::t('GallaryImages','Gallary Images')=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>Yii::t('GallaryImages','List GallaryImages'), 'url'=>array('index')),
	array('label'=>Yii::t('GallaryImages','Create GallaryImages'), 'url'=>array('create')),
	array('label'=>Yii::t('GallaryImages','Update GallaryImages'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('GallaryImages','Delete GallaryImages'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('GallaryImages','Manage GallaryImages'), 'url'=>array('admin')),
        array('label'=>Yii::t('Gallary','Gallary'), 'url'=>array('/gallary/gallary/admin')),
);
?>

<h1>View GallaryImages #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_gallary',
		'title',
		'body',
		'order',
		'image',
	),
)); ?>
