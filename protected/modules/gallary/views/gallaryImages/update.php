<?php
/* @var $this GallaryImagesController */
/* @var $model GallaryImages */

$this->breadcrumbs=array(
	Yii::t('GallaryImages','Gallary Images')=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('GallaryImages','List GallaryImages'), 'url'=>array('index')),
	array('label'=>Yii::t('GallaryImages','Create GallaryImages'), 'url'=>array('create')),
	array('label'=>Yii::t('GallaryImages','View GallaryImages'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('GallaryImages','Manage GallaryImages'), 'url'=>array('admin')),
        array('label'=>Yii::t('Gallary','Gallary'), 'url'=>array('/gallary/gallary/admin')),
);
?>

<h1>Update GallaryImages <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>