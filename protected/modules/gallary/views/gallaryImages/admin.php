<?php
/* @var $this GallaryImagesController */
/* @var $model GallaryImages */

$this->breadcrumbs=array(
	Yii::t('GallaryImages','Gallary Images')=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>Yii::t('GallaryImages','List GallaryImages'), 'url'=>array('index')),
	array('label'=>Yii::t('GallaryImages','Create GallaryImages'), 'url'=>array('create')),
        array('label'=>Yii::t('Gallary','Gallary'), 'url'=>array('/gallary/gallary/admin')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#gallary-images-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление картинками галлерей</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gallary-images-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
                    'name'=> 'id_gallary',
                    'filter'=> GallaryImages::model()->getAllGallarys(),
                    'value'=>'$data->Gallary->name',
                    ),
		'title',
		'order',
                array(
                  'header' => 'Order',
                  'name' => 'order',
                  'class' => 'ext.OrderColumn.OrderColumn',
                ),
		array(
                    'name'=>'image',
                    'type'=>'html',
                    'value'=>'$data->getImageH100()',
                    'filter'=>false,
                    //'value'=>'$data->image',
                ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
