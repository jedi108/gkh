<?php
/* @var $this GallaryImagesController */
/* @var $model GallaryImages */

$this->breadcrumbs=array(
	Yii::t('GallaryImages','Gallary Images')=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>Yii::t('GallaryImages','List GallaryImages'), 'url'=>array('index')),
	array('label'=>Yii::t('GallaryImages','Manage GallaryImages'), 'url'=>array('admin')),
        array('label'=>Yii::t('Gallary','Gallary'), 'url'=>array('/gallary/gallary/admin')),
);
?>

<h1>Создать изображение</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>