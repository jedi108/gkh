<?php
/* @var $this GallaryImagesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Gallary Images',
);

$this->menu=array(
	array('label'=>Yii::t('GallaryImages','Create GallaryImages'), 'url'=>array('create')),
	array('label'=>Yii::t('GallaryImages','Manage GallaryImages'), 'url'=>array('admin')),
        array('label'=>Yii::t('Gallary','Gallary'), 'url'=>array('/gallary/gallary/admin')),
);
?>

<h1>Gallary Images</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
