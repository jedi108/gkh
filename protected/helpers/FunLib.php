<?php
/**
 * Библиотека функций
 * 
 * @author Jasur Mirkhamidov <mirkhamidov.jasur@gmail.com>
 * 
 * 
 */


class FunLib {
	
	static function isdebug () { return true; }
	
	/**
	 * Вывод ошибки
	 * 
	 * Вывод ошибки списком
	 * 
	 * @param $d mixed Принимает как строку так и массив (одномерный)
	 * 
	 * @return string HTML 
	 */
	static function showErr($d) 
	{
		if ( empty($d) ) return null;
		ob_start();
		if ( is_array($d) ) {
			?><ul><?
			for( $i=0,$max=sizeof($d); $i<$max; ++$i ) {
				?><li><?=$d[$i]?></li><?
			}
			?></ul><?
		} else {
			?><span><?=$d?></span><?
		}
		return ob_get_clean();
	}
	
	/**
	 * Вывод массива
	 * 
	 * @param mixed $a data to VarDamp
	 * @param int $deep Глубина вывода
	 * @param bool true-вывод типа pre и цветной; false-как var_dump;
	 * @todo Найти эекстеншн группирующий массив/объекты 
	 */
	static function prr($a,$deep=15,$vardump=false)
	{
		if ( !YII_DEBUG ) return false;
		if ( empty($a) ) return false;
		ob_start();
		/*?><pre><?
		if ( is_array($a) ) {
			print_r($a);
		} else {
			echo $a;
		}
		?></pre><?*/
		echo CVarDumper::dump($a, $deep, $vardump);
		return ob_get_clean();
	}
	
	/**
	 * Вывод массива attibutes Модели в цикле
	 * 
	 * 
	 */
	static function prrAttr($model)
	{
		if ( !YII_DEBUG ) return false;
		if ( empty($model) ) return false;
		ob_start();
		?><pre><?
		if ( isset($model->attributes) ) {
			echo self::prr($model->attributes);
		} else {
			$c = sizeof($model);
			if ( $c > 0 ) {
				$ex = false;
				#echo 'DDD';
				for( $i=0; $i<$c; ++$i ) {
					$s = &$model[$i];
					if ( isset( $s->attributes ) ) {
						echo self::prr($s->attributes);
						if ( !$ex ) $ex = true;
					}
					
				}
				
				if ( $ex !== true ) return false;
				
			} else {
				return false;
			}
		}
		
		
		?></pre><?
		return ob_get_clean();
	}
	
	/**
	 * Возврат результатов вида JSON
	 * 
	 * @param $res bool Результат выполнения скрипта
	 * @param $arr mixed данные, которые нужно вернуть, можно массив, можно строкой
	 * @param $clean bool Нужно ли очищать результат
	 * 
	 * @return array JSON
	 */
	static function jsonEcho($res=false,$arr=null,$clean=false)
	{
		if ( is_array($arr) ) {
			$d = array( 'result' => $res );
			// Если кодировка файлов cp, то можно включить
			#$arr = self::convert_arr_utf($arr);
			$d = array_merge($d,$arr);
		} else {
			// Если кодировка файлов cp, то можно заменить
			#$d = array( 'result' => $res , 'data' => self::DataIconvWU($arr));
			$d = array( 'result' => $res , 'data' => $arr);
		}
		$d = json_encode($d);
		$replace = array(
			'\n' => '',
			'\r' => '',
			'\t' => '',
			'\\' => '',
		);
		if ( $clean ) $d = strtr($d,$replace);
		echo  $d;
	}
	
	
	/**
	 * Переконвертировать в UTF массив
	 */
	static function convert_arr_utf($arr)
	{
		$ret = array();
		if ( is_array($arr) ) {
			foreach( $arr as $k => $v ) {
				if ( is_array($v) ) {
					$ret[$k] = self::convert_arr_utf($v);
				} else {
					$ret[$k] = self::DataIconvWU($v);
				}
			}
			return $ret;
		} else return self::DataIconvWU($arr);
	}
	
	static function DataIconvUW($value)
	{
		return iconv("UTF-8","windows-1251",$value);
	}
	 
	 
	 
	static function DataIconvWU($value)
	{
		return iconv("windows-1251","UTF-8",$value);
	}
	
	
	/**
	 * Очистка переменной
	 * 
	 * @param $data string 
	 * 
	 * @return string
	 */
	static function clean_form($data)
	{
		return trim( strip_tags( $data ) );
	}
	
	
	/**
	 * Очистка массива (рекурсивно)
	 * 
	 * Зависит от: clean_form
	 * 
	 * @param @arr array 
	 * 
	 * @return array
	 */
	static function cleanArray($arr)
	{
		if ( !is_array($arr) ) {
			return self::clean_form($arr);
		} else {
			foreach( $arr as $k => $v ) {
				if ( is_array($v) ) {
					$arr[$k] = self::cleanArray($v);
				} else {
					$arr[$k] = self::clean_form($v);
				}
			}
			return $arr;
		}
	}
	
	/**
	 * Простая запись в БД
	 * 
	 * @param string $t название таблицы без префикса
	 * @param array $set Чем надо запослнить
	 * 
	 * @return bool
	 */
	static function fillDbInsert($t,&$set,$debug=false)
	{
		$conn = Yii::app()->db;
		$s = array();
		foreach( $set as $r => $v ) {
			#$s[] = "`$r`='$v'";
			$s[] = "`$r`='".addslashes($v)."'";
		}
		#echo self::prr($s);exit;
		$sqlInsert = 'INSERT INTO `'.$t.'` SET '.implode(',',$s);
		if ( $debug ) {
			echo $sqlInsert; exit;
		}
		try {
			$insRes=$conn->createCommand($sqlInsert)->execute();
			return $insRes;
		} catch(CDbException $e) {
			 #echo 'Выброшено исключение: ',  $e, "\n";
			self::debug($e->getMessage());
			#echo self::prr($e->getMessage());
			#var_dump(mysql_error());
		}
		
	}
	
	
	static function debug($s=null)
	{
		if ( self::isdebug() && $s !== null && YII_DEBUG ) {
			?><div class="fDebugCarrier" style="border: 2px dashed #4D4D4D;background: #BFBFBF;padding: 4px; color:black;">
				<div class="fDebugName" onClick="$(this).next().toggle();" style="font-size: 10px;text-align: center;border-bottom: 1px dashed #777;margin-bottom: 5px;cursor:pointer;">::: FUN LIB :: D E B U G :::</div>
				<div class="fToggleMe" style="display:none;"><?=$s?></div>
			</div><?
		}
	}

	/**
	 * Простое получение данных из БД
	 * 
	 * @param string $q SQL строка
	 * 
	 * @return array
	 */
	static function fillDbSelect($q)
	{
		$conn = Yii::app()->db;
		$insRes=$conn->createCommand($q)->query();
		$ret = array();
		while(($row=$insRes->read())!==false) {
			#echo FunLib::prr($row);
			$ret[] = $row;
		}
		return $ret;
	}
	
	/**
	 * Является ли переменная HEX
	 * 
	 * @param string $v 
	 * @return 
	 * 		false - if not hex
	 * 		true - if HEX
	 * 		null - if empty or not string
	 */
	static function is_hex($v)
	{
		if ( 
			empty($v) 
			|| is_array($v)
		) {
			return null;
		} else {
			return (bool) preg_match('|^[a-f0-9]+$|',$v);
		}
	}
	
	/**
	 * Упаковать данные, и можно пересылать, хоть массив
	 * 
	 * @param mixed $data То что нужно упаковать
	 * @param bool $viaUrl передаются ли данные по URL
	 * 
	 * @return string Encoded string
	 */
	static function packData($data,$viaUrl = false)
	{
		if ( empty($data) ) return false;
		if ( is_array($data) ) $data = serialize($data);
		
		
		$PASS = 'justPackData';
		$ret = base64_encode( self::EnDeCode($data,$PASS) );
		if ( $viaUrl ) {
			$ret = urlencode($ret);
		} 
		return $ret;
	}
	
	static function unpackData($data)
	{
		if ( empty($data) ) return false;
		
		$PASS = 'justPackData';
		$data = self::EnDeCode( base64_decode( $data ) , $PASS );
		$isArray = @unserialize($data);
		if ($isArray === 'b:0;' || $isArray !== false) {
			unset($data);
			return $isArray;
		} else {
			unset($isArray);
			return $data;
		}
	}
	
	static function EnDeCode($String, $Password)
	{
		$Salt='BGuxLWQtKweKEMV4';
		$StrLen = strlen($String);
		$Seq = $Password;
		$Gamma = '';
		while (strlen($Gamma)<$StrLen)
		{
			$Seq = pack("H*",sha1($Gamma.$Seq.$Salt));
			$Gamma.=substr($Seq,0,8);
		}
	 
		return $String^$Gamma;
	}
	
	/**
	 * Генерация Уникального ID
	 * 
	 * Способ генерации типа uuid
	 */
	static function getId()
	{
		#FIXME пока что генерит по UUID
		return uuid::calc();
	}
	
	
	/**
	 * Вырисовка option`ов для списка
	 * 
	 * @param array $arr
	 * @param string $attr
	 * 
	 * @return string HTML (options list)
	 */
	static function makeOptionList(&$arr,$attr=null)
	{
		if ( !is_array($arr) || sizeof($arr) == 0 ) return false;
		$ret = null;
		foreach ( $arr as $k => $v ) {
			$ret .= '<option '.$attr.' value="'.$k.'">'.$v.'</option>';
		}
		return $ret;
	}
	
	/**
	 * Конвертация ошибок от модели по методоу getErrors, а нормальный массив
	 * 
	 * @param array $e my error array
	 * @param array $m model`s error
	 * 
	 */
	public static function addModelErr2myErr(&$e,&$m)
	{
		#echo FunLib::prr($m);
		if ( empty($m) ) return false;
		$c = 0;
		foreach( $m as $v ) {
			$vC = sizeof($v);
			if ( $vC > 0 ) {
				for( $i=0;$i<$vC; ++$i ) {
					$e[] = $v[$i];
				}
				++$c;
			} else continue;
		}
		
		if ( $c > 0 ) return true;
			else return false;
	}
	
	
	/**
	 * Конвертация данных из модели findAll
	 * 
	 * @param object $model 
	 * @param array config data
	 * 
	 * @return array
	 */
	public static function modelDataAsArray(&$model,$p)
	{
		#echo self::prrAttr($model);
		if ( empty($model) || empty($p) || !isset($p['key']) ) return false;
		$key = $p['key'];
		if ( sizeof($model) > 0 ) {
			$ret=array();
			foreach( $model as $v ) {
				#echo FunLib::prr($v->attributes);
				
				
				$ret[ $v->$key ] = $v->attributes;
			}
			return $ret;
			#echo FunLib::prr($ret);
		} else return false;
	}
	
	/**
	 * Удалить элементы $orig_arr из $exc_arr
	 * 
	 * @param array $orig_arr Исходный массив
	 * @param array $exc_arr Массив из которого нужно удалить данные
	 * 
	 * @return mixed false-some error; array - returned data;
	 */
	public static function array_excluded_second(&$orig_arr,$exc_arr)
	{
		if ( empty($orig_arr) || empty($exc_arr) ) return false;
		foreach( $exc_arr AS $k => $v ) {
			if ( isset( $orig_arr[$k] ) ) {
				unset( $exc_arr[$k] );
			}
		}
		
		return $exc_arr;
	}
	
	/**
	 * Проверить action, есть ли доступ у текущего пользователя
	 * 
	 * @access public
	 * @example ::checkAccess('/fights/seasonTour/viewtournament')
	 * 
	 * @param string полный путь до action
	 * 
	 * @return bool
	 */
	public static function checkAccess($controller)
	{
		if ( empty($controller) ) return false;
		// Инициализация контроллера
		$init = Yii::app()->createController($controller);
		if ( $init ) {
			// Получим $accRules и у контроллер
			$accRules = $init[0]->accessRules();
			
			// пройдемся по всем правилам, при первом же совпадении правила сразу даем ответ
			if ( $accRules && sizeof($accRules) > 0 ) {
				$_t = array_filter(explode('/',$controller));
				// получим педанный action
				$controllerAction = end($_t);
				
				// просмотрим каждое правило
				for( $i=0,$max=sizeof($accRules); $i<$max; ++$i ) {
					$accRule = (array)$accRules[$i];
					
					// allor OR deny
					$access = $accRule[0]; 
					unset($accRule[0]);
					
					if ( isset($accRule['actions']) ) {
						// если есть actions, значит правило по actions
						if ( in_array($controllerAction,$accRule['actions']) ) {
							unset($accRule['actions']);
							return self::checkAccess_allowDeny( self::checkAccess_chekRule($accRule), $access ) ;
						} else continue;
					} else {
						// значит правило на все actions
						return self::checkAccess_allowDeny( self::checkAccess_chekRule($accRule), $access );
					}
				}
				unset($_t);
				// если не совпало ни одно правило, мало ли, то не дадим доступа
				return false;
			} else {
				// нет правил доступа? не дадим доступ
				return false;
			}
			
		} else {
			// Если контроллер не инициировался
			return false;
		}
	}
	
	/**
	 * Сверка ответа и дозволенности action
	 * 
	 * @access private
	 * 
	 * @param bool $result ответ ф-ии или чего нибудь
	 * @param string $ad allow OR deny
	 * 
	 * @return bool
	 */
	private static function checkAccess_allowDeny($result=null,$ad=null)
	{
		if ( $result===true && $ad == 'allow' ) return true;
		return false;
	}
	
	/**
	 * Проверка по доступу пользователя
	 * Могут быть разные правила
	 * 
	 * @access private
	 * 
	 * @param array $rule массив выражений
	 * 
	 * @return bool
	 */
	private static function checkAccess_chekRule(&$rule)
	{
		if ( empty($rule) ) return false;
		foreach( $rule as $type => $v ) {
			switch( $type ) {
				case 'expression':
					//FIXME may be check if function ot smth else is exist?
					return (bool)eval('return '.$v.';');
				break;
				
				case 'users':
					for( $i=0,$max=sizeof($v); $i<$max; ++$i ) {
						switch ( $v[$i] ) {
							case '*': return true;
							case '@': return (Yii::app()->user->isGuest? false : true );
						}
					}
					return false;
				break;
			}
		}
		return false;
	}
}