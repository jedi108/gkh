<?php
/**
 * Расширенная версия
 * 
 * calc выдаешь бинарный вид UUID размером 16 байт
 */
class uuid {
	
	/**
	 * Сгенерировать UUID
	 * 
	 * @param boolean $conv Конвертировать в BIN? Default=true
	 * @return binary uuid
	 **/
	static function calc($conv=true)
	{
		if ( $conv === false ) return( pack('H*', self::uuidgen(false) ) );
			else return self::uuidgen(false);
	}
	
	/**
	 * Возвращает UUID из бинарного вида в HEX
	 * 
	 * @param $f binary UUID в бинарном виде
	 * 
	 * @return 
	 * 		null - if empty 
	 * 		string
	 */
	static function gethex($f)
	{
		if ( empty($f) ) return null;
		
		if ( self::is_hex($f) ) return $f;
			else return( bin2hex($f) );
	}
	
	/**
	 * Возвращает UUID из HEX в BINARY
	 * 
	 * @param $f hex UUID 
	 * 
	 * @return binary
	 */
	static function getbin($f)
	{
		if ( empty($f) ) return null;
		if ( self::is_hex($f) ) return(  pack('H*',$f) );
			else return $f;
		#FIXME Mirkhamidov думаю надо проверять $f на другие типы, ибо нельзя возвзращать если не BIN
	}
	
	/**
	 * Генерация UUID
	 * 
	 * @param $f bool форматировать вывод или нет
	 * 
	 * @return mixed hex-$f=false, string-$f=true
	 */
	static function uuidgen($f=true) 
	{
		
		if ( $f ) $f = '%x-%04x-%04s-%04x-%04x%04x%04x';
			else $f = '%04x%04x%04s%04x%04x%04x%04x';
		return sprintf( $f,
			time()
			,substr(mt_rand( 0, 0xffff ),0,2).substr(microtime(true),4,2)
			,substr( hash('crc32', microtime(true).mt_rand( 0, 0xffff ) ), 2,4)
			,mt_rand( 0, 0xffff )
			,mt_rand( 0, 0x0fff )
			,mt_rand( 0x0fff, 0x3fff )
			,mt_rand( 0x4000, 0xffff )
		);
	}
	
	/**
	 * Самый первый метод
	 */
	static function uuidgen2() {
		return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
			mt_rand( 0, 0xffff ),
			mt_rand( 0, 0x0fff ) | 0x4000,
			mt_rand( 0, 0x3fff ) | 0x8000,
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		);
	}
	
	
	/**
	 * Является ли переменная HEX
	 * 
	 * @param string $v 
	 * @return 
	 * 		false - if not hex
	 * 		true - if HEX
	 * 		null - if empty or not string
	 */
	static function is_hex($v)
	{
		if ( 
			empty($v) 
			|| is_array($v)
		) {
			return null;
		} else {
			return (bool) preg_match('|^[a-f0-9]+$|',$v);
		}
	}
	
	
	public function run()
	{
		
	}
}
