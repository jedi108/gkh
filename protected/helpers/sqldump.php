<?php

class sqldump
{
	static function create()
	{
		return $date('Ymd');
		Yii::import('ext.yii-database-dumper.SDatabaseDumper');
		$dumper = new SDatabaseDumper;
		 
		// Get path to new backup file
		$file = Yii::getPathOfAlias('webroot.protected.backups').'/dump.sql';
		 
		// Gzip dump
		if(function_exists('gzencode'))
		    file_put_contents($file.'.gz', gzencode($dumper->getDump()));
		else
		    file_put_contents($file, $dumper->getDump());	
	}
}