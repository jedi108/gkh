<?php
class CNivoSlider extends CWidget
{
	public $target;
	public $customCss;
	public $config=array();
	
    public function run()
    {
		$assets = dirname(__FILE__).'/assets';
		$baseUrl = Yii::app()->assetManager->publish($assets);
		Yii::app()->clientScript->registerCoreScript('jquery');
		Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.nivo.slider.pack.js', CClientScript::POS_HEAD);
		if ($this->customCss!='fancy') {
			Yii::app()->clientScript->registerCssFile($this->customCss);
		} else {
			Yii::app()->clientScript->registerCssFile($baseUrl . '/fancy-nivo-slider.css');
		}
		Yii::app()->clientScript->registerCssFile($baseUrl . '/nivo-slider.css');
		$config = CJavaScript::encode($this->config);
		Yii::app()->getClientScript()->registerScript(__CLASS__, "
			$('$this->target').nivoSlider($config);
		");
	} 
}