<?php
/**
 * EAjaxUpload class file.
 * This extension is a wrapper of http://valums.com/ajax-upload/
 *
 * @author Vladimir Papaev <kosenka@gmail.com>
 * @version 0.1
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

/**
        How to use:

        view:
		 $this->widget('ext.EAjaxUpload.EAjaxUpload',
                 array(
                       'id'=>'uploadFile',
                       'config'=>array(
                                       'action'=>'/controller/upload',
                                       'allowedExtensions'=>array("jpg"),//array("jpg","jpeg","gif","exe","mov" and etc...
                                       'sizeLimit'=>10*1024*1024,// maximum file size in bytes
                                       'minSizeLimit'=>10*1024*1024,// minimum file size in bytes
                                       //'onComplete'=>"js:function(id, fileName, responseJSON){ alert(fileName); }",
                                       //'messages'=>array(
                                       //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
                                       //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
                                       //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
                                       //                  'emptyError'=>"{file} is empty, please select files again without it.",
                                       //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
                                       //                 ),
                                       //'showMessage'=>"js:function(message){ alert(message); }"
                                      )
                      ));

        controller:

	public function actionUpload()
	{
	        Yii::import("ext.EAjaxUpload.qqFileUploader");
	        
                $folder='upload/';// folder for uploaded files
                $allowedExtensions = array("jpg"),//array("jpg","jpeg","gif","exe","mov" and etc...
                $sizeLimit = 10 * 1024 * 1024;// maximum file size in bytes
                $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
                $result = $uploader->handleUpload($folder);
                $result=htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                echo $result;// it's array
	}

*/
class EAjaxUpload extends CWidget
{
  public $id="fileUploader";
	public $postParams=array();
	
  public $config=array();
  public $configInit=array();

  public $params=null;
  public $preset=null;
  public $previewId = null;
  public $inputId = null;
  

	public $css=null;

  function __construct()
  {

  }

  public function run()
  {
    $this->configInit=array(
            //'action' => '/images/upload/ajaxupload?p='.FunLib::packData($data,1),
            'allowedExtensions' => array("jpg", "png", "jpeg", "gif", "doc", "docx", "rar", "zip", "7zip","pdf", "xlsx"),
            'sizeLimit' => 50*1024*1024,
            'minSizeLimit' => 1*100,
            'onComplete'=>"js:fAUx.onComplete",
            'messages'=>array(
              'typeError'=> Y::t('fights.fighters_view_formFighter_js_messageTypeError',array(),null,'{file} has invalid extension. Only {extensions} are allowed.'),
              'sizeError'=> Y::t('fights.fighters_view_formFighter_js_messageSizeError',array(),null,'{file} is too large, maximum file size is {sizeLimit}.'),
              'minSizeError'=> Y::t('fights.fighters_view_formFighter_js_messageMinSizeError',array(),null,'{file} is too small, minimum file size is {minSizeLimit}.'),
              'emptyError'=> Y::t('fights.fighters_view_formFighter_js_messageEmptyError',array(),null,'{file} is empty, please select files again without it.'),
              'onLeave'=> Y::t('fights.fighters_view_formFighter_js_messageOnLeave',array(),null,'The files are being uploaded, if you leave now the upload will be cancelled.'),
            ),
            'showMessage'=>"js:fAUx.showMessage"
    );

    $this->config = array_merge($this->configInit, $this->config);
    
    //ЕСЛИ не задан путь, то путь по умолчанию /file/pic/temp
    if(!isset($this->params['path']))
      $this->params=array('path' => PATH_TMP);

    if(!isset($this->params['previewId']))
      $this->params=array('previewId' => "#img-preview");

    if(!isset($this->params['inputId']))
      $this->params=array('inputId' => "#image_path");

    if(!isset($this->params['preset']))
      $this->params=array('preset' => "#image_path");

    // //ЕСЛИ не задан путь, то путь по умолчанию /file/pic/temp
    // if (!isset($this->previewId))
    //   $this->previewId = "#img-preview";

    // if (!isset($this->inputId))
    //   $this->inputId = "#image_path";

    // if (!isset($this->preset))
    //   $this->inputId = "100x100";

    //ЕСЛИ экшен не задан, задаем свой экшен и добаляем к нему $this->params
    if (!isset($this->config['action'])) $this->config['action'] = '/images/upload/ajaxupload?p='.FunLib::packData($this->params,1);
      if (strspn($this->config['action'], '/images/upload/')!=16) $this->config['action'] = '/images/upload/ajaxupload?p='.FunLib::packData($this->params,1);


		if(empty($this->config['action']))
		{
		      throw new CException('EAjaxUpload: param "action" cannot be empty.');
                }

		if(empty($this->config['allowedExtensions']))
    {
		      throw new CException('EAjaxUpload: param "allowedExtensions" cannot be empty.');
    }

		if(empty($this->config['sizeLimit']))
		{
		      throw new CException('EAjaxUpload: param "sizeLimit" cannot be empty.');
    }

    unset($this->config['element']);

    echo '<div id="'.$this->id.'"><noscript><p>Please enable JavaScript to use file uploader.</p></noscript></div>';
    $assets = dirname(__FILE__).'/assets';
    $baseUrl = Yii::app()->assetManager->publish($assets);

    Yii::app()->clientScript->registerScriptFile($baseUrl . '/fileuploader.js', CClientScript::POS_HEAD);

    Yii::app()->clientScript->registerScriptFile($baseUrl . '/interactive_upload.js', CClientScript::POS_HEAD);

    $this->css=(!empty($this->css))?$this->css:$baseUrl.'/fileuploader.css';
    Yii::app()->clientScript->registerCssFile($this->css);

		$postParams = array('PHPSESSID'=>session_id(),'YII_CSRF_TOKEN'=>Yii::app()->request->csrfToken);
		if(isset($this->postParams))
		{
				$postParams = array_merge($postParams, $this->postParams);
		}

		$config = array(
                                'element'=>'js:document.getElementById("'.$this->id.'")',
                                'debug'=>true,
                                'multiple'=>false
                               );
		$config = array_merge($config, $this->config);
		$config['params']=$postParams;
		$config = CJavaScript::encode($config);
                Yii::app()->getClientScript()->registerScript("FileUploader_".$this->id, "var FileUploader_".$this->id." = new qq.FileUploader($config); ",CClientScript::POS_LOAD);
	}


}