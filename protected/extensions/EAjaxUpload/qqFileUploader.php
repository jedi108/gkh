<?php
/**
 * Handle file uploads via XMLHttpRequest
 */
class qqUploadedFileXhr {
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {
        $input = fopen("php://input", "r");
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);

        if ($realSize != $this->getSize()){
            return false;
        }

        $target = fopen($path, "w");
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);

        return true;
    }
    function getName() {
        return $_GET['qqfile'];
    }
    function getSize() {
        if (isset($_SERVER["CONTENT_LENGTH"])){
            return (int)$_SERVER["CONTENT_LENGTH"];
        } else {
            throw new Exception('Getting content length is not supported.');
        }
    }
}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class qqUploadedFileForm {
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {
        if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){
            return false;
        }
        return true;
    }
    function getName() {
        return $_FILES['qqfile']['name'];
    }
    function getSize() {
        return $_FILES['qqfile']['size'];
    }
}

class qqFileUploader {
    private $allowedExtensions = array();
    private $sizeLimit = 10485760;
    private $file;
    private $savepath;
    private $previewId;
    private $inputId;
    private $preset;

    function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760, $savepath=null, $previewId=null, $inputId=null, $preset=null)
    {
        $allowedExtensions = array_map("strtolower", $allowedExtensions);
        $this->savepath = $savepath;
        $this->previewId = $previewId;
        $this->inputId = $inputId;
        $this->inputId = $inputId;
        $this->preset = $preset;
        // echo $this->savepath;
        // die();

        $this->allowedExtensions = $allowedExtensions;
        $this->sizeLimit = $sizeLimit;

        $this->checkServerSettings();

        if (isset($_GET['qqfile'])) {
            $this->file = new qqUploadedFileXhr();
        } elseif (isset($_FILES['qqfile'])) {
            $this->file = new qqUploadedFileForm();
        } else {
            $this->file = false;
        }
    }

    private function checkServerSettings(){
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));

        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';
            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");
        }
    }

    private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;
        }
        return $val;
    }

    /**
     * Returns array('success'=>true) or array('error'=>'error message')
     */
    function handleUpload($uploadDirectory, $replaceOldFile = FALSE){
        if (!is_writable($uploadDirectory)){
            return array('error' => "Server error. Upload directory isn't writable.");
        }

        if (!$this->file){
            return array('error' => 'No files were uploaded.');
        }

        $size = $this->file->getSize();

        if ($size == 0) {
            return array('error' => 'File is empty');
        }

        if ($size > $this->sizeLimit) {
            return array('error' => 'File is too large');
        }

        $pathinfo = pathinfo($this->file->getName());


        $filename = $pathinfo['filename'];
        //$filename = md5(uniqid());
        $ext = $pathinfo['extension'];

        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
        }

        if(!$replaceOldFile){
            /// don't overwrite previous files that were uploaded
            while (file_exists($uploadDirectory . $filename . '.' . $ext)) {
                $filename .= rand(10, 99);
            }
        }

        //echo 'uploadDirectory' . $uploadDirectory;

        //Берем из сессии параметры для презета PREVIEW
        //if (isset(Yii::app()->session['converts'])) $conv = FunLib::unpackData(Yii::app()->session['converts']);

        //Генерация uuid в названии файла
        $uuid = uuid::calc();
        //Конфертированное имя файла для PREVIEW
        $filename = $uuid.'_'.$this->preset;
        //Оригинальное имя файла
        $filename_orig = $uuid;

 

        //СОХРАНЯЕМ ФАЙЛ
        if ($this->file->save($uploadDirectory . $filename_orig . '.' . $ext)){
                //ЕСЛИ файл сохранен
                $fileOriginal = $this->savepath.$filename_orig . '.' . $ext;
                //Генерируем PREVIEW
                //$previewFile = Media::SavePreset($fileOriginal, $this->preset, 'temp');
                // var_dump($fileOriginal, $this->preset);
                // die('ssssss');
                
                if(in_array(strtolower($ext), array('jpg','png','jpeg','bmp','gif')))
                    $previewFile = current(Media::createImageByPreset($fileOriginal, array($this->preset)));
                else 
                    $previewFile = '';

                #FIXME почему то убрали
                //unset(Yii::app()->session['converts']);
                //Возвращаем в json информацию о загруженном только что файле
                $fileOriginal = $this->savepath.$filename_orig . '%s.' . $ext;
                return array(   'success'=>true,
                    'filename'=>$previewFile, 
                    'fileorig'=>$fileOriginal, 
                    'filepath'=>$this->savepath, 
                    'file'=>$filename_orig. '.' . $ext,
                    'previewId'=>$this->previewId,
                    'inputId'=>$this->inputId,
                    'preset'=>$this->preset
                );
        } else {
            return array('error'=> 'Could not save uploaded file.' .
                'The upload was cancelled, or server error encountered');
        }

    }
}
