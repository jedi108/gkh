var fAUxConf =  { 
	path : "/file/pic/temp/",
	id_inputbox : "#YumProfile_image_path",
	id_img_src : "#img-thumb"
}


var fAUx = function() {
	var error = true;
	var errorId=0; 
	var successId=0;
	var infoId=0;
	 
	
	function zzz()
	{
		console.log('path');
	}

	function onComplete(id,fileName, responseJSON)
	{
		//console.log('onComplete');
		//console.log(responseJSON);
		//console.log(responseJSON.previewId);

		if ( responseJSON.success ) {
			if ( successId > 0 ) {
				notifier.closeById(successId);
				successId=0;
			}
			successId = notifier.addSuccess(responseJSON.successMessage,1,5000);
			console.log(responseJSON);
			$(responseJSON.inputId).val( responseJSON.fileorig );
			//$(fAUxConf.id_img_src).attr( 'src', fAUxConf.path+responseJSON.filename );
			$(responseJSON.previewId).attr( 'src', responseJSON.filename );
		}
	}
	
	function showMessage(message)
	{
		console.log('showMessage');
		console.log(message);
		if ( errorId > 0 ) {
			notifier.closeById(errorId);
			errorId = 0;
		}
		errorId = notifier.addError(message,1,5000);
	}
	
	return {
		onComplete:onComplete
		,showMessage:showMessage
	}
}();

//http://doom-tv.local/rates/rates/test




/**** START NOTIFIER ****/

var notifier = function() {
	/**
	* Выброс уведомлений
	* 
	* Зависимость: jQuery
	* 
	* Автор: Jasur Mirkhamidov <mirkhamidov.jasur@gmail.com>
	*/
	var selfName = 'notifier';		// собственное имя, для простоты управления и адаптации
	var debugInfoCarrierName = selfName+'DebugInfo';					//	общий блок для всех уведомлений
	var debugInfoInfoBlockName = selfName+'DebugInfoInfoBlock';			//	Информационный блок, серенький
	var debugInfoErrorBlockName = selfName+'DebugInfoErrorBlock';		//	Блок ошибок, красный
	var debugInfoSuccessBlockName = selfName+'DebugInfoSuccessBlock';	//	Блок удачных сообщений
	var debugInfoCloseBtnName = selfName+'DebugCloseBtn';				//	блок кнопки закрытия
	var debugInfoCarrier;			//	основной блок в init сделаем объектом
	var defTimeOut = 1500;			//	дефолтный автовыключение блока
	var defBlockCloseTimeOut = 400;	//	дефолтный таймаут для скрытия блока при его удалении
	
	/**
	 * Инициализация блоков
	 */
	function debugInfoInit()
	{
		
		if( $('#'+debugInfoCarrierName).length == 0 ) {
			// Если основного блока нет, то добавим
			$('body').prepend('<div id="'+debugInfoCarrierName+'"></div>');
			debugInfoCarrier = $('#'+debugInfoCarrierName);
			
			debugInfoCarrier.css({
					//'border':'1px solid blue'
					'z-index':'10000000'
					,'display': 'block'
					,'position': 'fixed'
					,'top': '0'
					,'right': '0'
					,'bottom': '0'
					,'width': '100%'
					,'height': '0'
					,'line-height': '28px'
				});
		}
		//addInfo('Some');
		//addError('Some');
		//addSuccess('Some');
		//notifier.addInfo('<center><img src="'+glSnakeImg+'" alr="Loading..." /></center>');
		//notifier.addInfo('<center><img src="'+glSnakeImg+'" alr="Loading..." /></center>');
	}
	
	/**
	 * Общий метод добавления блока
	 * 
	 * @param string 	txt 	Текст для вывода
	 * @param bool 		out		Авто выход? 
	 * @param int 		time	Если out=true, то время в секундах
	 * @param string	classs	Класс блока, разные для Info,Success,Error
	 * @param json		styles	Стили для блока
	 * @param object	notifierConf	Конфигурация для активности попапчика
	 * 			active 		влючение и выключение таймаута (0-выключен; 1-включен)
	 * 			time		если включен, то время выключения (сек)
	 */
	function addBlock(txt,out,time,classs,styles,notifierConf)
	{
		if ( txt ) {
			var id = getRandomInt(999,999999);
			$(debugInfoCarrier).append('<div class="'+classs+'" id="'+id+'" ><div class="'+debugInfoCloseBtnName+'" rel="'+id+'" onClick="'+selfName+'.closeMe(this)">x</div>'+txt+'</div>');
			
			$('#'+id).css(styles);
			$('div.'+debugInfoCloseBtnName).css({
					'border':'2px solid #4D4D4D'
					,'padding':'1px 0px 2px 2px'
					,'background':'#7E7E7E'
					,'cursor':'pointer'
					,'line-height': '8px'
					,'color':'black'
					,'width':'10px'
					,'height':'10px'
					,'float':'right'
					,'font-weight':'bold'
					//,'text-align':'center'
				});
			
			
			if ( 
				notifierConf  
				&& ( 
					( notifierConf.active == 1 || notifierConf.active == 0 )
					|| ( notifierConf.active == true || notifierConf.active == false )
					|| ( notifierConf.active == 'true' || notifierConf.active == 'false' )
				)
			) {
				if ( notifierConf.active ) {
					if ( ! notifierConf.time ) time = defTimeOut;
						else time = notifierConf.time;
					setTimeout(function(){ $('div.'+classs+'[id="'+id+'"]').hide().remove() },time);
				}
				
			} else if ( out ) {
				if ( !time ) time = defTimeOut;
				setTimeout(function(){ $('div.'+classs+'[id="'+id+'"]').hide().remove() },time);
			}
			return id;
		} else return false;
	}
	
	/**
	 * Добавление информационного блока
	 * 
	 * @param string 	txt 	Текст для вывода
	 * @param bool 		out		Авто выход? 
	 * @param int 		time	Если out=true, то время в секундах
	 */
	function addInfo(txt,out,time,notifierConf)
	{
		if ( !txt ) return false;
		var css = {
			'border':'2px solid #1A1A1A'
			,'display': 'block'
			,'width': '600px'
			,'margin':'0 auto'
			,'padding':'3px'
			,'margin-top':'3px'
			,'background-color':'#BFBFBF'
			,'color':'black'
		}
		return addBlock(txt,out,time,debugInfoInfoBlockName,css,notifierConf);
	}
	
	/**
	 * Добавление блока об удачном выполнении события
	 * 
	 * @param string 	txt 	Текст для вывода
	 * @param bool 		out		Авто выход? 
	 * @param int 		time	Если out=true, то время в секундах
	 */
	function addSuccess(txt,out,time,notifierConf)
	{
		if ( !txt ) return false;
		var css = {
				'border':'2px solid green'
				,'display': 'block'
				,'width': '600px'
				,'margin':'0 auto'
				,'padding':'3px'
				,'margin-top':'3px'
				,'background-color':'#90EE90'
				,'color':'black'
			}
		return addBlock(txt,out,time,debugInfoSuccessBlockName,css,notifierConf);
	}
	
	/**
	 * Добавление блока ошибок
	 * 
	 * @param string 	txt 	Текст для вывода
	 * @param bool 		out		Авто выход? 
	 * @param int 		time	Если out=true, то время в секундах
	 */
	function addError(txt,out,time,notifierConf)
	{
		if ( !txt ) return false;
		var css = {
				'border':'2px solid red'
				,'display': 'block'
				,'width': '600px'
				,'margin':'0 auto'
				,'padding':'3px'
				,'margin-top':'3px'
				,'background-color':'#FFC0CB'
				,'color':'black'
			}
		return addBlock(txt,out,time,debugInfoErrorBlockName,css,notifierConf);
	}
	
	/**
	 * Закрыть блок
	 */
	function closeMe(callObj)
	{
		$(callObj).parent().fadeOut(defBlockCloseTimeOut,function(){ $(callObj).parent().remove() });
	}
	
	function closeById(id)
	{
		if( id ) {
			if ( $('#'+id).length > 0 ) {
				//$('#'+id).remove();
				$('#'+id).fadeOut(defBlockCloseTimeOut,function(){ $('#'+id).remove() });
			} else return false;
		} else return false;
	}
	
	
	function getRandomInt(min, max)
	{
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	
	function d(d){console.log(d)}
	
	function debug()
	{
		if ( !YII_DEBUG ) return false;
		if ( arguments.length == 0 ) return false;
		console.log('START========================================='+arguments.callee.caller.name+'=========================================START');
		for( var i in arguments ) {
			console.log(arguments[i]);
		}
		console.log('END========================================='+arguments.callee.caller.name+'=========================================END');
		
		return true;
	}
	
	
	
	return{
		debugInfoInit:debugInfoInit
		,addInfo:addInfo
		,addSuccess:addSuccess
		,addError:addError
		,closeMe:closeMe
		,closeById:closeById
		,debug:debug
	}
}();

/**** END  NOTIFIER ****/