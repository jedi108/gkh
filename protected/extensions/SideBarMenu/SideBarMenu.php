<?php
/**
 * XDropDownMenu class file.
 *
 * CDropDownMenu is an extension to CMenu that supports Drop-Down Menus using the
 * superfish jquery-plugin.
 *
 * @author Herbert Maschke <thyseus@gmail.com>
 * @link http://www.yiiframework.com/
 */
Yii::import('zii.widgets.CMenu');
class SideBarMenu extends CMenu
{
	public $cssFile = 'sidebarmenu.css';

	public function init()
	{
		parent::init();
	}

	/**
	 * Calls {@link renderMenu} to render the menu.
	 */
	public function run()
	{
		$this->renderDropDownMenu($this->items);
	}

	protected function renderDropDownMenu($items)
	{
		echo '<div class="sidebarmenu">';
		$this->htmlOptions = array_merge($this->htmlOptions, array('id' => 'sidebarmenu1'));
		$this->renderMenu($items);

		$this->registerClientScript();
		echo '</div>';
		echo '<br style="clear:both;" />';
	}

	protected function registerClientScript() {
		$assets = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'vendors';
		$baseUrl = Yii::app()->getAssetManager()->publish($assets);

		//Yii::app()->clientScript->registerCoreScript('jquery');
		Yii::app()->clientScript->registerCssFile($baseUrl . '/' . $this->cssFile);
		Yii::app()->clientScript->registerScriptFile($baseUrl . '/' . 'sidebarmenu.js',CClientScript::POS_HEAD);
	}

}
