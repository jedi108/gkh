<?php
$this->breadcrumbs=array(
	'Заявки'=>array('/zayavka/start'),
	'Создать заявку',
);

//Yii::app()->clientscript->scriptMap['jquery.js'] = false;
?>

<h1>Форма заявки для частного лица</h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'enableAjaxValidation'=>true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => false,
))); ?>

<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
?>

	<p class="note">Поля отмеченные <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'lastname'); ?>
		<?php echo $form->textField($model,'lastname',array('size'=>30,'maxlength'=>155)); ?>
		<?php echo $form->error($model,'lastname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'firstname'); ?>
		<?php echo $form->textField($model,'firstname',array('size'=>30,'maxlength'=>155)); ?>
		<?php echo $form->error($model,'firstname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'middlename'); ?>
		<?php echo $form->textField($model,'middlename',array('size'=>30,'maxlength'=>155)); ?>
		<?php echo $form->error($model,'middlename'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'year_birthday'); ?>
		<?php echo $form->textField($model,'year_birthday',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'year_birthday'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'addr'); ?>
		<?php echo $form->textField($model,'addr',array('size'=>30,'maxlength'=>155)); ?>
		<?php echo $form->error($model,'addr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>30,'maxlength'=>104)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
	<?php if (isset($org)) {
		// echo CHtml::activelabelEx($org, 'name_small');
		// echo CHtml::activeTextField($org, 'name_small' , array('size'=>60,'maxlength'=>255));
		// echo $form->error($org, 'name_small');

		echo $form->labelEx($org,'name_small');
		echo $form->textField($org,'name_small',array('size'=>60,'maxlength'=>255));
		echo $form->error($org,'name_small');
	}
	?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_specializ'); ?>
		<?php //echo $form->textField($model,'id_specializ'); 
				$data = CHtml::listData(Specializ::model()->findAll(), 'id', 'name');
			echo $form->dropDownList($model, 'id_specializ', $data, array('prompt'=>'Выберите специальность','style'=>'width: 600px'));
		?>
		<?php echo $form->error($model,'id_specializ'); ?>
	</div>
	<div class="span-6">
		<?php echo $form->labelEx($model,"id_cat"); ?>
		<?php echo $form->dropDownList($model, "id_cat", 
			CHtml::listData(Cat::model()->findAll(),'id','name_cat'), 
			array('style'=>'width: 180px', 
				  'prompt'=>'Выберите категорию:',));?>
		<?php echo $form->error($model,"id_cat"); ?>
	</div>

	<div class="span-4">
		<?php echo $form->labelEx($model,"id_org_adr"); ?>
		<?php echo $form->dropDownList($model, "id_org_adr", 
			Org::getFillials(), 
			array('style'=>'width: 240px', 
				  'prompt'=>'Выберите желаемый адрес:',));?>
		<?php echo $form->error($model,"id_org_adr"); ?>
	</div>	

	<div class="clear"></div>
	<br/>
	<h5>ОБРАЗОВАНИЕ:</h5>
	<div class="clear"></div>
	<div class="span-4">
		<?php echo $form->labelEx($educat,"institution"); ?>
		<?php echo $form->textField($educat,"institution",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo $form->error($educat,"institution"); ?>
	</div>

	<div class="span-4">
		<?php echo $form->labelEx($educat,"specialty"); ?>
		<?php echo $form->textField($educat,"specialty",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo $form->error($educat,"specialty"); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($educat,"qualification_text"); ?>
		<?php echo $form->textField($educat,"qualification_text",array("size"=>20,"maxlength"=>128)); ?>
		<?php echo $form->error($educat,"qualification_text"); ?>
	</div>	
	<div class="span-4">
		<?php echo $form->labelEx($educat,"end_year"); ?>
		<?php echo $form->textField($educat,"end_year",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo $form->error($educat,"end_year"); ?>
	</div>	
	<div class="clear"></div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Отправить' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->