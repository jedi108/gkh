<div class="form">

<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' ;
		CVarDumper::dump ($message); 
		echo "</div>\n";
    }
?>	
	
	
<p class="note">Поля <span class="required">*</span> обязательны для заполнения.</p>

<?php
$this->breadcrumbs=array(
	'Заявки'=>array('/zayavka/start'),
	'Создать заявку',
);



Yii::app()->clientScript->registerScriptFile('/js/orgProfile.js', CClientScript::POS_END);

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'zayavka',
	'enableAjaxValidation'=>true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => false,
		'enableClientValidation'=>false,
//		'afterValidate'=>'js:function(form, data, hasError) {
//            if(jQuery.isEmptyObject(data)) {
//                alert("ok")
//            }
//            return false;
//        }'
))); 

Yii::app()->session['form'] = $form;

echo $form->errorSummary($modelOrg); 
//echo $form->errorSummary($modelProfile); 
//echo $form->errorSummary($educat);  
//echo $form->errorSummary($gkhZ); 
//echo $form->errorSummary($gkhZG);  
echo $form->hiddenField($gkhZG, 'id_user');
echo $form->error($gkhZG,"id_user");
echo $form->hiddenField($gkhZG, 'num');
echo $form->error($gkhZG,"num");
echo $form->hiddenField($gkhZG, 'type');
echo $form->error($gkhZG,"type");

switch ($this->typeOfZayavka) {
	case 'org':
		?><h1>Форма заявки для организации</h1><?php
		$this->renderPartial('z_org', array( 'form'=>$form, 
			'modelProfile'=>$modelProfile, 
			'modelProfileRuk'=>$modelProfileRuk, 
			'modelProfileBuh'=>$modelProfileBuh,
			'modelProfileZay'=>$modelProfileZay,
			'educat'=>$educat, 
			'modelOrg'=>$modelOrg, 
			'gkhZ'=>$gkhZ));
		break;

	case 'profile':
		?><h1>Форма заявки для частного лица</h1><?php
		$this->renderPartial('z_profile', array('i'=>0, 'form'=>$form, 
			'modelProfile'=>$modelProfile, 
			'modelProfileRuk'=>$modelProfileRuk,
			'modelProfileBuh'=>$modelProfileBuh,
			'modelProfileZay'=>$modelProfileZay,
			'educat'=>$educat, 
			'modelOrg'=>$modelOrg, 
			'gkhZ'=>$gkhZ));
		break;
}

?>

<!--		
<div class="row">
<input type='button' value='Сохранить данные' onclick='sexFields();'>
</div>
-->

 
	<div class="buttons">
		<?php echo CHtml::submitButton($modelOrg->isNewRecord ? 'Отправить данные' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->