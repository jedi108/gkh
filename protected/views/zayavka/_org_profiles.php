 
<?php //for ($i=0; $i<5; $i++) { ?>
	
	<div id="pro_<?=$i?>" class="view last">
	<h6>Физ-лицо №<?=$i+1?></h6>
	<?php if ($i>0) {  
		echo "<input type='button' value='Удалить' onclick='$( \"#pro_".$i."\").remove()'>";
	} ?>
	<br/>
	<div class="span-4">
		<?php echo $form->labelEx($model,"[$i]lastname"); ?>
		<?php echo $form->textField($model,"[$i]lastname",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo $form->error($model,"[$i]lastname"); ?>
	</div>

	<div class="span-4">
		<?php echo $form->labelEx($model,"[$i]firstname"); ?>
		<?php echo $form->textField($model,"[$i]firstname",array("size"=>20,"maxlength"=>255), $enableAjaxValidation=false); ?>
		<?php echo $form->error($model,"[$i]firstname"); ?>
	</div>

	<div class="span-4">
		<?php echo $form->labelEx($model,"[$i]middlename"); ?>
		<?php echo $form->textField($model,"[$i]middlename",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo $form->error($model,"[$i]middlename"); ?>
	</div>
	<div class="clear"></div>
 
	<div class="span-7">
		<?php echo $form->labelEx($model,"[$i]id_specializ"); ?>
		<?php //echo $form->textField($model,'id_specializ'); 
				$data = CHtml::listData(Specializ::model()->findAll(), 'id', 'name');
			echo $form->dropDownList($model, "[$i]id_specializ", $data, array('prompt'=>'Выберите специальность','style'=>'width: 270px'));
		?>
		<?php echo $form->error($model,"[$i]id_specializ"); ?>
	</div>
	<div class="span-5">
		<?php echo $form->labelEx($model,"[$i]id_cat"); ?>
		<?php echo $form->dropDownList($model, "[$i]id_cat", 
			CHtml::listData(Cat::model()->findAll(),'id','name_cat'), 
			array('style'=>'width: 180px', 
				  'prompt'=>'Выберите категорию:',));?>
		<?php echo $form->error($model,"[$i]id_cat"); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($model,"[$i]id_org_adr"); ?>
		<?php echo $form->dropDownList($model, "[$i]id_org_adr", 
			Org::getFillials(), 
			array('style'=>'width: 240px', 
				  'prompt'=>'Выберите желаемый адрес:',));?>
		<?php echo $form->error($model,"[$i]id_org_adr"); ?>
	</div>		
 
	<div class="span-2 lsat buttonadd">

	</div>
	<div class="clear"></div>
	ОБРАЗОВАНИЕ
	<div class="clear"></div>
	<div class="span-4">
		<?php echo $form->labelEx($educat,"[$i]institution"); ?>
		<?php echo $form->textField($educat,"[$i]institution",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo $form->error($educat,"[$i]institution"); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($educat,"[$i]qualification_text"); ?>
		<?php echo $form->textField($educat,"[$i]qualification_text",array("size"=>20,"maxlength"=>128)); ?>
		<?php echo $form->error($educat,"[$i]qualification_text"); ?>
	</div>	
	<div class="span-4">
		<?php echo $form->labelEx($educat,"[$i]specialty"); ?>
		<?php echo $form->textField($educat,"[$i]specialty",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo $form->error($educat,"[$i]specialty"); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($educat,"[$i]end_year"); ?>
		<?php echo $form->textField($educat,"[$i]end_year",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo $form->error($educat,"[$i]end_year"); ?>
	</div>	
	<div class="clear"></div>
	</div>
<?php //} ?>