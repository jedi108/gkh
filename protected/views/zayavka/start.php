<?php
$this->breadcrumbs=array(
	'Заявки'//=>array('/zayavka/start'),
	//'Создать заявку',
);

$role = Yii::app()->user->role;
if($role=='admin' || $role=='sex') {
	echo CHtml::button('Управление заявками',array('submit'=>'/zayavka/admin'));
	echo '<br><hr><br>';
}
?>

<h1>Заявки</h1>
<h2>
<?php echo CHtml::link('Форма заявки для частных лиц', array ('/zayavka/addProfile')); ?>
</h2>
<h2>
<?php echo CHtml::link('Форма заявки для организации', array ('zayavka/addOrg')); ?>
</h2>