<?php

$dataProvider=new CActiveDataProvider('Education', array(
    'criteria'=>array(
        'condition'=>'id_profile='.$id,
        //'order'=>'create_time DESC',
       // 'with'=>array('author'),
    ),
    'pagination'=>array(
        'pageSize'=>20,
    ),
));

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'edu1',
	'summaryText' => '',
	'enableSorting'=>false,
	'dataProvider'=>$dataProvider,
	//'filter'=>$dataProvider,
	'columns'=>array(
		'educate',
		'n_diplom',
		'end_year',
	)
	//),
));

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'edu2',
	'summaryText' => '',
	'enableSorting'=>false,
	'dataProvider'=>$dataProvider,
	//'filter'=>$dataProvider,
	'columns'=>array(
		
		'institution',
		'specialty',
		//'qualification',
		'qualification_text'
	)
	//),
));