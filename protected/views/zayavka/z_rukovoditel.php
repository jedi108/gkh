 
	<div class="span-4">
		<?php echo $form->labelEx($model,"[$i]lastname"); ?>
		<?php echo $form->textField($model,"[$i]lastname",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo $form->error($model,"[$i]lastname"); ?>
	</div>

	<div class="span-4">
		<?php echo $form->labelEx($model,"[$i]firstname"); ?>
		<?php echo $form->textField($model,"[$i]firstname",array("size"=>20,"maxlength"=>255), $enableAjaxValidation=false); ?>
		<?php echo $form->error($model,"[$i]firstname"); ?>
	</div>

	<div class="span-4">
		<?php echo $form->labelEx($model,"[$i]middlename"); ?>
		<?php echo $form->textField($model,"[$i]middlename",array("size"=>20,"maxlength"=>20)); ?>
		<?php echo $form->error($model,"[$i]middlename"); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($model,"[$i]doljnost"); ?>
		<?php echo $form->textField($model,"[$i]doljnost",array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,"[$i]doljnost"); ?>
	</div>	
	<div class="span-4">
		<?php echo $form->labelEx($model,"[$i]phone"); ?>
		<?php echo $form->textField($model,"[$i]phone",array('size'=>20,'maxlength'=>24)); ?>
		<?php echo $form->error($model,"[$i]phone"); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($model,"[$i]email"); ?>
		<?php echo $form->textField($model,"[$i]email",array('size'=>30,'maxlength'=>104)); ?>
		<?php echo $form->error($model,"[$i]email"); ?>
	</div>