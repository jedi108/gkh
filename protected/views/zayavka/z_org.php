


<div class="view viewgroup">
	 <div class="view ">
		<?php echo $form->labelEx($modelOrg,'name_small', array('class'=>'span-7')); ?>
		<?php echo $form->textField($modelOrg,'name_small',array('size'=>45,'maxlength'=>255)); ?>
		<?php echo $form->error($modelOrg,'name_small'); ?>
		<div class="clear"></div>
		<?php echo $form->labelEx($modelOrg,'name_full', array('class'=>'span-7')); ?>
		<?php echo $form->textField($modelOrg,'name_full',array('size'=>45,'maxlength'=>255)); ?>
		<?php echo $form->error($modelOrg,'name_full'); ?>
		<div class="clear"></div>
		<?php echo $form->labelEx($modelOrg,'adr_ur', array('class'=>'span-7')); ?>
		<?php echo $form->textField($modelOrg,'adr_ur',array('size'=>45,'maxlength'=>255)); ?>
		<?php echo $form->error($modelOrg,'adr_ur'); ?>
		<div class="clear"></div>
		<?php echo $form->labelEx($modelOrg,'adr_fact', array('class'=>'span-7')); ?>
		<?php echo $form->textField($modelOrg,'adr_fact',array('size'=>45,'maxlength'=>255)); ?>
		<?php echo $form->error($modelOrg,'adr_fact'); ?>
 
		<div class="span-10">

		<?php echo $form->labelEx($modelOrg,'email', array('class'=>'span-4')); ?>
		<?php echo $form->textField($modelOrg,'email',array('size'=>20,'maxlength'=>128)); ?>
		<?php echo $form->error($modelOrg,'email'); ?>
	
		<div class="clear"></div>
		
		<?php echo $form->labelEx($modelOrg,'www', array('class'=>'span-4')); ?>
		<?php echo $form->textField($modelOrg,'www',array('size'=>20,'maxlength'=>128)); ?>
		<?php echo $form->error($modelOrg,'www'); ?>	
	
		<div class="clear"></div>
 
		<?php echo $form->labelEx($modelOrg,'phone', array('class'=>'span-4')); ?>
		<?php echo $form->textField($modelOrg,'phone',array('size'=>15,'maxlength'=>128)); ?>
		<?php echo $form->error($modelOrg,'phone'); ?>
 
		<div class="clear"></div>

		<?php echo $form->labelEx($modelOrg,'fax', array('class'=>'span-4')); ?>
		<?php echo $form->textField($modelOrg,'fax',array('size'=>15,'maxlength'=>128)); ?>
		<?php echo $form->error($modelOrg,'fax'); ?>

		</div>

		<div class="span-9 last">

		<?php echo $form->labelEx($modelOrg,'inn', array('class'=>'span-4')); ?>
		<?php echo $form->textField($modelOrg,'inn',array('size'=>12,'maxlength'=>12)); ?>
		<?php echo $form->error($modelOrg,'inn'); ?>
 
		<div class="clear"></div>

		<?php echo $form->labelEx($modelOrg,'kpp', array('class'=>'span-4')); ?>
		<?php echo $form->textField($modelOrg,'kpp',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($modelOrg,'kpp'); ?>
		<div class="clear"></div>

		<?php echo $form->labelEx($modelOrg,'okpo', array('class'=>'span-4')); ?>
		<?php echo $form->textField($modelOrg,'okpo',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($modelOrg,'okpo'); ?>
		<div class="clear"></div>

		<?php echo $form->labelEx($modelOrg,'okved', array('class'=>'span-4')); ?>
		<?php echo $form->textField($modelOrg,'okved',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($modelOrg,'okved'); ?>		
		</div>
		<div class="clear"></div>
		</div>
<div class="clear"></div>		
<div class="span10 view">


		<?php echo $form->labelEx($modelOrg,'bnk_name', array('class'=>'span-4')); ?>
		<?php echo $form->textField($modelOrg,'bnk_name',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($modelOrg,'bnk_name'); ?>
		<div class="clear"></div>
		<?php echo $form->labelEx($modelOrg,'bnk_korr', array('class'=>'span-4')); ?>
		<?php echo $form->textField($modelOrg,'bnk_korr',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($modelOrg,'bnk_korr'); ?>
		<div class="clear"></div>
		<?php echo $form->labelEx($modelOrg,'bnk_rchet', array('class'=>'span-4')); ?>
		<?php echo $form->textField($modelOrg,'bnk_rchet',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($modelOrg,'bnk_rchet'); ?>
		<div class="clear"></div> 
 		<?php echo $form->labelEx($modelOrg,'bik', array('class'=>'span-4')); ?>
		<?php echo $form->textField($modelOrg,'bik',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($modelOrg,'bik'); ?>
		<div class="clear"></div> 

<div class="clear"></div>
</div>
	
<div class="clear"></div>

<div class="view">
	<h5><strong>Генеральный директор</strong></h5>
	<div>
		<?php $this->renderPartial('z_rukovoditel', array('form'=>$form, 'model'=>$modelProfileRuk, 'i'=>'ruk')); ?>
	</div>
	<div class="clear"></div>
</div>

<div class="clear"></div>
<div class="view">
	<h5><strong>Главный бухгалтер</strong></h5>
	 
		<?php $this->renderPartial('z_rukovoditel', array('form'=>$form, 'model'=>$modelProfileRuk, 'i'=>'buh')); ?>
	 <div class="clear"></div>
</div>

<div class="clear"></div>
<div class="view">
	<h5><strong>Ответственное лицо (исполнитель заявки)</strong></h5>
	 
		<?php $this->renderPartial('z_rukovoditel', array('form'=>$form, 'model'=>$modelProfileZay, 'i'=>'zay')); ?>
	 <div class="clear"></div>
</div>

<div class="clear"></div>
<br/>


</div>
<div><strong>Заполните список сотрудников, направляемых для обучения (переаттестации)</strong></div>
<div class="row fiz">
<?php $this->renderPartial('z_profile', array(
		'modelProfile'=>$modelProfile ,
		'educat'=>$educat,
		'form'=>$form,
		'gkhZ'=>$gkhZ,
		'i'=>0)); ?>
</div>


<div class="row">
<input type='button' value='+ Добавить физ.лицо' onclick='AddFields();'>
</div>

<script type="text/javascript">

    // Эта функция добавляет валидацию для новых полей в форме. Например: добавление с помощью ajax
     // Вам нужно написать: $.fn.yiiactiveform.addFields($(form), $(fields)), и ajax-валидация начнет работать
     // Эта функция исключает повторы
     //
    if ($.fn.yiiactiveform)
    {
        $.fn.yiiactiveform.addFields = function(form, fields)
        {
            var $s = form.data('settings');
            if ($s != undefined)
            {
                fields.each(function()
                {
                    var $field = $(this), has = false;
                    $.each($s.attributes, function(i, o)
                    {
                        if (o.id == $field.attr('id'))
                        {
                            has = true;
                            return false;
                        }
                    });

                    if (!has)
                    {
                        $s.attributes[$s.attributes.length] = $.extend({
                            validationDelay:$s.validationDelay,
                            validateOnChange:$s.validateOnChange,
                            validateOnType:$s.validateOnType,
                            hideErrorMessage:$s.hideErrorMessage,
                            inputContainer:$s.inputContainer,
                            errorCssClass:$s.errorCssClass,
                            successCssClass:$s.successCssClass,
                            beforeValidateAttribute:$s.beforeValidateAttribute,
                            afterValidateAttribute:$s.afterValidateAttribute,
                            validatingCssClass:$s.validatingCssClass
                        }, {
                            id:$field.attr('id'),
                            inputID:$field.attr('id'),
                            errorID:$field.attr('id') + '_em_',
                            model:$field.attr('name').split('[')[0],
                            name:$field.attr('name'),
                            enableAjaxValidation:true,
                            status:1,
                            value:$field.val()
                        });
                        form.data('settings', $s);
                    }
                });
            }
        }
    }
 
 </script>
