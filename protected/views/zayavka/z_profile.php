<?php
//		Yii::app()->clientScript->scriptMap[ 'jquery.js' ] = false;
//		Yii::app()->clientScript->scriptMap[ 'jquery.min.js' ] = false;
?>

<?php //for ($i=0; $i<5; $i++) { ?>
	
	<div id="pro_<?=$i?>" class="view last viewgroup">
		
	<?php if($this->typeOfZayavka=='org') { ?>
	<h6>Физ-лицо №<?=$i+1?></h6>
	<?php if ($i>0) {  
		echo "<input type='button' value='Удалить' onclick='$( \"#pro_".$i."\").remove()'>";
	} ?>
	<?php } ?>
	<br/>
	<div class="span-4">
		<?php echo $form->labelEx($modelProfile,"[$i]lastname"); ?>
		<?php echo $form->textField($modelProfile,"[$i]lastname",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo $form->error($modelProfile,"[$i]lastname"); ?>
	</div>

	<div class="span-4">
		<?php echo $form->labelEx($modelProfile,"[$i]firstname"); ?>
		<?php echo $form->textField($modelProfile,"[$i]firstname",array("size"=>20,"maxlength"=>255), $enableAjaxValidation=false); ?>
		<?php echo $form->error($modelProfile,"[$i]firstname"); ?>
	</div>

	<div class="span-4">
		<?php echo $form->labelEx($modelProfile,"[$i]middlename"); ?>
		<?php echo $form->textField($modelProfile,"[$i]middlename",array("size"=>20,"maxlength"=>20)); ?>
		<?php echo $form->error($modelProfile,"[$i]middlename"); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($modelProfile,"[$i]year_birthday"); ?>
		<?php echo $form->textField($modelProfile,"[$i]year_birthday",array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($modelProfile,"[$i]year_birthday"); ?>
	</div>	

	<div class="clear"></div>
	<div class="span-4">
		<?php echo $form->labelEx($modelProfile,"[$i]doljnost"); ?>
		<?php echo $form->textField($modelProfile,"[$i]doljnost",array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($modelProfile,"[$i]doljnost"); ?>
	</div>	
	<div class="span-4">
		<?php echo $form->labelEx($modelProfile,"[$i]phone"); ?>
		<?php echo $form->textField($modelProfile,"[$i]phone",array('size'=>20,'maxlength'=>24)); ?>
		<?php echo $form->error($modelProfile,"[$i]phone"); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($modelProfile,"[$i]email"); ?>
		<?php echo $form->textField($modelProfile,"[$i]email",array('size'=>30,'maxlength'=>104)); ?>
		<?php echo $form->error($modelProfile,"[$i]email"); ?>
	</div>
	<?php 
	//Если профайл, то показать поле организации
	if ($this->typeOfZayavka=='profile') { ?>
		<div class="clear"></div>
		<?php echo $form->labelEx($modelOrg,'name_small', array('class'=>'span-6')); ?>
		<?php echo $form->textField($modelOrg,'name_small',array('size'=>21,'maxlength'=>255)); ?>
		<?php echo $form->error($modelOrg,'name_small'); ?>		
	<?php }
	?>
	<div class="clear"></div>

	<div class="span-2 lsat buttonadd">

	</div>
	<div class="clear"></div>
	
	<h5><strong>Образование</strong></h5>
	<div class="clear"></div>
	<div class="span-4">
		<?php echo $form->labelEx($educat,"[$i]id_educate"); ?>
		<?php echo $form->dropDownList($educat, "[$i]id_educate", 
					$educat->getEducationByArr(), 
					array('style'=>'width: 150px', 
						  'prompt'=>'Образование:',));?>
		<?php echo $form->error($educat,"[$i]id_educate"); ?>
	</div>	

	<div class="span-4">
		<?php echo $form->labelEx($educat,"[$i]n_diplom"); ?>
		<?php echo $form->textField($educat,"[$i]n_diplom",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo $form->error($educat,"[$i]n_diplom"); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($educat,"[$i]institution"); ?>
		<?php echo $form->textField($educat,"[$i]institution",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo $form->error($educat,"[$i]institution"); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($educat,"[$i]qualification_text"); ?>
		<?php echo $form->textField($educat,"[$i]qualification_text",array("size"=>20,"maxlength"=>128)); ?>
		<?php echo $form->error($educat,"[$i]qualification_text"); ?>
	</div>	
	<div class="span-4">
		<?php echo $form->labelEx($educat,"[$i]specialty"); ?>
		<?php echo $form->textField($educat,"[$i]specialty",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo $form->error($educat,"[$i]specialty"); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($educat,"[$i]end_year"); ?>
		<?php echo $form->textField($educat,"[$i]end_year",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo $form->error($educat,"[$i]end_year"); ?>
	</div>	
	<div class="clear"></div>

    <div class="view ">
	<h5><strong>Обучение в УКК ЖКХ</strong></h5>
	<div class="clear"></div>

	<div class="span-7">
		<?php echo $form->labelEx($gkhZ,"[$i]id_specializ"); ?>
		<?php $data = CHtml::listData(Specializ::model()->findAll(), 'id', 'name');
				echo $form->dropDownList($gkhZ, "[$i]id_specializ", $data, array('prompt'=>'Выберите специальность','style'=>'width: 270px'));
		?>
		<?php echo $form->error($gkhZ,"[$i]id_specializ"); ?>
	</div>

	<div class="span-5">
		<?php echo $form->labelEx($gkhZ,"[$i]id_form_podgotovki"); ?>
		<?php echo $form->dropDownList($gkhZ, "[$i]id_form_podgotovki", 
					$gkhZ->form_podgotovki, 
					array('style'=>'width: 180px', 
						  'prompt'=>'Выберите форму:',));?>
		<?php echo $form->error($gkhZ,"[$i]id_form_podgotovki"); ?>
	</div>

	<div class="span-5">
		<?php echo $form->labelEx($gkhZ,"[$i]id_cat"); ?>
		<?php echo $form->dropDownList($gkhZ, "[$i]id_cat", 
			CHtml::listData(Cat::model()->findAll(),'id','name_cat'), 
			array('style'=>'width: 180px', 
				  'prompt'=>'Выберите категорию:',));?>
		<?php echo $form->error($gkhZ,"[$i]id_cat"); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($gkhZ,"[$i]id_org_adr"); ?>
		<?php echo $form->dropDownList($gkhZ, "[$i]id_org_adr", 
			Org::getFillials(), 
			array('style'=>'width: 240px', 
				  'prompt'=>'Выберите желаемый адрес:',));?>
		<?php echo $form->error($gkhZ,"[$i]id_org_adr"); ?>
	</div>
	
	<div class="clear"></div>
	</div>

    <div class="view ">
	<h5><strong>Аттестация</strong></h5>
	<div class="clear"></div>
	<div class="span-5">
		<?php echo $form->labelEx($gkhZ,"[$i]id_attestation"); ?>
		<?php echo $form->dropDownList($gkhZ, "[$i]id_attestation", 
					$gkhZ->getAttestation(), 
					array('style'=>'width: 180px', 
						  'prompt'=>'Выберите:',));?>
		<?php echo $form->error($gkhZ,"[$i]id_attestation"); ?>
	</div>	
	<div class="span-6">
		<?php echo $form->labelEx($gkhZ,"[$i]attestation"); ?>
		<?php echo $form->textField($gkhZ,"[$i]attestation",array("size"=>22,"maxlength"=>64)); ?>
		<?php echo $form->error($gkhZ,"[$i]attestation"); ?>
	</div>	

	
	<div class="clear"></div>
	</div>	
	
	<div class="clear"></div>
</div>