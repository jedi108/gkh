<?php
$this->pageTitle = 'Заявка № ' . $modGkhz->num . ' от ' . $modGkhz->timestamp;
//if($print)
//{
//$cs = Yii::app()->clientScript;
//Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl."/css/print.css", 'screen, projection');
//}
?>

<script type="text/javascript"> 
	function printit(){ 
		window.print() ; 
	} 
</script> 
<input type="button" class="printButton" VALUE="распечатать страницу" onClick="printit()"> 


<style>
	.header{	
		color: white;
		background: url("/css/bg.gif") repeat-x scroll left top white;
		text-align: center;
		font-size: 121%;
		margin-top: 17px;
		padding: 2px;	
	}
</style>

<center><h1>Заявка №: <?= $modGkhz->num ?></h1> </center>
<center><h2> от <?= $modGkhz->timestamp ?></h2></center>

<div><h5><center> Заявитель: 
			<?php
//echo $modGkhz->type . '<br>';

			if ($modGkhz->type == 1) {
//ОРГАНИЗАЦИЯ	
				$model = $modGkhz->org;
				echo $model->name_small;
			} else {
//ФИЗ ЛИЦО
				$modelProfile = $modGkhz->zayavka;
				foreach ($modelProfile as $key => $model) {
					$f[] = $model->idProfile->lastname;
					$f[] = $model->idProfile->firstname;
					$f[] = $model->idProfile->middlename;
				}
				echo implode(' ', $f);
			}
			?>
		</center></h5></div>
<p><center>Согласно договора №________ от _______________20_____г</center></p>
<strong>Просим вас обучить и аттестовать</strong>





<?php
$modelProfile = $modGkhz->zayavka;
foreach ($modelProfile as $key => $model) {
	?>

	<div class="view">

		<?php
		//Заявка
		$zay = GkhZayavka::model()->find('id_profile=:id_profile', array(':id_profile' => $model->idProfile->id));
		?><div class="header"><center><strong>Заявка</strong></center></div><?php
	echoM('name', $zay->idSpecializ);
	echoM('name_cat', $zay->idCat);
	echoM('adr_fact', $zay->idOrgAddres, true, 'Желаемый адрес обучения');
	echoM('formPodg', $zay);
	echoM('thisAttestation', $zay);
	echoM('attestation', $zay);
		?><div class="header"><center><strong>Сведения о слушателе</strong></center></div><?php
	echoM('lastname', $model->idProfile, false);
	echoM('firstname', $model->idProfile, false);
	echoM('middlename', $model->idProfile);
	echoM('year_birthday', $model->idProfile);
	echoM('addr', $model->idProfile);
	echoM('email', $model->idProfile, false);
	echoM('phone', $model->idProfile);
	echoM('doljnost', $model->idProfile);

	//Образование
		?><div class="header"><center><strong>Образование</strong></center></div><?php
	$edu = $model->idProfile->education[0];

	echoM('institution', $edu);
	echoM('n_diplom', $edu);
	//echo '1';
	echoM('educate', $edu);
	//echo '1';
	//echoM( 'qualification', $edu);
	echoM('specialty', $edu);
	echoM('end_year', $edu);
	echoM('qualification_text', $edu);
	echoM('descEducate', $edu);



	//echo FunLib::prrAttr( $model->idProfile->education );
	//$this->renderPartial('Zeducation',array('id'=>$model->idProfile->id));
		?>
	</div>
	<?php
}
if ($modGkhz->type == 1) {
	//Берем директора
	?><div class="header"><center><strong>Директор</strong></center></div><?php
	$director = ProfileOrg::model()->find('id_org=:id_org AND type_link=:type_link', array(':id_org' => $modOrg->id, ':type_link' => 0));
	echoM('lastname', $director->idProfile, false);
	echoM('firstname', $director->idProfile, false);
	echoM('middlename', $director->idProfile);
	echoM('year_birthday', $director->idProfile);
	echoM('addr', $director->idProfile);
	echoM('email', $director->idProfile, false);
	echoM('phone', $director->idProfile);
	echoM('doljnost', $director->idProfile);

	//Берем Бухгалтера
	?><div class="header"><center><strong>Бухгалтер</strong></center></div><?php
	$director = ProfileOrg::model()->find('id_org=:id_org AND type_link=:type_link', array(':id_org' => $modOrg->id, ':type_link' => 2));
	echoM('lastname', $director->idProfile, false);
	echoM('firstname', $director->idProfile, false);
	echoM('middlename', $director->idProfile);
	echoM('year_birthday', $director->idProfile);
	echoM('addr', $director->idProfile);
	echoM('email', $director->idProfile, false);
	echoM('phone', $director->idProfile);
	echoM('doljnost', $director->idProfile);

	//Берем Ответственное лицо
	?><div class="header"><center><strong>Ответственное лицо (исполнитель заявки)</strong></center></div><?php
	$director = ProfileOrg::model()->find('id_org=:id_org AND type_link=:type_link', array(':id_org' => $modOrg->id, ':type_link' => 3));
	echoM('lastname', $director->idProfile, false);
	echoM('firstname', $director->idProfile, false);
	echoM('middlename', $director->idProfile);
	echoM('year_birthday', $director->idProfile);
	echoM('addr', $director->idProfile);
	echoM('email', $director->idProfile, false);
	echoM('phone', $director->idProfile);
	echoM('doljnost', $director->idProfile);
}
?>
<div  class="header"><center><strong>Сведения об организации</strong></center></div>
<div class="spa view">
	<?php
//Показываем организацию
	$model = $modGkhz->org;
	echoM('name_small', $model);
	echoM('name_full', $model);
	echoM('inn', $model);
	echoM('kpp', $model);
	echoM('adr_fact', $model);
	echoM('adr_ur', $model);
	echoM('phone', $model);
	echoM('fax', $model);
	echoM('email', $model);
	echoM('www', $model);
	?>
</div>

<?php
if ($modGkhz->type == 1) {
	?><div class="last span-8 view"><?php
	echoM('okpo', $model);
	echoM('okved', $model);
	echoM('bnk_name', $model);
	echoM('bik', $model);
	echoM('bnk_korr', $model);
	echoM('bnk_rchet', $model);
	?></div><?php
}
?>

<div class="clear"></div>



<?php

/**
 * Показываем атрибут и значение поля модельки
 * @param type $val - название поля
 * @param type $m - модель
 */
function echoM($val, $m, $isBr = true, $title = null) {
	if (($m->getAttribute($val) !== '')) {
		$theTitle = (is_null($title)) ? $m->getAttributeLabel($val) : $title;
		echo '<b>' . $theTitle . '</b>: <span>' . $m->$val . '</span>';

		if ($isBr)
			echo '<br>';
		else
			echo '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp';
	}
}

function cgrid($model, $arrFields) {

	$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'org-grid',
		'dataProvider' => $model->search(),
		//'filter'=>$model,
		'columns' => $arrFields
			//),
	));
	//@see http://www.yiiplayground.cubedwater.com/index.php?r=UiModule/dataview/gridViewArray
	//or activeDp
}
?>				