
<div class="row span-24 fiz">
	
	<div class="span-4">
		<?php echo CHtml::activeLabel($model,"[$i]lastname"); ?>
		<?php echo CHtml::activeTextField($model,"[$i]lastname",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo CHtml::error($model,"[$i]lastname"); ?>
	</div>

	<div class="span-4">
		<?php echo CHtml::activeLabel($model,"[$i]firstname"); ?>
		<?php echo CHtml::activeTextField($model,"[$i]firstname",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo CHtml::error($model,"[$i]firstname"); ?>
	</div>

	<div class="span-4">
		<?php echo CHtml::activeLabel($model,"[$i]middlename"); ?>
		<?php echo CHtml::activeTextField($model,"[$i]middlename",array("size"=>20,"maxlength"=>255)); ?>
		<?php echo CHtml::error($model,"[$i]middlename"); ?>
	</div>
	<div class="span-9">
		<?php echo CHtml::activeLabel($model,'[$i]id_specializ'); ?>
		<?php //echo $form->textField($model,'id_specializ'); 
				$data = CHtml::listData(Specializ::model()->findAll(), 'id', 'name');
			echo CHtml::activeDropDownList($model, '[$i]id_specializ', $data, array('style'=>'width: 340px'));
		?>
		<?php echo CHtml::error($model,'[$i]id_specializ'); ?>
	</div>
	<div class="span-2 lsat">
	<input type='button' value='Удалить' onclick=''>
	</div>
</div>
<div class="clear"></div>