<?php
$this->breadcrumbs=array(
	'Заявки'=>array('/zayavka/start'),
	'Создать заявку',
);

?>

<h1>Форма заявки для частного лица</h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'my',
	'enableAjaxValidation'=>true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => false,
))); 



echo $form->errorSummary($model); 

?>

	 
	<div class="row span-20 fiz">
 
	<?php $this->renderPartial('__test2', array('model'=>$model , 'form'=>$form, 'i'=>0)); ?>
	</div>
	 

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Отправить' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<div class="row">
<input type='button' value='+ Добавить физ.лицо' onclick='AddFields();'>
</div>

<script>
var mlen = function() {
	return $('.buttonadd').size();
  //return $('.fiz input[type="button"]').size();
}

var AddFields = function() {
	
	var url = '/zayavka/addOrgTest/?i=' + mlen();

	//console.log(mlen);
	$.ajax({
		type: 'POST',
		url: url,
		//data: 'i=' + len,
		success: function(html) {
			$('.fiz').append(html);
		}
	});
}





     // Эта функция добавляет валидацию для новых полей в форме. Например: добавление с помощью ajax
     // Вам нужно написать: $.fn.yiiactiveform.addFields($(form), $(fields)), и ajax-валидация начнет работать
     // Эта функция исключает повторы
     //
    if ($.fn.yiiactiveform)
    {
        $.fn.yiiactiveform.addFields = function(form, fields)
        {
            var $s = form.data('settings');
            if ($s != undefined)
            {
                fields.each(function()
                {
                    var $field = $(this), has = false;
                    $.each($s.attributes, function(i, o)
                    {
                        if (o.id == $field.attr('id'))
                        {
                            has = true;
                            return false;
                        }
                    });

                    if (!has)
                    {
                        $s.attributes[$s.attributes.length] = $.extend({
                            validationDelay:$s.validationDelay,
                            validateOnChange:$s.validateOnChange,
                            validateOnType:$s.validateOnType,
                            hideErrorMessage:$s.hideErrorMessage,
                            inputContainer:$s.inputContainer,
                            errorCssClass:$s.errorCssClass,
                            successCssClass:$s.successCssClass,
                            beforeValidateAttribute:$s.beforeValidateAttribute,
                            afterValidateAttribute:$s.afterValidateAttribute,
                            validatingCssClass:$s.validatingCssClass
                        }, {
                            id:$field.attr('id'),
                            inputID:$field.attr('id'),
                            errorID:$field.attr('id') + '_em_',
                            model:$field.attr('name').split('[')[0],
                            name:$field.attr('name'),
                            enableAjaxValidation:true,
                            status:1,
                            value:$field.val()
                        });
                        form.data('settings', $s);
                    }
                });
            }
        }
    }
 
 
</script>
