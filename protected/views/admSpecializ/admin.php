<?php
$this->pageTitle=Yii::app()->name . ' - '. 'Прайс лист';//$direct->name;
$this->breadcrumbs=array(
	'',
	//'Направления'=>array('/Direction/index'),
	//$direct->name,
);
?>

<?php
if (Yii::app()->user->isAdmin)
	echo '<h2> Администрирование специальностей </h2>';
else
	echo '<h1> Прайс лист </h1>';
?>


<div id="detail"></div>
<?php


if (Yii::app()->user->isAdmin)
	echo CHtml::Button("Создать специальность", array(
		'submit'=>Yii::app()->createUrl('AdmSpecializ/AddSpecializ'),
	));

?>


<div>
	 
	<br/><a href="/file/price2013.xlsx">Прайс лист (скачать) .</a>
	<br/> 
</div>
<br/> 

<div class="row span-10">
<?php
echo CHtml::dropDownList(
		'Specializ[dir_name]', 
		'', 
		CHtml::listData(Direction::model()->findAll(), 'id', 'name'),
		array(
			'prompt'=>'Выбор направления (ВСЕ)',
			'onchange'=>"$.fn.yiiGridView.update('specializ', {data: $(this).serialize()});")
		);
?>
</div>

	
<?php
$this->widget('ext.groupgridview.GroupGridView', array(
	'id'=>'specializ',
	//'dataProvider'=>$SqldataProvider,//$SqldataProvider->searchDao(), 
	'dataProvider'=>$SqldataProvider->searchDao(),
	'filter'=>$SqldataProvider,
	'mergeColumns' => array('dir_name','spec_name'),  
	//'cssFile' => false,
	'pager' => array(
		//'cssFile' => false,
	),
	'columns'=>array(
	// array(
			// 'header'=>'Направление',
			// 'name'=>'dir_name',
			// 'value'=>'$data["dir_name"]',
			// 'filter'=>CHtml::listData(Direction::model()->findAll(), 'id', 'name'),
		// ),
		array(
			'header'=>'Специальность',
			'name'=>'spec_name',
			'type'=>'raw',
			'value'=>'CHtml::link($data["spec_name"],Yii::app()->createUrl("/Specializ/view/", array("id"=>$data["id"])))',
		),

		array(
			'header'=>'Категория',
			'name'=>'name_cat',
			'filter'=>CHtml::listData(Cat::model()->findAll(), 'id', 'name_cat'),
		),
		array(
			'header'=>'Стоимость',
			'name'=>'cost',
			//'value'=>'$data["cost"]',
			'filter'=>false,
		),
		array(
			'header'=>'Ст.годовая',
			'name'=>'cost_year',
			//'value'=>'$data["cost"]',
			'filter'=>false,
		),			
		array(
			'header'=>'Курс',
			'name'=>'kurs',
			'filter'=>CHtml::listData(Kurs::model()->findAll(), 'id', 'name'),
			'value'=>'$data["kursname"]',
		),
		array(
			'visible'=>Yii::app()->user->isAdmin,
			'htmlOptions'=>array('style'=>'text-align: center'),
			'header'=>'Редактировать',
			'name'=>'spec_name',
			'filter'=>false,
			'type'=>'raw',
			'value'=>'Chtml::link("edit",Yii::app()->createUrl("AdmSpecializ/EditSpecializ/",
				array("id"=>$data["id"])))',
		),
		array(
			'visible'=>Yii::app()->user->isAdmin,
			'header'=>'delete',
			'name'=>'spec_name',
			'filter'=>false,
			'type'=>'raw',
			//'value'=>'CHtml::link("удалить","#")',
			 'value'=>'Chtml::link("delete",Yii::app()->createUrl("AdmSpecializ/deleteSpecializ/",
			 	array("id"=>$data["id"], "asDialog"=>1)))',
			'htmlOptions'=>array('style'=>'text-align: center',
				//'onclick'=> 'showDialog( $(this).attr( \'href\' ) ); return false;'),
			'onclick'=> 'showDialog( $(this).find("a").attr( \'href\' ) ); return false;'),
 
		),
		/*array(
			//'class'=>'CButtonColumn',
			//'template'=>'',//'{view}{update}{delete}',
			
			'buttons' => array(
				'view'=>array(
					'url'=>'Yii::app()->createUrl("rates/rates/ViewRate", array("id"=>$data["id"],"asDialog"=>1))',
				),
				'update'=>array(
					'url'=>'Yii::app()->createUrl("rates/rates/update", array("id"=>$data["id"]))',
				),
				/* - РАБОЧИЙ СКРИПТ НА УДАЛЕНИЕ v0.1
				'delete'=>
					array(
						'click'=>'function(){$("#cru-frame").attr("src",$(this).attr("href")); $("#cru-dialog").dialog("open");  return false;}',
						'url'=>'Yii::app()->createUrl("rates/rates/delete", array("id"=>$data["id"],"asDialog"=>1))',
						'options'=>array(  
						'ajax'=>array(
							'type'=>'POST',
							// ajax post will use 'url' specified above 
							'url'=>"js:$(this).attr('href')", 
							'update'=>'#viewRate',
							),
						),
				),*/
				/* - РАБОЧИЙ СКРИПТ НА УДАЛЕНИЕ v1.1 + с диалоговым окном*/
				/*
				'delete'=>array(
						'url'=>'Yii::app()->createUrl("rates/rates/delete", array("id"=>$data["id"],"asDialog"=>1))',
				), 
			),
			
		),*/
	),
)); 

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'mydialog',
    // additional javascript options for the dialog plugin
	'options'=>array(
		'title'=>'Modal Dialog',
		'width'=>400,
		'height'=>200,
		'autoOpen'=>false,
		'resizable'=>false,
		'modal'=>true,
		'overlay'=>array(
			'backgroundColor'=>'#000',
			'opacity'=>'0.5'
		),
		'buttons'=>array(
			'OK'=>'js:function(){isok();}',
			'Cancel'=>'js:function(){iscancel();}',    
		),
	),
));
echo 'Вы действительно хотите удалить ?';
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>


<script type="text/javascript">
/**
 * url на удаление ставки (id_r_rates)
 */
var uri;

/**
 * Функция показывающая диалоговое окно
 * @param string - url на удаление ставки (id_r_rates)
 */
function showDialog(urlZ){
	uri = urlZ;
	$('#mydialog' ).dialog( { title: 'Подвердите действие' } ).dialog( 'open' ); 
}

/**
 * Диалоговое окно: пользователь нажал ок
 * 1. Посылаем запрос на удаление ставки
 * 2. Результат показываем в #viewRate
 * 3. Обновляем Грид
 */
function isok() {
	$("#detail").html('');
	$.ajax({ 
		'url': uri,
		'cache':false,
		'success':function(html){ 
			$('#mydialog').dialog('close');
			$("#detail").html(html);
			$.fn.yiiGridView.update("specializ"); // update (Название грида)
		} 
	});
}

/**
 * Пользователь нажал отмену, закрываем диалоговое окно
 */
function iscancel() {
	$('#mydialog').dialog('close');
}

</script> 