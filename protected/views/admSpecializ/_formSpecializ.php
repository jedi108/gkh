<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'specializ-_formSpecializ-form',
	'enableAjaxValidation'=>false,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => false,
	),
)); ?>

	<p class="note">Поля помеченные <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

<div class="row span-25">

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>110)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row span-13">
		<!-- <div class="row"> -->
			<?php 
				// echo $form->labelEx($model,'id_f_direction');
				// $data = CHtml::listData(Direction::model()->findAll(), 'id', 'name');
				// echo $form->dropDownList($model,'id_f_direction', $data);
				// echo $form->error($model,'id_f_direction');
			?>
		<!-- </div> -->

		<div class="row">
			<?php 
				echo $form->labelEx($model,'id_kurs');
				$dataKurs = CHtml::listData(Kurs::model()->findAll(), 'id', 'name');
				echo $form->dropDownList($model,'id_kurs', $dataKurs, array('prompt'=>'Выберите курс:') );
				echo $form->error($model,'id_kurs');
			?>
		</div>

		<div class="row span-10">
			<?php echo $form->labelEx($model,'id_org'); ?>
			<?php echo $form->dropDownList($model,'id_org', CHtml::listData(Org::model()->findAll(), 'id','name_small'),
						array('prompt'=>'Выберите юр.лицо:')  ); ?>
			<?php echo $form->error($model,'id_org'); ?>
			<div>Если не указанна организация, то будет показан(а) организация и адрес из направления этой специальности</div>
		</div>
	</div>

	<div class="row span-10">
		<div class="row">
			<?php echo $form->labelEx($model,'times'); ?>
			<?php echo $form->textField($model,'times'); ?>
			<?php echo $form->error($model,'times'); ?>
		</div>
            
		<div class="row">
                    
			<?php echo $form->labelEx($model,'cost_all'); ?>
			<?php echo $form->textField($model,'cost_all'); ?>
			<?php echo $form->error($model,'cost_all'); ?>
                    <div>Если общая стоимость> 0, то стоимость первичная и ежегодная будут скрыты.</div>
		</div>
            
		<div class="row">
			<?php echo $form->labelEx($model,'cost'); ?>
			<?php echo $form->textField($model,'cost'); ?>
			<?php echo $form->error($model,'cost'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'cost_year'); ?>
			<?php echo $form->textField($model,'cost_year'); ?>
			<?php echo $form->error($model,'cost_year'); ?>
		</div>
    </div>


	<div class="clear"></div>	
        
	<?php
		if($model->name){
			$this->widget('ext.widgets.multiselects.XMultiSelects',array(
			    'leftTitle'=>'Включенные направления',
			    'leftName'=>'Relation[id_directions][]',
			    'leftList'=>  Direction::getAllDirectionIn($model->id),
			    'rightTitle'=>'Не включенные направления',
			    'rightName'=>'Specializ[id_directions][]',
			    //'rightList'=>  array_diff(Direction::getAllDirectionIn($model->id),  Direction::getAllDirections()),
                            'rightList'=>  array_diff(Direction::getAllDirections(),Direction::getAllDirectionIn($model->id)),
			    'size'=>20,
			    'width'=>'400px',
			));
		} 
	?>
        
</div>

	<?php 
	$this->widget('ext.yiiext.widgets.ckeditor.ECKEditor',array(
		'model'=>$model,                # Data-Model (form model)
		'attribute'=>'body',         # Attribute in the Data-Model
		//'editorTemplate'=>'full',	
		//'editorTemplate' => 'advanced',
		//"toolbar"=>"basic",
		'skin'=>'office2003',
		'options'=>array(
//			'editorTemplate'=>'basic',		
//                'toolbar' => array(
//                    array('Bold','Italic','Underline','Strike','-','NumberedList','BulletedList','-','Outdent','Indent','Blockquote','-','Link','Unlink','-','Table','SpecialChar','-','Cut','Copy','Paste','-','Undo','Redo','-','Maximize',),
//                ),
            'filebrowserBrowseUrl'=>CHtml::normalizeUrl(array('site/browse')),
			'filebrowserImageBrowseUrl'=>CHtml::normalizeUrl(array('site/browse')),
			//'filebrowserUploadUrl'=>CHtml::normalizeUrl(array('site/browse')),
			'filebrowserUploadUrl'=>CHtml::normalizeUrl(array('site/file')),

        ),
	) );
	?>

<div class='vids row span-12'>
<?php 
$AllSelection= CHtml::listData(CatSpecializ::model()->catSpec(), 'id', 'name_cat');
$Selected = CatSpecializ::catSel($model->id);

echo CHtml::checkBoxList('cat', $Selected, $AllSelection);
//echo //$form->error($model->catSpecializs, 'id_t_cat'); 
?>
</div>
<style>
.vids input{
	float: left;
}
</style>

<div class="row span-8 the_dates">
 
<label for="Specializ_dates">Даты</label> 
	<?php
		//echo $form->labelEx($model->specializDates,'date'); 
		foreach($model->specializDates as $i=>$item) {
			echo '<div class="clear"></div>';
			$item['date'] = date("d.m.Y",strtotime($item['date']));
			echo CHtml::activeTextField($item,"[$i]date");
			echo Chtml::button('удалить', array(
				'name'=>'del',
				'onclick'=>'delOpt('.$i.')',
				'id'=>'SpecializDatesD_'.$i.'_date',
				//'htmlOptions'=>array('class'=>'deletes'),
				));
			// $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			//    'name' => $i.'_date',
			//    'model' => $item,
			//    'attribute' => '[$i]date',
			//    'language' => 'ru',
			//    'options' => array(
			//        'showAnim' => 'fold',
			//    ),
			//    'htmlOptions' => array(
			//        'style' => 'height:20px;'
			//    ),
			// )); 
			echo $form->error($item,"[$i]date");
			echo '<div class="clear"></div>';
		}
	?>
	

 
<input type="button" class="treeSmall" value=" + Добавить дату" onclick="addOpt()">

	<div class="clear"></div>

</div>

	<div class="clear"></div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Сохранить');  ?>
	</div>
<?php $this->endWidget(); ?>
 

</div><!-- form -->

<script>
function addOpt() 
{
	count = $(".the_dates input[type='text']").length;
	inputOpt = '<input name="SpecializDates[' + count + '][date]" id="SpecializDates_' + count + '_date" type="text"  value="26-07-2012">';
	inputDel = '<input name="del" onclick="delOpt(' + count + ')" id="SpecializDatesD_' + count + '_date" type="button" value="удалить">';
	$(".the_dates").append(inputOpt);
	$(".the_dates").append(inputDel);
}

function delOpt(i)
{
	$('#SpecializDates_' + i + '_date').remove();
	$('#SpecializDatesD_' + i + '_date').remove();
}
</script>