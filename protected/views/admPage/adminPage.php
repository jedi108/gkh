<?

echo CHtml::Button('Создать страницу', array(
	'submit'=>Yii::app()->createUrl('AdmPage/addPage'),
));

$this->widget('ext.groupgridview.GroupGridView', array(
	'id'=>'pages',
	'dataProvider'=>$dataProvider->search(),
	'filter'=>$dataProvider,
	'columns'=>array(
		array(
			//'header'=>'Направление',
			//'name'=>'dir_name',
			'value'=>'$data->title',
			'filter'=>CHtml::listData(Direction::model()->findAll(), 'id', 'name'),
			//'type'=>'raw',
		),
		array(
			'header'=>'Тип страници',
			'name'=>'type',
			'value'=>'$data->pageType',
			'filter'=>Page::model()->types,
		),
		array(
            'class'=>'CButtonColumn',
			'viewButtonUrl'=>'$this->grid->controller->createUrl("page/view",array("id"=>$data->id))',
            'updateButtonUrl'=>'$this->grid->controller->createUrl("EditPage",array("id"=>$data->id))',
            'deleteButtonUrl'=>'$this->grid->controller->createUrl("delete",array("id"=>$data->id))',
            'deleteConfirmation'=>Yii::t('ui','Are you sure to delete this item?'),
        ),
	),
));
?>