<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'Page',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
        ),
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'type'); ?>
        <?php echo $form->dropDownList($model, 'type', $model->types); ?>
        <?php echo $form->error($model, 'type'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size' => 100, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'date_created'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'language' => 'ru',
            'model' => $model,
            'attribute' => 'date_created',
            'options' => array(
                'showAnim' => 'fold',
                'dateFormat' => 'yy-mm-dd',
            ),
            'htmlOptions' => array(
                'style' => 'height:20px;'
            ),
        ));
        ?>
    </div>



    <div class='files'>

        <?php
        $this->renderPartial('application.modules.jfiles.views._attrFile', array('model' => $model->jfiles));
        ?>

    </div>

    <?php
    $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
        'id' => 'one',
        'params' => array(
            'path' => '/file/tmp/', //--- Путь к папке картинок
            'previewId' => '#img-logo', //--- <img src id="" (превью)
            'inputId' => '#Direction_img_logo', //--- <text type=input id="" (текстбокс)
            'preset' => '100x80', //--- Превьющка которая появится в <img src id="" 
        ),
        'config' => array(
            'onComplete' => "js:onComplete",
        ),
    ));
    ?>

    <br/>
    <?php
    // OLD Version
    //-----------------------------------------------------------
    // if (isset($jFiles)) {
    //   for ($i=0; $i < (count($jFiles)); $i++) { 
    //    		echo $this->renderPartial('application.modules.jfiles.views._jfile', array(
    //    			'submodel'=>$jFiles[$i],
    //    			'index'=>$i+1,
    //    		)); 
    //    }    
    // }
    ?>


    <?php //echo CHtml::button('Add file', array('class'=>'prod_add')); ?>


    <div class="row">
        <?php echo $form->labelEx($model, 'body'); ?>
        <?php //echo $form->textField($model,'body'); ?>
        <?php echo $form->error($model, 'body'); ?>
    </div>

        <?php
        $this->widget('ext.yiiext.widgets.ckeditor.ECKEditor', array(
            'model' => $model, # Data-Model (form model)
            'attribute' => 'body', # Attribute in the Data-Model
            //'editorTemplate'=>'full',	
            //'editorTemplate' => 'advanced',
            //"toolbar"=>"basic",
            'skin' => 'office2003',
            'options' => array(
//			'editorTemplate'=>'basic',		
//                'toolbar' => array(
//                    array('Bold','Italic','Underline','Strike','-','NumberedList','BulletedList','-','Outdent','Indent','Blockquote','-','Link','Unlink','-','Table','SpecialChar','-','Cut','Copy','Paste','-','Undo','Redo','-','Maximize',),
//                ),
                'filebrowserBrowseUrl' => CHtml::normalizeUrl(array('site/browse')),
                'filebrowserImageBrowseUrl' => CHtml::normalizeUrl(array('site/browse')),
                //'filebrowserUploadUrl'=>CHtml::normalizeUrl(array('site/browse')),
                'filebrowserUploadUrl' => CHtml::normalizeUrl(array('site/file')),
            ),
        ));
        ?>

    <?php echo $form->error($model, 'body'); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'id_gallary'); ?>
        <?php echo $form->dropDownList($model, 'id_gallary', GallaryImages::model()->getAllGallarys(), array('prompt' => 'Выберете галерею')); ?>
        <?php echo $form->error($model, 'id_gallary'); ?>
    </div>
    
    <div class="row buttons">
        <?php echo CHtml::submitButton('Сохранить'); ?>
    </div>

        <?php $this->endWidget(); ?>

</div><!-- form -->


