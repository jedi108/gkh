<?php $this->beginContent('//layouts/main'); ?>
<div class="left-menu-block last">
	<div class="button-about">
		<a href="<?php echo Yii::app()->createUrl('/site/about');?>">О комбинате</a>
	</div>
<?php
$res = Jmenu::model()->findByPK(19)->getCMenuArray();
echo '<div class="left_about_menu">';
$this->widget('zii.widgets.CMenu',array(
		 	'items'=>  $res[1]['items'],
		));
echo '</div>';
?>
	<div class="row leftMenu">
		<!--<div class="block-title">Главное меню</div>-->
		<?php 
		$this->widget('zii.widgets.CMenu',array(
		 	'items'=>  Jmenu::model()->findByPK(70)->getCMenuArray(),
		));
        ?>
	</div>
	<div class="login-block">
		<span>

            <?php if (Yii::app()->user->isGuest) { ?>
                <a href="<?php echo Yii::app()->createUrl('/user/login');?>" class="login-link">Вход</a>
                <a href="<?php echo Yii::app()->createUrl('/user/registration');?>" class="reg-link">Регистрация</a>
            <?php } else { ?>
                <a href="<?php echo Yii::app()->createUrl('/user/profile');?>" class="login-link">Профиль</a>
                <a href="<?php echo Yii::app()->createUrl('/user/logout');?>" class="reg-link">Выход</a>
            <?php } ?>
		</span>
	</div>
	<div class="button-kurs">
		<a href="<?php echo Yii::app()->createUrl('/page/15');?>">
			<span class="p1">Записаться</span>
			<span class="p2">на курсы</span>
		</a>		
	</div>
	
	<?php
		Yii::app()->clientScript->registerScript('button-kurs', '
			$(".button-kurs a").hover(
				function(){
					$(".p1").css({"color":"#840000"});
					$(".p2").css({"color":"#eb2629"});
				},
				function(){
					$(".p1").css({"color":"#000000"});
					$(".p2").css({"color":"#840000"});
				}
			);
		',
		CClientScript::POS_READY);
	?>
	<?php if(Article::model()->count()>0) { ?>
	<div class="block-title-article"><a href="/article" style="padding:0">Статьи</a></div>
    	<div class="left-info">
		<?php
		foreach ($models=Article::model()->findAll(array('limit'=>3)) as $item => $model) {
//			echo '<div class="date">';
//			echo Yii::app()->dateFormatter->format('d.MM.y', $model->date_created);
//			echo '</div>';
			if ($item > 0)
			echo '<div class="info_fon_top">&nbsp;</div>';
			echo '<div>';
			echo CHtml::link($model->title, Yii::app()->createUrl('/article/article/view',array('id'=>$model->id)));
			echo '</div>';
		}
		?>
			<div class="info_fon_top">&nbsp;</div>
			<div class="info_all_link">
			<?php echo CHtml::link('Все статьи >', Yii::app()->createUrl('/article')); ?>
			</div>
		</div>   
	<?php } ?>
	<?php //if (Yii::app()->getRequest()->getPathInfo() != "Direction/index" && Yii::app()->getRequest()->getPathInfo() != ""){ ?>
        <div class="block-title"><a href="/news" style="padding:0">Новости</a></div>
    	<div class="left-info">
		<?php
		foreach ($models=News::model()->findAll(array('condition'=>'type=0', 'limit'=>5)) as $item => $model) {
			if ($item > 0)
			echo '<div class="info_fon_top">&nbsp;</div>';
			echo '<div class="date">';
			echo Yii::app()->dateFormatter->format('d.MM.y', $model->date_created);
			echo '</div>';
			echo '<div>';
			echo CHtml::link($model->title, Yii::app()->createUrl('/news/view',array('id'=>$model->id)));
			echo '</div>';
		}
		?>
		<div class="info_fon_top">&nbsp;</div>
		<div class="info_all_link">
			<?php echo CHtml::link('Все новости >', Yii::app()->createUrl('/news')); ?>
		</div>
        </div>
		<?php //} ?>
	<?php
// Yii::import('application.modules.juser.components.*');   
// print_r (Yii::app()->user);die();
$role = Yii::app()->user->role;
if($role=='admin' || $role=='sex') {
	echo '<div id="sidebar" style="width: 225px; margin: 0 auto;">';
	//echo CHtml::button('Управление заявками',array('submit'=>'/zayavka/admin'));
	echo CHtml::button('Управление отзывами',array('submit'=>'/feedback/admin'));
	echo CHtml::button('Выход из сайта',array('submit'=>'/site/logout'));
	echo '</div>';
}

	?>	 
	<!-- sidebar -->
	<div class="left-banner">
            <a href="/Direction/3">
		<img width="292px" src="<?php echo Yii::app()->request->baseUrl; ?>/upload/banner/banner_1.png"/>
            </a>
	</div>
	<div style="clear: both;"></div>
</div>
    
<!--<div class="span-19 last"> -->
<div class="row" style="width:960px;float: right;">

    
	<?php

	if (Yii::app()->user->isAdmin || Yii::app()->user->isAdminSite) {
		?><div class="row"><?
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Operations',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
		?></div><!-- sidebar --><?
	}
	?>
			
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>

<?php $this->endContent();