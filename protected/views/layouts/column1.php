<?php $this->beginContent('//layouts/main'); ?>

	
	<?php

	if (Yii::app()->user->isAdmin || Yii::app()->user->isAdminSite) {
		?><div class="row"><?
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Operations',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
		?></div><!-- sidebar --><?
	}
	?>
	


<div id="content">
	<?php echo $content; ?>


</div><!-- content -->


	
<?php $this->endContent(); ?>