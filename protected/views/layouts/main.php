<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<meta name="keywords" content="Обучение специалистов ЖКХ. жкх москвы, ЖКХ, Специалист ЖКХ. ЖКХ вакансии. Работники ЖКХ, Портал ЖКХ, жкх россии, обучение жкх, курсы жкх, жилищно комунальное хозяйство, учебно-курсовой комбинат жилищно коммунального хозяйства">
	<meta name="description" content="Учебно-курсовой комбинат жилищно-коммунального хозяйства. Обучение специалистов ЖКХ. Комбинат реализует как основные образовательные программы подготовки работников ЖКХ по 68 специальностям, так и программы дополнительного профессионального образования (профессиональная переподготовка и повышение квалификации) рабочих, инженерно-технических работников и специалистов по вопросам охраны труда и техники безопасности,  экологии, пожарной безопасности, безопасной эксплуатации опасных производственных объектов, энергоресурсосбережения, реформирования ЖКХ и другим специальностям.">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/my.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/wmenu.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/leftmenu.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/tooltip.css" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon114x114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon72x72.png">

<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/cvi_map_lib.js', CClientScript::POS_HEAD);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/cvi_tip_lib.js', CClientScript::POS_HEAD);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/mapper.js', CClientScript::POS_HEAD);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/maputil.js', CClientScript::POS_HEAD);
	Yii::app()->clientScript->registerScript('maps', '
		function showCoords(map,area,x,y,w,h) {
			function parseDMS(v,n){
				var d,m,s; d=parseInt(v); 
				m=Math.abs(parseFloat(v-d)*60); 
				s=Math.abs(parseFloat(parseInt(m)-m)*60); 
				return Math.abs(d)+"° "+parseInt(m)+"\' "+parseInt(s)+"\'\' "+n;
			}	
			if(map=="map") {
				alert(1);
				var obj, country = "", lon = (x*360/w)-180, lat = 90-(y*180/h);
				lon = parseDMS(lon,lon!=0?(lon<0?"W":"E"):""); lat = parseDMS(lat,lat!=0?(lat<0?"S":"N"):"");
				if(area!=0) {obj = document.getElementById(area); country = "  ("+(obj.title||obj.alt)+")";} 
				document.getElementById("map").innerHTML = "<p class=\'coords\'>Latitude: "+lat+"  Longitude: "+lon+country+"<\/p>";
			}
		}
	',
	CClientScript::POS_READY);

	$cs = Yii::app()->getClientScript();
	$cs->registerCoreScript("jquery");

	if (!Yii::app()->user->isAdmin) {
//            Yii::app()->clientScript->registerScriptFile(
//                Yii::app()->assetManager->publish(
//                     Yii::getPathOfAlias('webroot').'/js/anim.js'
//                ),
//                CClientScript::POS_END
//            );	
	}
?>

</head>

<body>
<?php if (!Yii::app()->user->isAdmin) { ?>
<div id="header">
	<div class="header_block">
		<a href="/" class="logotype-link">
			<div id="logotype">
			</div>
		</a>
		<div class="slogan">
			<p class="slogan-p1">
			Государственное автономное образовательное учреждение Московской области
			</p>
			<p class="slogan-p2">
			"Учебно-курсовой комбинат жилищно-коммунального хозяйства"
			</p>
		</div>
		<div class="header-contact">
			<p class="header-contact-phone">(495) 529-74-77</p>
			<p class="header-contact-fax">факс (495) 529-74-16</p>
			<p class="header-contact-text">143900, Московская область, г.Балашиха, ул.Советская,	д.42</p>
			<p class="header-contact-text">E-mail: ukk2010@rambler.ru</p>
		</div>
		<div style="clear: both;"></div>
	</div>
	<!-- <div class='slogan'>
	Министерство образования Московской области Лицензия на право осуществления образовательной деятельности Серия 50Л01 № 0000348 от 06.02.2013 г. (Рег. № 70170)
	</div> -->
	<!-- <a id="logomain" href="/">
	<div id="logo"><?php //echo CHtml::encode(Yii::app()->name); ?></div>
	</a> -->
</div><!-- header -->
<?php } ?>
<div class="container" id="page">
<div class="bottom-content">
	<div class="bottom-content-sub">
		&nbsp;
	</div>
</div>
<div id="wmenuz">
<?php
$this->widget('ext.JQuerySlideMenu.JQuerySlideMenu', array(
    'items'=>  Jmenu::model()->findByPK(19)->getCMenuArray(),
));	
?>
</div>
	<div class="container-sub">
	<?php //if(isset($this->breadcrumbs)):?>
		<?php //$this->widget('zii.widgets.CBreadcrumbs', array(
			//'links'=>$this->breadcrumbs,
			//'homeLink'=>CHtml::link('Главная', Yii::app()->homeUrl),
		//)); ?><!-- breadcrumbs -->
	<?php //endif?>


	 
	<?php echo $content; ?>
	 

	<div class="clear"></div>
 




<?php
$controller = Yii::app()->getController();
$isHome = $controller->getId() === 'site' && $controller->getAction()->getId() === 'index';
if ($isHome) {

	// $D = Direction::model()->findAll('img_slide IS NOT NULL');
	// 	$files = array();
	// 	foreach ($D as $key => $field) {
	// 		if ( $field->img_slide!='') { 
	// 			$img = array (
	// 				'image'=>Media::imgPath($field->img_slide,'slide', 'temp'),
	// 				'alt'=>$field->name,
	// 				'url'=>Yii::app()->CreateUrl('/Direction/view/', array('id'=>$field->id)),
	// 				'info'=>array(
	// 					'title'=>$field->name,
	// 					'text'=>false,//$field->name,
	// 				),
	// 			);
	// 			$files[]=$img;
	// 		}
	// 	}
	// $this->widget('ext.coinSlider.CoinSliderWidget', array(
	//     'items' => $files
	// ));
}
// 	CVarDumper::dump($files, 100, true);
//* START ------------- NIVO ----------------
// if (!Yii::app()->user->isAdmin) {
// 	$D = Direction::model()->findAll('img_slide IS NOT NULL');
// 		$files = array();
// 		foreach ($D as $key => $field) {
// 			if ( $field->img_slide!='') { 
// 				$img = array (
// 					'src'=>Media::imgPath($field->img_slide,'slide', 'temp'),
// 					'url'=>Yii::app()->CreateUrl('/Direction/view/', array('id'=>$field->id)),
// 					'caption'=>$field->name,
// 					'linkOptions'=>array('title'=>$field->name),
// 					//'linkOptions'=>Yii::app()->request->baseUrl.'/site/page?view=events',
// 				);
// 				$files[]=$img;
// 			}
// 		}
// 	$this->widget('application.extensions.nivoslider.ENivoSlider', array(
// 	    'images'=>$files,
// 	    )
// 	);	
// }
		

 
//* -------------------- NIVO ------------- END
?>

<?php //if (Yii::app()->user->isGuest) { ?>    
<!--
<script type="text/javascript" src="//yandex.st/share/share.js"
charset="utf-8"></script>
<div align="right" class="yashare-auto-init" data-yashareL10n="ru"
 data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,gplus"
></div>-->
<?php //} ?>      
	</div>
	<div style="clear: both;"></div>
</div><!-- page -->


<div id="footer">
	<div class="footer-sub-1">
		<div class="footer-conact floatleft">
			<div class="footer-conact-title">Контакты</div>
			<div class="footer-conact-phone">(495) 529-74-77</div>
			<div class="footer-conact-fax">факс (495) 529-74-16</div>
			<p>143900, Московская область, г.Балашиха, ул.Советская, д.42</<p>
			<p>E-Mail:<a href="mailto:ukk2010@rambler.ru">ukk2010@rambler.ru</a></p>
		</div>
                <div class="floatright">
                    <br/>
                    <a href="http://vk.com/id276635078" target="_blank"><img alt="" src="/css/images/vkontakte.png" style="width: 128px;"></a>
                    &nbsp;&nbsp;
                    <a href="https://www.facebook.com/profile.php?id=100007286176474&amp;fref=ts" target="_blank"><img alt="" src="/css/images/facebook.png" style="width: 128px;"></a>
                </div>
                <div class="clear"></div>
	</div>
	<div class="footer-sub-2">
		<div class="footer-menu">
			<div class="footer-copyright">
				2004-<?php echo date('Y'); ?> &copy; <span>УКК ЖКХ Балашиха</span>
			</div>
			<div class="footer-links qq-upload-list">
				<ul>
					<li><a href="/site/about">О комбинате</a></li>
					<li>|</li>
					<li><a href="/site/uslugi">Услуги</a></li>
					<li>|</li>
					<li><a href="#">Правил приема</a></li>
					<li>|</li>
					<li><a href="#">Расписание</a></li>
					<li>|</li>
					<li><a href="#">Договор</a></li>
					<li>|</li>
					<li><a href="#">Заявки</a></li>
					<li>|</li>
					<li><a href="/page/48">Прайс</a></li>
					<li>|</li>
					<li><a href="#">Полезная информация</a></li>
				</ul>
			</div>
			<div style="clear: both;"></div>
		</div>
	</div>
	<!--<div id="footer" class="container">
<?php if (!Yii::app()->user->isAdmin) { ?>
<h5>
	Государственное автономное образовательное учреждение Московской области «Учебно-курсовой комбинат жилищно-коммунального хозяйства» 
</h5>
		
		 
143900, Московская область,   г.Балашиха,   ул.Советская,   д.42 
		 <br>
		 
тел. (495) 529-74-77, факс (495) 529-74-16
		 
		  <br>
E-mail: ukk2010@rambler.ru
		 


<?php } ?>
		<br>Copyright &copy; <?php echo date('Y'); ?> by jedi-it.<br/>
		 
<div style="margin-left: 39px;float: right;">
<!-- Yandex.Metrika informer --><!--
<a href="http://metrika.yandex.ru/stat/?id=16475860&amp;from=informer"
target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/16475860/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:16475860,lang:'ru'});return false}catch(e){}"/></a>
<!-- /Yandex.Metrika informer -->

</div>
 
<?php if (Yii::app()->user->isAdmin) $this->widget('dash.components.DashMenuWidget'); ?>

<?php if (!Yii::app()->user->isAdmin) { ?>

    <?php include_once("analyticstracking.php") ?>


<?php } ?>

</body>
</html>
