<?

echo CHtml::Button('Создать лицензию', array(
	'submit'=>Yii::app()->createUrl('LicenzAdm/create'),
));

$this->widget('ext.groupgridview.GroupGridView', array(
	'id'=>'lic',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'name',
		'pic',
		array(
            'class'=>'CButtonColumn',
        ),
	),
));
?>