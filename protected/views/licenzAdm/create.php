<?php
$this->breadcrumbs=array(
	'Лицензии'=>array('/LicenzAdm/admin'),
	'Создать лицензию',
);

$this->menu=array(
 	array('label'=>'Управление лицензиями', 'url'=>array('admin')),
);
?>

<h1>Создать лицензию</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>