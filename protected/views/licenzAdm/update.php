<?php
$this->breadcrumbs=array(
	'Profiles'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
 
	array('label'=>'Создать лицензию', 'url'=>array('create')),
	array('label'=>'Просмотри лицензии', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление лицензиями', 'url'=>array('admin')),
);
?>

<h1>Редактировать <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>