<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'enableAjaxValidation'=>true,
	'enableAjaxValidation'=>true,
	'clientOptions' => array(
		'validateOnSubmit' => false,
		'validateOnChange' => false,
))); ?>

 

	<p class="note">Поля отмеченные <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>70,'maxlength'=>155)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
 

	<div class="row span-7">
		<?php
		/*		 * * uploadify ** */
		echo $form->labelEx($model, 'pic');
		echo $form->textField($model, 'pic', array('style' => 'display:none'));

		if ($model->pic == '')
			$file = '/file/no-photo.jpg';
		else
			$file = sprintf($model->pic, '');

		echo Media::img($file, '100x80', 'temp', array('id' => 'img-logo'));
		echo $form->error($model, 'pic');

		$this->widget('ext.EAjaxUpload.EAjaxUpload', array(
			'id' => 'one',
			'params' => array(
				'path' => '/file/tmp/', //--- Путь к папке картинок
				'previewId' => '#img-logo', //--- <img src id="" (превью)
				'inputId' => '#Licenz_pic', //--- <text type=input id="" (текстбокс)
				'preset' => '100x80', //--- Превьющка которая появится в <img src id="" 
			),
		));
		/*		 * * END uploadify ** */
		?>
		<?php echo $form->error($model,'pic'); ?>
	</div>
    <br class="clear"><br>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->