<?php
$this->breadcrumbs=array(
	'Directions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Direction', 'url'=>array('index')),
	array('label'=>'Create Direction', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('direction-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Directions</h1>
 

<?php 
$assetsDir = dirname(__FILE__).'/../assets';

$buttons = array('class'=>'CButtonColumn');
$buttons['viewButtonUrl']='Yii::app()->controller->createUrl("/Direction/view",array("id"=>$data->primaryKey))';; 
// $buttons['updateButtonUrl']='Yii::app()->controller->createUrl("update",array("id_post"=>$data->primaryKey))';; 
// $buttons['deleteButtonUrl']='Yii::app()->controller->createUrl("delete",array("id_post"=>$data->primaryKey))';;


$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'direction-grid',
	'dataProvider'=>$model->search(),
    'pager' => array(
            'class'=>'CAjaxLinkPager',
            //'updateId' => 'membersListDiv',
            //'loadingCssClass' => 'grid-view-loading',
    ),
    'enablePagination'=>true,
	'filter'=>$model,
	'template'=>"{items}",
	'columns'=>array(
		'id',
		'name',
		array(
			'name'=>'img_logo',
			'type'=>'raw',
			'value' => '$data->Imglogo',
		),
		array(
			'name'=>'img_slide',
			'type'=>'raw',
			'value' => '$data->Imgslide150x80',
		),
		$buttons,
	),
)); ?>
