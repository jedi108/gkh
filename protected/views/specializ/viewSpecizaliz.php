<?php
//$this->pageTitle = $mSpecializ->idFDirection['name'] . '. Курсы ЖКХ Москвы. ';
//$this->pageTitle='Обучение специалистов ЖКХ. ' . $mSpecializ->idFDirection['name']  .'. Курсы ЖКХ Москва.';
?>

<?php
if (Yii::app()->user->getState('direction_name')) {
    $this->breadcrumbs = array(
    'Направления' => array('/site/uslugi'),
    Yii::app()->user->getState('direction_name') =>
        array('/Direction/view', 'id'=>Yii::app()->user->getState('direction_id'))
    );
} else {
    $this->breadcrumbs = array(
        'Направления' => array('/site/uslugi'),
    );
}
?>


<?php
$this->menu = array(
    array('label' => 'Редактировать', 'url' => array('AdmSpecializ/EditSpecializ', 'id' => $mSpecializ->id)),
    array('label' => 'Все страницы', 'url' => array('AdmSpecializ/admin')),
);
?><h1><?
echo $mSpecializ['name'];
//echo $mSpecializ->idFDirection->imgslide;
?></h1><?
    if ($mSpecializ->isKurs) {
        echo '<span>' . $mSpecializ->KursData['field'] . ':</span> ';
        echo '<b>' . $mSpecializ->KursData['data'] . '</b>';
    }
    ?><br/><p><br/><?
if ($mSpecializ['cost_all'] == 0) {
    if ($mSpecializ['cost'] > 0)
        echo '<span>Стоимость первичная:</span> <b>' . $mSpecializ['cost'] . '</b> руб';
    ?><br/><?
            if ($mSpecializ['cost_year'] > 0)
                echo '<span>Стоимость ежегодная:</span> <b>' . $mSpecializ['cost_year'] . '</b> руб';
        } else {
            echo '<span>Стоимость общая:</span> <b>' . $mSpecializ['cost_all'] . '</b> руб';
            ?><br/><?
    }
    ?></p><?
    ?>
<em style="font-size: 90%;">
    * Примечание: стоимость обучения одного человека может отличаться от указанной в прайс-листе в зависимости от наполняемости учебной группы и условий проведения лекционных и практических занятий. 
    Проезд и проживание в стоимость обучения не входят.
</em>
<!--<div class="clear"></div>-->
<?
?><p><?
//echo '<span>Часов обучения:</span>'.$mSpecializ['times'];
?></p><?
?>
<!--<div class="clear"></div>-->
    <?
    ?><p><?
    ?><!--<div>Категориии:</div>--><?
foreach ($mSpecializ->categories as $key => $cat) {
    echo $cat['name_cat'] . '<br/>';
}
    ?></p><?
    echo $mSpecializ->body;
    ?>

<p><?
    ?><!---<div>Даты проведения:</div>---><?
//	foreach ($mSpecializ->specializDates as $key => $date) {
//		echo $date['date']. '<br/>';
//	}
?></p><? ?>
<!--<div class="clear"></div>-->
    <?
// Показывает привязанный филиал
//
// $orgData = $mSpecializ->orgAdr;
// echo '<div class="highlight">'.$orgData['name_small'].'</div>';
// echo '<address>'.$orgData['adr_fact'].'</address>';
// echo '<div>'.$orgData['phone'].'</div>';
    ?>

<div style="overflow: hidden;">
    <div class="add_zayav">
        <a href="/page/15" title="Обучение специалистов ЖКХ">Заявки на обучение специалистов ЖКХ</a>
<?php
//echo CHtml::link('Оставить заявку от частного лица', 
//		Yii::app()->createUrl('/Zayavka/addProfile',array('id_specializ'=>$mSpecializ->id) )
//	);
?>
    </div>        
</div>

<!--Направления-->
        <?php
        if (count(Direction::getAllDirectionIn($mSpecializ->id)) > 0) {
            echo '<h5>Направления в которые входит специальность:</h5>';
            echo '<ul>';
            foreach (Direction::getAllDirectionIn($mSpecializ->id) as $id => $name) {
                echo '<li>';
                echo CHtml::link($name, array('/Direction/view', 'id'=>$id));
                echo '</li>';
            }
            echo '</ul>';
        }
        ?>

<?php
//echo CHtml::link('Оставить заявку от компании', 
//		Yii::app()->createUrl('/Zayavka/addOrg',array('id_specializ'=>$mSpecializ->id) )
//	);
?>

<div class="clear"></div>


<?php
//ФИЛЛИАЛЫ
//$o = Org::model()->findAll('orglink = 1');
//
//foreach ($o as $i => $org) {
//	echo '<div>'. $org['name_small'] . '</div>';
//}
//CVarDumper::dump($mSpecializ->direction[0]->attributes,100,1);
//echo FunLib::prrAttr($o, 100,1);
// echo FunLib::prrAttr($mSpecializ->idFDirection);
// echo FunLib::prrAttr($mSpecializ);
// echo FunLib::prrAttr($mSpecializ->specializDates);
// echo FunLib::prrAttr($mSpecializ->categories);
?>