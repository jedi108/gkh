<?php
$this->breadcrumbs=array(
	'Orgs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Org', 'url'=>array('/Org/index')),
	array('label'=>'Create Org', 'url'=>array('/Org/create')),
	array('label'=>'View Org', 'url'=>array('/Org/view', 'id'=>$model->id)),
	array('label'=>'Manage Org', 'url'=>array('/Org/admin')),
);
?>

<h1>Редактирование организации <strong><?php echo $model->name_small; ?></strong></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>