<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'org-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name_small'); ?>
		<?php echo $form->textField($model,'name_small',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name_small'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FIO_rukovoditel'); ?>
		<?php echo $form->textField($model,'FIO_rukovoditel',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'FIO_rukovoditel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FIO_ispolnitil'); ?>
		<?php echo $form->textField($model,'FIO_ispolnitil',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'FIO_ispolnitil'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'logo'); ?>
		<?php echo $form->textField($model,'logo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'logo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'inn'); ?>
		<?php echo $form->textField($model,'inn',array('size'=>12,'maxlength'=>12)); ?>
		<?php echo $form->error($model,'inn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kpp'); ?>
		<?php echo $form->textField($model,'kpp',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'kpp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bik'); ?>
		<?php echo $form->textField($model,'bik',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'bik'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'city'); ?>
		<?php echo $form->textField($model,'city',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'city'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'adr_fact'); ?>
		<?php echo $form->textField($model,'adr_fact',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'adr_fact'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adr_ur'); ?>
		<?php echo $form->textField($model,'adr_ur',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'adr_ur'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fax'); ?>
		<?php echo $form->textField($model,'fax',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'fax'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnk_korr'); ?>
		<?php echo $form->textField($model,'bnk_korr',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'bnk_korr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bnk_rchet'); ?>
		<?php echo $form->textField($model,'bnk_rchet',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'bnk_rchet'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'_org_type'); ?>
		<?php //echo $form->textField($model,'_org_type'); ?>
		<?php echo CHtml::dropDownList('Org[_org_type]', $model->_org_type, $model->_types); ?>
		<?php echo $form->error($model,'_org_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'orglink'); ?>
		<?php //echo $form->textField($model,'_org_type'); ?>
		<?php echo CHtml::dropDownList('Org[orglink]', $model->orglink, $model->org_links); ?>
		<?php echo $form->error($model,'orglink'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->