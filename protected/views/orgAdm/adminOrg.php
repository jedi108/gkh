<h2>Справочник организаций </h2>

<div id="detail"></div>
<?php

echo CHtml::Button("Создать организацию", array(
	'submit'=>Yii::app()->createUrl('AdmSpecializ/AddSpecializ'),
));


// echo CHtml::Button("Создать специализацию", array(
// 		'submit' => 'submit'=>Yii::app()->createUrl('AdmSpecializ/AddSpecializ')));


$this->widget('ext.groupgridview.GroupGridView', array(
	'id'=>'org',
	//'dataProvider'=>$SqldataProvider,//$SqldataProvider->searchDao(), 
	'dataProvider'=>$SqldataProvider->searchDao(),
	'filter'=>$SqldataProvider,
	//'mergeColumns' => array('name_small','spec_name'),  
	//'cssFile' => false,
	'pager' => array(
		//'cssFile' => false,
	),
	'columns'=>array(
		array(
			'header'=>'Краткое название',
			'name'=>'name_small',
			'value'=>'$data["name_small"]',
			//'filter'=>CHtml::listData(Direction::model()->findAll(), 'id', 'name'),
			//'type'=>'raw',
		),
		array(
			'header'=>'Лого',
			'name'=>'logo',
			//'filter'=>false,
			//'htmlOptions'=>array('style'=>'text-align: left'),
		),

		array(
			'header'=>'Инн',
			'name'=>'inn',
			'filter'=>false,
		),	
		array(
			'htmlOptions'=>array('style'=>'text-align: center'),
			'header'=>'Редактировать',
			'name'=>'name_small',
			'filter'=>false,
			'type'=>'raw',
			'value'=>'Chtml::link("edit",Yii::app()->createUrl("AdmSpecializ/EditSpecializ/",
				array("id"=>$data["id"])))',
		),
		array(

			'header'=>'delete',
			'name'=>'name_small',
			'filter'=>false,
			'type'=>'raw',
			//'value'=>'CHtml::link("удалить","#")',
			 'value'=>'Chtml::link("delete",Yii::app()->createUrl("AdmSpecializ/deleteSpecializ/",
			 	array("id"=>$data["id"], "asDialog"=>1)))',
			'htmlOptions'=>array('style'=>'text-align: center',
				//'onclick'=> 'showDialog( $(this).attr( \'href\' ) ); return false;'),
			'onclick'=> 'showDialog( $(this).find("a").attr( \'href\' ) ); return false;'),
 
		),
		/*array(
			//'class'=>'CButtonColumn',
			//'template'=>'',//'{view}{update}{delete}',
			
			'buttons' => array(
				'view'=>array(
					'url'=>'Yii::app()->createUrl("rates/rates/ViewRate", array("id"=>$data["id"],"asDialog"=>1))',
				),
				'update'=>array(
					'url'=>'Yii::app()->createUrl("rates/rates/update", array("id"=>$data["id"]))',
				),
				/* - РАБОЧИЙ СКРИПТ НА УДАЛЕНИЕ v0.1
				'delete'=>
					array(
						'click'=>'function(){$("#cru-frame").attr("src",$(this).attr("href")); $("#cru-dialog").dialog("open");  return false;}',
						'url'=>'Yii::app()->createUrl("rates/rates/delete", array("id"=>$data["id"],"asDialog"=>1))',
						'options'=>array(  
						'ajax'=>array(
							'type'=>'POST',
							// ajax post will use 'url' specified above 
							'url'=>"js:$(this).attr('href')", 
							'update'=>'#viewRate',
							),
						),
				),*/
				/* - РАБОЧИЙ СКРИПТ НА УДАЛЕНИЕ v1.1 + с диалоговым окном*/
				/*
				'delete'=>array(
						'url'=>'Yii::app()->createUrl("rates/rates/delete", array("id"=>$data["id"],"asDialog"=>1))',
				), 
			),
			
		),*/
	),
)); 

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'mydialog',
    // additional javascript options for the dialog plugin
	'options'=>array(
		'title'=>'Modal Dialog',
		'width'=>400,
		'height'=>200,
		'autoOpen'=>false,
		'resizable'=>false,
		'modal'=>true,
		'overlay'=>array(
			'backgroundColor'=>'#000',
			'opacity'=>'0.5'
		),
		'buttons'=>array(
			'OK'=>'js:function(){isok();}',
			'Cancel'=>'js:function(){iscancel();}',    
		),
	),
));
echo 'Вы действительно хотите удалить ?';
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>


<script type="text/javascript">
/**
 * url на удаление ставки (id_r_rates)
 */
var uri;

/**
 * Функция показывающая диалоговое окно
 * @param string - url на удаление ставки (id_r_rates)
 */
function showDialog(urlZ){
	uri = urlZ;
	$('#mydialog' ).dialog( { title: 'Подвердите действие' } ).dialog( 'open' ); 
}

/**
 * Диалоговое окно: пользователь нажал ок
 * 1. Посылаем запрос на удаление ставки
 * 2. Результат показываем в #viewRate
 * 3. Обновляем Грид
 */
function isok() {
	$.ajax({ 
		'url': uri,
		'cache':false,
		'success':function(html){ 
			$('#mydialog').dialog('close');
			$("#detail").html(html);
			$.fn.yiiGridView.update("org"); // update (Название грида)
		} 
	});
}

/**
 * Пользователь нажал отмену, закрываем диалоговое окно
 */
function iscancel() {
	$('#mydialog').dialog('close');
}

</script> 