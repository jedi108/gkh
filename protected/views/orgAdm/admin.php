<?php
$this->breadcrumbs=array(
	'Юр.лица'=>array('org/index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Список организаций', 'url'=>array('/Org/index')),
	array('label'=>'Создать организацию', 'url'=>array('/Org/create')),
);

?>

<?

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('org-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Список организаций</h1>
<div>
СПРАВКА: Поле источник имеет два значения:<br/>
1. админ - организации которые наполняет администратор сайта<br/>
2. Заявка - организации которые попадают из он-лайн заявок
</div>
<!--
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
-->

<?php 
// echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); 
// echo '<div class="search-form" style="display:none">';
// $this->renderPartial('_search',array(
// 	'model'=>$model,
// )); 
// echo "</div>"; //<!-- search-form -->
?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'org-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'name_small',
		//'FIO_rukovoditel',
		//'FIO_ispolnitil',
		// 'inn',
		// 'kpp',
		// 'bik',
		//'adr_fact',
		'adr_fact',
		'phone',
		// 'fax',
		//'email',
		array(
		'header'=>'Партнерство',
		'name'=>'orglink',
		'value'=>'$data->orglnk',
		'filter'=>Org::model()->org_links,
		),
		// array(
		// 'header'=>'Источник',
		// 'name'=>'_org_type',
		// 'value'=>'$data->orgFrom',
		// 'filter'=>Org::model()->_types,
		// ),
		array(
			'class'=>'CButtonColumn',
			'viewButtonUrl'=>'$this->grid->controller->createUrl("/Org/view",array("id"=>$data->id))',
		),
	),
)); ?>
