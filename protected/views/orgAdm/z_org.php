<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'enableAjaxValidation'=>true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => false,
))); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

<div class="span-13">
	 
		<?php echo $form->labelEx($model,'name_small', array('class'=>'span-6')); ?>
		<?php echo $form->textField($model,'name_small',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name_small'); ?>
	 
		<?php echo $form->labelEx($model,'FIO_rukovoditel', array('class'=>'span-6')); ?>
		<?php echo $form->textField($model,'FIO_rukovoditel',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'FIO_rukovoditel'); ?>
 
 
 
		<?php echo $form->labelEx($model,'FIO_ispolnitil', array('class'=>'span-6')); ?>
		<?php echo $form->textField($model,'FIO_ispolnitil',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'FIO_ispolnitil'); ?>
 
 
 
		<?php echo $form->labelEx($model,'email', array('class'=>'span-6')); ?>
		<?php echo $form->textField($model,'email',array('size'=>20,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
 
<div class="clear"></div>
 
		<?php echo $form->labelEx($model,'phone', array('class'=>'span-6')); ?>
		<?php echo $form->textField($model,'phone',array('size'=>15,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'phone'); ?>
 
<div class="clear"></div>

		<?php echo $form->labelEx($model,'fax', array('class'=>'span-6')); ?>
		<?php echo $form->textField($model,'fax',array('size'=>15,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'fax'); ?>
  
<div class="clear"></div>


 
</div>

<div class="span-10">

 
		<?php echo $form->labelEx($model,'adr_ur', array('class'=>'span-4')); ?>
		<?php echo $form->textField($model,'adr_ur',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'adr_ur'); ?>
 
 
 
		<?php //echo $form->labelEx($model,'adr_fact', array('class'=>'span-4')); ?>
		<?php //echo $form->textField($model,'adr_fact',array('size'=>25,'maxlength'=>255)); ?>
		<?php //echo $form->error($model,'adr_fact'); ?>
 

		<?php echo $form->labelEx($model,'bnk_korr', array('class'=>'span-4')); ?>
		<?php echo $form->textField($model,'bnk_korr',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'bnk_korr'); ?>

<div class="clear"></div>

		<?php echo $form->labelEx($model,'bnk_rchet', array('class'=>'span-4')); ?>
		<?php echo $form->textField($model,'bnk_rchet',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'bnk_rchet'); ?>
 
<div class="clear"></div>

 		<?php echo $form->labelEx($model,'bik', array('class'=>'span-4')); ?>
		<?php echo $form->textField($model,'bik',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'bik'); ?>

<div class="clear"></div>
 
		<?php echo $form->labelEx($model,'inn', array('class'=>'span-4')); ?>
		<?php echo $form->textField($model,'inn',array('size'=>12,'maxlength'=>12)); ?>
		<?php echo $form->error($model,'inn'); ?>
 
<div class="clear"></div>

		<?php echo $form->labelEx($model,'kpp', array('class'=>'span-4')); ?>
		<?php echo $form->textField($model,'kpp',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'kpp'); ?>
 

</div>
<div class="clear"></div>

<?php $this->renderPartial('_org_profiles', array('model'=>$profile ,'form'=>$form)); ?>







	<div class="buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->