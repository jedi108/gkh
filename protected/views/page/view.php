<?php
//$this->pageTitle=Yii::app()->name . ' - '. 'О нас';//$direct->name;
$this->pageTitle='Обучение специалистов ЖКХ. ' . ' - '. $model->title .'. Курсы ЖКХ Москва.';
$this->breadcrumbs= 
	$model->linkNews;
	//'Направления'=>array('/Direction/index'),
	//$direct->name,
 
?>

<div class="row">
<h3><?=$model->title?></h3>
</div>


<?php
if (count($model->jfiles )>0) {
	echo '<div class="right">';
	echo '<strong>Приложения</strong>';
	foreach ($model->jfiles as $key => $value) {
		echo '<div>';
		switch ($value['name']) {
			case '': $fName = 'файл'; break; 
			default: $fName = $value['name']; break;
		} 
		echo CHtml::link($fName, sprintf($value['file'],''));
		echo '</div>';
	}
	echo '</div>';
}
?>

 
<?php
$this->menu=array(
    array('label'=>'Редактировать', 'url'=>array('AdmPage/EditPage', 'id'=>$model->id)),
    array('label'=>'Все страницы', 'url'=>array('AdmPage/admin')),
);
?>
 
<div class="clearfixs"></div>
<?php //echo imgResizeHtml::getImgFromHtml($model->body); ?>

<div><?=$model->body?></div>

<?php 
//CVarDumper::dump($model->Gallary,100,1);
if (!is_null($model->Gallary)) {
    
    $this->renderPartial('application.modules.gallary.views.gallary._preview5',array('model'=>$model->Gallary));
}
?>