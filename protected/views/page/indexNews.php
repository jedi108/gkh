<?php
$this->pageTitle=Yii::app()->name . ' - '. 'Новости';//$direct->name;
$this->breadcrumbs=array(
	'Новости',
	//'Направления'=>array('/Direction/index'),
	//$direct->name,
);
?>

<?php
foreach ($models as $model) {
	echo Yii::app()->dateFormatter->format('d MMMM yyyy', $model->date_created);
	echo '<h4>';
	echo CHtml::link($model->title, Yii::app()->createUrl('/page/view',array('id'=>$model->id)));
	echo '</h4>';
}
?>
<?php 
$this->widget('CLinkPager', array(
	'pages' => $pages,
));
?>