<?php
$this->breadcrumbs=array(
	'Jmenus',
);

$this->menu=array(
	array('label'=>'Создать пункт', 'url'=>array('create')),
	array('label'=>'Все меню', 'url'=>array('admin')),
);
?>

<h1>Jmenus</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
