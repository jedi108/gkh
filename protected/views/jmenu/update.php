<?php
$this->breadcrumbs=array(
	'Jmenus'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Jmenu', 'url'=>array('index')),
	array('label'=>'Create Jmenu', 'url'=>array('create')),
	array('label'=>'View Jmenu', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Jmenu', 'url'=>array('admin')),
);
?>

<h1>Update Jmenu <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>