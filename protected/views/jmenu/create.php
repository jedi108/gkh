<?php
$this->breadcrumbs=array(
	'Jmenus'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Список пунктов', 'url'=>array('index')),
	array('label'=>'Все меню', 'url'=>array('admin')),
);
?>

<h1>Create Jmenu</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>