<?php
$this->breadcrumbs=array(
	'Jmenus'=>array('index'),
	$model->name,
);
 
?>

<?php
$this->menu=array(
    array('label'=>'Редактировать этот пункт', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить этот пункт (нельзя удалить главное)', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Создать пункт', 'url'=>array('create')),
    array('label'=>'Все меню', 'url'=>array('admin')),
);
?>

<h1>View Jmenu #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'link',
	),
)); ?>
