<?php
$this->widget('ext.yandexmap.YandexMap',array(
        'id'=>'map',
        'width'=>600,
        'height'=>400,
        'center'=>array(55.76, 37.64),
        'controls' => array(
            'zoomControl' => true,
            'typeSelector' => true,
            'mapTools' => true,
            'smallZoomControl' => false,
            'miniMap' => true,
            'scaleLine' => true,
            'searchControl' => true,
            'trafficControl' => true
        ),
        'placemark' => array(
            array(
                'lat'=>55.8,
                'lon'=>37.8,
                'properties'=>array(
                    'balloonContentHeader'=>'header',
                    'balloonContent'=>'1',
                    'balloonContentFooter'=>'footer',
                ),
                'options'=>array(
                    'draggable'=>true
                )
            )
        ),
        'polyline' => array(
            array('lat'=>55.80,'lon'=>37.30),
            array('lat'=>55.80,'lon'=>37.40),
            array('lat'=>55.70,'lon'=>37.30),
            array('lat'=>55.70,'lon'=>37.40),
            'properties'=>array(
                'balloonContentHeader'=>'header',
                'balloonContent'=>'Ломаная линия',
                'balloonContentFooter'=>'footer',
            ),
            'options'=>array(
                'draggable'=>true,
                'strokeColor'=> '#000000',
                'strokeWidth'=> 4,
                'strokeStyle'=> '1 5'
            )
        ),
    ));
?>