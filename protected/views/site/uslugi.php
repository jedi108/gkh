<?php
$this->pageTitle='Обучение специалистов ЖКХ. ';
$this->breadcrumbs=array( '',
	//'Услуги',//=>array('/Direction/index'),
	//$direct->name,
);
?>

<h1>Услуги</h1>
<h2>Направления подготовки</h2>
<?php

 
	echo '<div class="container span-18 last">';
		$this->widget('application.components.directions.directions',array());
	echo '</div>';

?>
<div class="clear"><br><br></div>
<h2>Курсы обучения</h2>

<?php
//* START ------------- NIVO ----------------
 
	$D = Direction::model()->findAll('img_slide IS NOT NULL');
		$files = array();
		foreach ($D as $key => $field) {
			if ( $field->img_slide!='') { 
				$img = array (
					'src'=>Media::imgPath($field->img_slide,'slide790', 'temp'),
					'url'=>Yii::app()->CreateUrl('/Direction/view/', array('id'=>$field->id)),
					'caption'=>$field->name,
					'linkOptions'=>array('title'=>$field->name),
					//'linkOptions'=>Yii::app()->request->baseUrl.'/site/page?view=events',
				);
				$files[]=$img;
			}
		}
	$this->widget('application.extensions.nivoslider.ENivoSlider', array(
	    'images'=>$files,
	    )
	);	
 
//* -------------------- NIVO ------------- END

echo '<div class="clear"><br><br></div>';

foreach ($files as $k => $f) {
	echo CHtml::link( 
		$f['caption'], //title
		$f['url']
	);
	echo '<br>';		
}