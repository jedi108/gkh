<?php
$this->breadcrumbs=array(
	'Заявки'=>array('/zayavka/start'),
	'Создать заявку',
);

$this->menu=array(
	array('label'=>'List Profile', 'url'=>array('index')),
	array('label'=>'Manage Profile', 'url'=>array('admin')),
);


?>

<h1><?=(isset($org))? $this->zTitleProfile : 'Создать профиль' ?></h1>

<?php 

if (isset($org))
	echo $this->renderPartial('application.views.profile._form', array('model'=>$model, 'org'=>$org)); 
else
	echo $this->renderPartial('application.views.profile._form', array('model'=>$model)); 

?>