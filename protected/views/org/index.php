<?php
$this->breadcrumbs=array(
	'Orgs',
);

$this->menu=array(
	array('label'=>'Создать организацию', 'url'=>array('create')),
	array('label'=>'Справочник организаций', 'url'=>array('orgAdm/admin')),
);
?>

<h1>Orgs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
