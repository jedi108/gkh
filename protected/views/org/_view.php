<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_small')); ?>:</b>
	<?php echo CHtml::encode($data->name_small); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FIO_rukovoditel')); ?>:</b>
	<?php echo CHtml::encode($data->FIO_rukovoditel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FIO_ispolnitil')); ?>:</b>
	<?php echo CHtml::encode($data->FIO_ispolnitil); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('logo')); ?>:</b>
	<?php echo CHtml::encode($data->logo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inn')); ?>:</b>
	<?php echo CHtml::encode($data->inn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kpp')); ?>:</b>
	<?php echo CHtml::encode($data->kpp); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('bik')); ?>:</b>
	<?php echo CHtml::encode($data->bik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adr_fact')); ?>:</b>
	<?php echo CHtml::encode($data->adr_fact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adr_ur')); ?>:</b>
	<?php echo CHtml::encode($data->adr_ur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fax')); ?>:</b>
	<?php echo CHtml::encode($data->fax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnk_korr')); ?>:</b>
	<?php echo CHtml::encode($data->bnk_korr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bnk_rchet')); ?>:</b>
	<?php echo CHtml::encode($data->bnk_rchet); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('_org_type')); ?>:</b>
	<?php echo CHtml::encode($data->_org_type); ?>
	<br />

	*/ ?>

</div>