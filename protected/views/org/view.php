<?php
$this->breadcrumbs=array(
	'Orgs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Список орг', 'url'=>array('index')),
	array('label'=>'Создать орг', 'url'=>array('/orgAdm/create')),
	array('label'=>'Редактировать орг', 'url'=>array('/orgAdm/update', 'id'=>$model->id)),
	array('label'=>'Удалить орг', 'url'=>'#', 'linkOptions'=>array('submit'=>array('/orgAdm/delete','id'=>$model->id),'confirm'=>'Вы уверены удалить организацию?')),
	array('label'=>'Справочник орг', 'url'=>array('/orgAdm/admin')),
);
?>

<h1>ЮрЛицо: <?php echo $model->name_small; ?></h1>

<div class="span-10 border-1">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		//'name_small',
		'FIO_rukovoditel',
		'FIO_ispolnitil',
		'logo',
		'inn',
		'kpp',
		'bik',
		'adr_fact',
		'adr_ur',
		'phone',
		'fax',
		'email',
		'bnk_korr',
		'bnk_rchet',
		'_org_type',
	),
)); ?>
</div>
<div class="span-10 border-1">
<?php
	echo FunLib::prrAttr($model->profile);
?>
</div>